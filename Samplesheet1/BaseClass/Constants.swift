//
//  Constants.swift
//  Samplesheet1
//
//  Created by Ratheesh TR on 07/01/19.
//  Copyright © 2019 Sankar. All rights reserved.
//

import Foundation
import UIKit
extension UIViewController{
    func storeValue<T>(val : T , Key : String) {
        let user = UserDefaults.standard
        user.set(val, forKey: Key)
        user.synchronize()
    }
    
    func getValue<T>(key: String) -> T?{
        let user = UserDefaults.standard
        let value = user.value(forKey: key) as? T
        return value
    }
}

extension String{
    var containsValidCharacter: Bool {
        let characterSet = CharacterSet(charactersIn: "1234567890-")
        let range = (self as NSString).rangeOfCharacter(from: characterSet)
        return range.location != NSNotFound
    }
}

struct Constants{
    static let appdelegateSharedInstance = UIApplication.shared.delegate as! AppDelegate
    static let motorYield = "motoryield"
    static let dogLegSeverity = "dogLegSeverity"
    static let currentInclination = "currentInclination"
    static let currentAzimuth = "currentAzimuth"
    static let slideSeen = "slideSeen"
    static let amountSlid = "amountSlid"
    static let courseLength = "courseLength"
    static let toolFace = "toolFace"
    static let TVD      = "TVD"
    static let targetTVD = "targetTVD"
    static let targetInclination = "targetInc"
    static let buildRateNeeded = "buildRateNeeded"
    static let depth = "depth"
    static let inclination = "inclination"
    static let azimuth = "azimuth"
    static let depth1 = "depth1"
    static let inclination1 = "inclination1"
    static let azimuth1 = "azimuth1"
    static let projectedInc = "projectedInclination"
    
}

struct BuildRateNeeded {
    var currentInclination:String?
    var currentTVD:String?
    var targetInclination:String?
    var targetTVD:String?
}

struct motorYieldValues {
    var dogLegSeverity:String?
    var slideSeen:String?
    var courselength:String?
}

struct DogLegValues {
    var depth:String?
    var inclination:String?
    var azimuth:String?
    var depth1:String?
    var inclination1:String?
    var azimuth1:String?
}

struct FeetToSlide {
    var motorYield:String?
    var buildRateNeeded:String?
    var courselength:String?
}

struct ProjectionToBit {
    var motorYield:String?
    var amountSlid:String?
    var currentInclination:String?
}

struct slideVlaues {
    var amoundslideValue:String?
    var motorYieldValue:String?
    var currenIncValue:String?
    var currentAzimuthValue:String?
    var toolFaceValue:String?
    
}
