//
//  GSBaseLabel.swift
//  PickZyShoppingApp
//
//  Created by Ratheesh TR on 5/3/18.
//  Copyright © 2018 PickZy Software Pvt Ltd. All rights reserved.
//

import UIKit

@IBDesignable class GSBaseLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpLabel()
    }
    
    override func prepareForInterfaceBuilder() {
        setUpLabel()
    }
    
    //MARK: Methods to change some properties
    
    func setUpLabel() {
        layer.borderWidth = 0.6
        layer.borderColor = UIColor.lightGray.cgColor
        layer.masksToBounds = true
    }
}
