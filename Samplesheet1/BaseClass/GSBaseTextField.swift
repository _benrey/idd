//
//  GSTextField.swift
//  PickZyShoppingApp
//
//  Created by Ratheesh TR on 5/2/18.
//  Copyright © 2018 PickZy Software Pvt Ltd. All rights reserved.
//

import UIKit

@IBDesignable class GSBaseTextField: UITextField,UITextFieldDelegate {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpTextField()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpTextField()
    }
    
    override func prepareForInterfaceBuilder() {
        setUpTextField()
    }
    
    //MARK: Methods to change some properties
    
    func setUpTextField() {
        delegate = self
        layer.borderWidth = 0.6
        layer.borderColor = UIColor.lightGray.cgColor
        layer.masksToBounds = true
    }
}
    

