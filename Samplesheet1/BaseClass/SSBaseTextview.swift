//
//  SSBaseTextview.swift
//  Samplesheet1
//
//  Created by Sankar on 20/7/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import Foundation
import UIKit
 class SSBaseTextview:UITextView,UITextViewDelegate {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpTextField()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpTextField()
    }
    
    override func prepareForInterfaceBuilder() {
        setUpTextField()
    }
    
    //MARK: Methods to change some properties
    
    func setUpTextField() {
        delegate = self
        layer.borderWidth = 0.6
        layer.borderColor = UIColor.lightGray.cgColor
        layer.masksToBounds = true
}
}
