//
//  GSCornerEdgeView.swift
//  PickZyShoppingApp
//
//  Created by Bala on 5/10/18.
//  Copyright © 2018 PickZy Software Pvt Ltd. All rights reserved.
//

import UIKit

@IBDesignable class GSCornerEdgeView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpCornerEdgeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpCornerEdgeView()
    }
    
    override func prepareForInterfaceBuilder() {
        setUpCornerEdgeView()
    }
    
    //MARK: Methods to change some properties
    
    func setUpCornerEdgeView() {
        layer.borderWidth = 0.6
        layer.borderColor = UIColor.lightGray.cgColor
        layer.masksToBounds = true
    }

}
