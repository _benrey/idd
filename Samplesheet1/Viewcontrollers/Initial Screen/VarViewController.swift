//
//  VarViewController.swift
//  Samplesheet1
//
//  Created by Ratheesh TR on 04/02/19.
//  Copyright © 2019 Sankar. All rights reserved.
//

import UIKit

class VarViewController: UIViewController,UIScrollViewDelegate,UITextFieldDelegate {
    
    @IBOutlet var textFieldGroup : [UITextField]!
    @IBOutlet weak var enterBtn: UIButton!
    
    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var ptbLabel: UILabel!
    @IBOutlet weak var dlsLabel: UILabel!
    @IBOutlet weak var ftsLabel: UILabel!
    @IBOutlet weak var brnLabel: UILabel!
    @IBOutlet weak var pageControl:UIPageControl!
    var activeTextField = UITextField()
    
    var motorYield_Double = Double()
    var motorYield_Str = String()
    
    var ptbValue_Double = Double()
    var ptbValue_Str = String()
    
    var ftsValue_Double = Double()
    var ftsValue_Str = String()
    
    var brnValue_Double = Double()
    var brnValue_Str = String()
    
    
    var isIncreasing = true
    // @IBOutlet weak var mainStackView: UIStackView!
    var operation = false
    var changingSign = false
    @IBOutlet weak var mainStack:UIStackView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.enterBtn.layer.cornerRadius = 15
        self.enterBtn.layer.masksToBounds = true
        for (idx,item) in textFieldGroup.enumerated(){
            let tmpView = UIView.init(frame: CGRect(x: 0, y: 0, width: 5, height: 10))
            item.leftView = tmpView
            item.leftViewMode = .always
            item.tag = idx
        }
       activeTextField = textFieldGroup[0]
       pageControl.currentPage = 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in 0..<textFieldGroup.count{
        textFieldGroup[i].delegate = self
        textFieldGroup[i].inputView = UIView()
        }
        textFieldGroup[0].becomeFirstResponder()
        
        self.tabBarController?.moreNavigationController.setNavigationBarHidden(true, animated: true)
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeLeft)
        DispatchQueue.main.async {
            self.iPhoneScreenSizes()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func iPhoneScreenSizes(){
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        
        switch height {
        case 480.0:
            mainStack.spacing = 25
            print("iPhone 3,4")
        case 568.0:
            mainStack.spacing = 25
        case 667.0:
            mainStack.spacing = 40
            print("iPhone 6")
        case 736.0:
            mainStack.spacing = 40
            print("iPhone 6+")
        case 812.0:
            mainStack.spacing = 55
            print("iPhone X")
        case 896.0:
            mainStack.spacing = 55
            print("iPhone XR")
        default:
            print("not an iPhone")
            
        }
    }
    
    @IBAction func calcButtonTapped(_ sender: UIButton) {
        
        switch sender.tag {
            
        case 0:
            Addnumberfunc("0")
        case 1:
            Addnumberfunc("1")
        case 2:
            Addnumberfunc("2")
        case 3:
            Addnumberfunc("3")
        case 4:
            Addnumberfunc("4")
        case 5:
            Addnumberfunc("5")
        case 6:
            Addnumberfunc("6")
        case 7:
            Addnumberfunc("7")
        case 8:
            Addnumberfunc("8")
        case 9:
            Addnumberfunc("9")
        case 11:
            decimalPointPressed()
        case 10:
            plusMinusAction()
        default:
            print("dfgdfg")
        }
    }
    
    func decimalPointPressed()
    {
        guard let text = activeTextField.text , !text.contains(".") else {
            return
        }
        if activeTextField.text?.count == 0 {
            activeTextField.text = "0."
        }
        else{
            activeTextField.text = text + "."
        }
    }
    
    @IBAction func acaction(_ sender: UIButton) {
        
        activeTextField.text = ""
        switch activeTextField.tag {
        case 0:
            Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity] = ""
            
        case 1:
            Constants.appdelegateSharedInstance.initialDict[Constants.currentInclination] = ""
            
        case 2:
            Constants.appdelegateSharedInstance.initialDict[Constants.currentAzimuth] = ""
            
        case 3:
            Constants.appdelegateSharedInstance.initialDict[Constants.slideSeen] = ""
            
        case 4:
            Constants.appdelegateSharedInstance.initialDict[Constants.amountSlid] = ""
            
        case 5:
            Constants.appdelegateSharedInstance.initialDict[Constants.courseLength] = ""
            
        case 6:
            Constants.appdelegateSharedInstance.initialDict[Constants.toolFace] = ""
            
        case 7:
            Constants.appdelegateSharedInstance.initialDict[Constants.TVD] = ""
            
        case 8:
            Constants.appdelegateSharedInstance.initialDict[Constants.targetTVD] = ""
            
        case 9:
            Constants.appdelegateSharedInstance.initialDict[Constants.targetInclination] = ""
            
        default:
            print("default")
        }
        setvalues("", activeTextField.tag)
    }
    
    @IBAction func bsaction(_ sender: UIButton) {
        if activeTextField.text != ""{
            activeTextField.text?.removeLast()
            setvalues(activeTextField.text!, activeTextField.tag)
        }
        else{
            setvalues("", activeTextField.tag)
        }
    }
    
    func plusMinusAction()
    {
        if (activeTextField.text == "") {
            return
        } else {
            if !changingSign {
                changingSign = true
                activeTextField.text = "-" + activeTextField.text!
            } else {
                changingSign = false
                activeTextField.text?.removeFirst()
            }
        }
        setvalues(activeTextField.text!, activeTextField.tag)
    }
    
    @IBAction func percentageAction(_ sender: Any) {
        
        
    }
    
    func Addnumberfunc(_ number:String)
    {
        if (activeTextField.text?.count)! < 5{
        var textnum = ""
        if let num = activeTextField.text{
            textnum = num
        }
        textnum = textnum + number

        activeTextField.text = textnum
//        activeTextField.layer.borderWidth = 0.6
//        activeTextField.layer.borderColor = UIColor.darkGray.cgColor
//        activeTextField.layer.masksToBounds = true
        setvalues(textnum,activeTextField.tag)
        }
    }
    
    
    func setvalues(_ textnum : String ,_ index:Int){
        switch index {
        case 0:
            Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity] = textnum
            
        case 1:
            Constants.appdelegateSharedInstance.initialDict[Constants.currentInclination] = textnum
            
        case 2:
            Constants.appdelegateSharedInstance.initialDict[Constants.currentAzimuth] = textnum
            
        case 3:
            Constants.appdelegateSharedInstance.initialDict[Constants.slideSeen] = textnum
            
        case 4:
            Constants.appdelegateSharedInstance.initialDict[Constants.amountSlid] = textnum
            
        case 5:
            Constants.appdelegateSharedInstance.initialDict[Constants.courseLength] = textnum
            
        case 6:
            Constants.appdelegateSharedInstance.initialDict[Constants.toolFace] = textnum
        
        case 7:
            Constants.appdelegateSharedInstance.initialDict[Constants.TVD] = textnum
            
        case 8:
            Constants.appdelegateSharedInstance.initialDict[Constants.targetTVD] = textnum
            
        case 9:
            Constants.appdelegateSharedInstance.initialDict[Constants.targetInclination] = textnum
            
        default:
            print("default")
        }
    }
    
    @IBAction func enterAction(_ sender: UIButton) {
        
        let dogLegTemp = Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity]
        let currentInclinationTemp = Constants.appdelegateSharedInstance.initialDict[Constants.currentInclination]
        let currentAzimuthTemp = Constants.appdelegateSharedInstance.initialDict[Constants.currentAzimuth]
        let slideSeenTemp = Constants.appdelegateSharedInstance.initialDict[Constants.slideSeen]
        let AmountSlidTemp = Constants.appdelegateSharedInstance.initialDict[Constants.amountSlid]
        let courseLengthTemp = Constants.appdelegateSharedInstance.initialDict[Constants.courseLength]
        let ToolFaceTemp = Constants.appdelegateSharedInstance.initialDict[Constants.toolFace]
        let TVD = Constants.appdelegateSharedInstance.initialDict[Constants.TVD]
        let targetTVD = Constants.appdelegateSharedInstance.initialDict[Constants.targetTVD]
        let targetInc = Constants.appdelegateSharedInstance.initialDict[Constants.targetInclination]
        
        
        if (dogLegTemp != nil && currentInclinationTemp != nil && currentAzimuthTemp != nil && slideSeenTemp != nil && AmountSlidTemp != nil && ToolFaceTemp != nil && courseLengthTemp != nil && TVD != nil && targetTVD != nil && targetInc != nil) && (dogLegTemp != "" && currentInclinationTemp != "" && currentAzimuthTemp != "" && slideSeenTemp != "" && AmountSlidTemp != "" && ToolFaceTemp != "" && courseLengthTemp != "" && TVD != "" && targetTVD != "" && targetInc != ""){
             if Double(currentAzimuthTemp!)! > 359{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter current azimuth value less than 360", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if Double(AmountSlidTemp!)! > 999 {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter amount slid value less than 1000", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
//            let tabBarController = self.navigationController?.viewControllers[0] as! UITabBarController
//            tabBarController.selectedIndex += 1
                tabBarController?.selectedIndex += 1
            }
        }

        for (idx,item) in textFieldGroup.enumerated(){
            if item == activeTextField , idx < 9{
                activeTextField = textFieldGroup[idx + 1]
                pageControl.currentPage = idx + 1
                break
            }
        }
    }
    
    //MARK: - TextField Delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
        for (idx,item) in textFieldGroup.enumerated(){
            if item == activeTextField , idx < 9{
                pageControl.currentPage = idx
                break
            }
        }
//       textField.resignFirstResponder()
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        if textFieldGroup[0].text!.count != 0 && textFieldGroup[3].text!.count != 0 && textFieldGroup[5].text!.count != 0 {
            let doglegSeverity = Double(textFieldGroup[0].text!)
            let slideSeenValue = Double(textFieldGroup[3].text!)
            let courseLengthValue = Double(textFieldGroup[5].text!)
            let result = (doglegSeverity! / slideSeenValue!) * courseLengthValue!
            motorYield_Double = Double(result)
            let resultCheck =  (motorYield_Double*100).rounded()/100
            motorYield_Str = String(resultCheck)
            //     storeValue(val: resultValue_Str, Key: Constants.motorYield)
            Constants.appdelegateSharedInstance.initialDict[Constants.motorYield] = motorYield_Str
            print("\(motorYield_Str)")
//            tabBarController?.tabBar.items![1].title = motorYield_Str
//            lblResultValue.text = resultValue_Str
            myLabel.text = "MY : " + motorYield_Str
            
        }
        
      
        if motorYield_Double != 0 && textFieldGroup[3].text!.count != 0 && textFieldGroup[1].text!.count != 0 {
            let motoryield = motorYield_Double
            let amountSlidValue = Double(textFieldGroup[3].text!)
            let currentIncValue = Double(textFieldGroup[1].text!)
            let result = (motoryield / 100 * amountSlidValue!) + currentIncValue!
            ptbValue_Double = Double(result)
            let resultCheck =  (ptbValue_Double*100).rounded()/100
            ptbValue_Str = String(resultCheck)
            let motorYield_Str = String(motoryield)
            //     storeValue(val: motorYield_Str, Key: Constants.motorYield)
            Constants.appdelegateSharedInstance.initialDict[Constants.motorYield] = motorYield_Str
            
            ptbLabel.text =  "PTB : " + ptbValue_Str
            Constants.appdelegateSharedInstance.initialDict[Constants.projectedInc] = ptbValue_Str
        }
        
        if textFieldGroup[1].text!.count != 0 && textFieldGroup[7].text!.count != 0 && textFieldGroup[9].text!.count != 0 && textFieldGroup[8].text!.count != 0 {
            let currentIncValue = Double(textFieldGroup[1].text!)
            let currentTVDVal = Double(textFieldGroup[7].text!)
            let targetIncVal = Double(textFieldGroup[9].text!)
            let targetTvdVal = Double(textFieldGroup[8].text!)
            let buildrate = ((sin(deg2rad(targetIncVal!))-sin(deg2rad(currentIncValue!)))*5729.58) / (Double(targetTvdVal!) - Double(currentTVDVal!))
            brnValue_Double = Double(buildrate)
            let resultCheck =  (brnValue_Double*100).rounded()/100
            brnValue_Str = String(resultCheck)
            brnLabel.text = "BRN : " + brnValue_Str
            Constants.appdelegateSharedInstance.initialDict[Constants.buildRateNeeded] = brnValue_Str
        }
        
        if motorYield_Double != 0 && brnValue_Double != 0 && textFieldGroup[3].text!.count != 0 {
            let motoryield = motorYield_Double
            let buildrateneeded = brnValue_Double
            let courseLengthValue = Double(textFieldGroup[3].text!)
            let result = (buildrateneeded / motoryield) * courseLengthValue!
            ftsValue_Double = Double(result)
            let resultCheck =  (ftsValue_Double*100).rounded()/100
            ftsValue_Str = String(resultCheck)
            ftsLabel.text = "FTS : " + ftsValue_Str
        }
       
    }
    
    
    
   
    
    //MARK: - Gesture Recognizer methods
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
              //  print("Swiped right")
                slideThroughVariablesLeft()
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
               // print("Swiped left")
                slideThroughVariablesRight()
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    func slideThroughVariablesRight(){
        for (idx,item) in textFieldGroup.enumerated(){
            if item == activeTextField , idx <= 9{
                if idx == 9 {
                    activeTextField = textFieldGroup[0]
                    pageControl.currentPage = 0
                    break
                }
                else{
                    activeTextField = textFieldGroup[idx + 1]
                    pageControl.currentPage = idx + 1
                    break
                }
            }
        }
    }
    
    func slideThroughVariablesLeft(){
        for (idx,item) in textFieldGroup.enumerated(){
            if item == activeTextField , idx >= 0{
                if idx == 0 {
                activeTextField = textFieldGroup[9]
                pageControl.currentPage = 9
                break
                }
                else{
                activeTextField = textFieldGroup[idx - 1]
                pageControl.currentPage = idx - 1
                break
                }
            }
        }
    }
}

