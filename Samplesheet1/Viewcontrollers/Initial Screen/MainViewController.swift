//
//  MainViewController.swift
//  Samplesheet1
//
//  Created by Mohanapriya on 26/06/19.
//  Copyright © 2019 Sankar. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

      @IBOutlet weak var contentView: UIView!
    
    var currentViewController: UIViewController?
    
    lazy var firstChildTabVC: UIViewController? = {
        let firstChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "VarViewController")
        return firstChildTabVC
    }()
    
    lazy var secondChildTabVC : UIViewController? = {
        let secondChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "DoglegCalculationVC")
        
        return secondChildTabVC
    }()
    
    lazy var thirdChildTabVC : UIViewController? = {
        let thirdChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "MotorYieldCalculationVC")
        
        return thirdChildTabVC
    }()
    
    lazy var forthChildTabVC : UIViewController? = {
        let forthChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "ProjectionCalculationVC")
        
        return forthChildTabVC
    }()
    
    lazy var fifthChildTabVC : UIViewController? = {
        let fifthChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "BuildRateCalculationVC")
        
        return fifthChildTabVC
    }()
    
    lazy var sixthChildTabVC : UIViewController? = {
        let sixthChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "FTSCalculationVC")
        
        return sixthChildTabVC
    }()
    
    lazy var seventhChildTabVC : UIViewController? = {
        let seventhChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "ouijaVC")
        
        return seventhChildTabVC
    }()
    
    enum TabIndex : Int {
        case firstChildTab = 0
        case secondChildTab = 1
        case thirdChildTab = 2
        case fourthChildTab = 3
        case fifthChildTab = 4
        case sixthChildTab = 5
        case seventhChildTab = 6
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        displayCurrentTab(0)
    
    }
    
    
    @IBAction func BtnAction(_ sender: UIButton) {
        
        displayCurrentTab(sender.tag - 1)
    }
    
    @IBAction func menuAct(_ sender: Any) {
        
        let optionMenu = UIAlertController(title: nil, message: "Choose", preferredStyle: .actionSheet)
        
        let curveAction = UIAlertAction(title: "Curves", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "curveVC") as! CurveViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        optionMenu.addAction(curveAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        switch index {
        case TabIndex.firstChildTab.rawValue :
            vc = firstChildTabVC
            
        case TabIndex.secondChildTab.rawValue :
            vc = secondChildTabVC
            
        case TabIndex.thirdChildTab.rawValue :
            vc = thirdChildTabVC
            
        case TabIndex.fourthChildTab.rawValue :
            vc = forthChildTabVC
            
        case TabIndex.fifthChildTab.rawValue :
            vc = fifthChildTabVC
            
        case TabIndex.sixthChildTab.rawValue :
            vc = sixthChildTabVC
        case TabIndex.seventhChildTab.rawValue :
            vc = seventhChildTabVC
        default:
            return nil
        }
        
        return vc
    }
    
    
    func displayCurrentTab(_ tabIndex: Int){
        if let vc = viewControllerForSelectedSegmentIndex(tabIndex) {
            
            self.addChildViewController(vc)
            vc.didMove(toParentViewController: self)
            
            vc.view.frame = self.contentView.bounds
            self.contentView.addSubview(vc.view)
            self.currentViewController = vc
            
                
            }
        }

}
