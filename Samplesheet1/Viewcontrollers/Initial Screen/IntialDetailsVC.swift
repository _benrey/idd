//
//  IntialDetailsVC.swift
//  Samplesheet1
//
//  Created by Ratheesh TR on 24/01/19.
//  Copyright © 2019 Sankar. All rights reserved.
//

import UIKit

class IntialDetailsVC: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UIScrollViewDelegate,UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet var calcButtons: [UIButton]!
    let titleText = ["Dog Leg Severity:","Current Inclination:","Current Azimuth:","Slide Seen:","Slide Ahead:","Course Length:","Tool Face:"]
    
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    var isIncreasing = true
    // @IBOutlet weak var mainStackView: UIStackView!
    var operation = false
    var changingSign = false

    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func calcButtonTapped(_ sender: UIButton) {
        
        let buttonposition = sender.convert(CGPoint.zero, to: mainCollectionView)
        let index = mainCollectionView.indexPathForItem(at: buttonposition)
        
        switch sender.tag {
            
        case 0:
            Addnumberfunc("0", index!)
        case 1:
            Addnumberfunc("1", index!)
        case 2:
            Addnumberfunc("2", index!)
        case 3:
            Addnumberfunc("3", index!)
        case 4:
            Addnumberfunc("4", index!)
        case 5:
            Addnumberfunc("5", index!)
        case 6:
            Addnumberfunc("6", index!)
        case 7:
            Addnumberfunc("7", index!)
        case 8:
            Addnumberfunc("8", index!)
        case 9:
            Addnumberfunc("9", index!)
        case 11:
            decimalPointPressed(index!)
        case 10:
            plusMinusAction(index!)
        default:
            print("dfgdfg")
        }
    }
    
    func decimalPointPressed(_ index:IndexPath)
    {
        let cell = mainCollectionView.cellForItem(at: index) as! IntialViewCell
        guard let text = cell.valueTxtField.text, !text.contains(".") else {
            return }
        if cell.valueTxtField.text?.count == 0
        {
            cell.valueTxtField.text = "0."
        }
        else
        {
            cell.valueTxtField.text = text + "."
        }
    }
    
    @IBAction func acaction(_ sender: UIButton) {
        let buttonposition = sender.convert(CGPoint.zero, to: mainCollectionView)
        let index = mainCollectionView.indexPathForItem(at: buttonposition)
        let cell = mainCollectionView.cellForItem(at: index!) as! IntialViewCell
        
        
        switch index?.item {
        case 0:
            Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity] = nil
            
        case 1:
            Constants.appdelegateSharedInstance.initialDict[Constants.currentInclination] = nil
            
        case 2:
            Constants.appdelegateSharedInstance.initialDict[Constants.currentAzimuth] = nil
            
        case 3:
            Constants.appdelegateSharedInstance.initialDict[Constants.slideSeen] = nil
            
        case 4:
            Constants.appdelegateSharedInstance.initialDict[Constants.amountSlid] = nil
            
        case 5:
            Constants.appdelegateSharedInstance.initialDict[Constants.courseLength] = nil
            
        case 6:
            Constants.appdelegateSharedInstance.initialDict[Constants.toolFace] = nil
            
        default:
            print("default")
        }
        cell.valueTxtField.text = ""
    }
    
    @IBAction func bsaction(_ sender: UIButton) {
        let buttonposition = sender.convert(CGPoint.zero, to: mainCollectionView)
        let index = mainCollectionView.indexPathForItem(at: buttonposition)
        
        let cell = mainCollectionView.cellForItem(at: index!) as! IntialViewCell
        
        if cell.valueTxtField.text != ""
        {
            cell.valueTxtField.text?.removeLast()
            setvalues(cell.valueTxtField.text!, index!)
        }
        else {
            setvalues("", index!)
        }
    }
    
    func plusMinusAction(_ index:IndexPath)
    {
        let cell = mainCollectionView.cellForItem(at: index) as! IntialViewCell
        
        if (cell.valueTxtField.text == "") {
            return
        } else {
            if !changingSign {
                changingSign = true
                cell.valueTxtField.text = "-" + cell.valueTxtField.text!
            } else {
                changingSign = false
                cell.valueTxtField.text?.removeFirst()
            }
        }
        setvalues(cell.valueTxtField.text!, index)
    }
    
    @IBAction func percentageAction(_ sender: Any) {
        
        
    }
    
    func Addnumberfunc(_ number:String, _ index:IndexPath)
    {
        
        let cell = mainCollectionView.cellForItem(at: index) as! IntialViewCell
        var textnum = ""
        if let num = cell.valueTxtField.text{
            
            //   textnum = String(cell.valueLbl.text!)
            textnum = num
        }
        if operation {
            
            textnum = ""
            operation = false
        }
        textnum = textnum + number
        
        cell.valueTxtField.text = textnum
        setvalues(textnum,index)
    }
    
    
    func setvalues(_ textnum : String ,_ index:IndexPath){
        switch index.item {
        case 0:
            Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity] = textnum
            
        case 1:
            Constants.appdelegateSharedInstance.initialDict[Constants.currentInclination] = textnum
            
        case 2:
            Constants.appdelegateSharedInstance.initialDict[Constants.currentAzimuth] = textnum
            
        case 3:
            Constants.appdelegateSharedInstance.initialDict[Constants.slideSeen] = textnum
            
        case 4:
            Constants.appdelegateSharedInstance.initialDict[Constants.amountSlid] = textnum
            
        case 5:
            Constants.appdelegateSharedInstance.initialDict[Constants.courseLength] = textnum
            
        case 6:
            Constants.appdelegateSharedInstance.initialDict[Constants.toolFace] = textnum
            
        default:
            print("default")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titleText.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "initialCell", for: indexPath) as! IntialViewCell
        cell.enterBtn.tag = indexPath.item
        cell.titleLbl.text = titleText[indexPath.item]
        
        //       print ("Collection View Height :\(collectionView.frame.height)")
        
        if indexPath.item == 5{
         cell.signBtn.isEnabled = true
        }
        else{
        cell.signBtn.isEnabled = false
            }
        
        switch indexPath.item {
        case 0:
            if let val = Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity]{
                cell.valueTxtField.text = val
            }
            else{
                cell.valueTxtField.text = ""
            }
        case 1:
            if let val = Constants.appdelegateSharedInstance.initialDict[Constants.currentInclination]{
                cell.valueTxtField.text = val
            }
            else{
                cell.valueTxtField.text = ""
            }
        case 2:
            if let val = Constants.appdelegateSharedInstance.initialDict[Constants.currentAzimuth]{
                cell.valueTxtField.text = val
            }
            else{
                cell.valueTxtField.text = ""
            }
        case 3:
            if let val = Constants.appdelegateSharedInstance.initialDict[Constants.slideSeen]{
                cell.valueTxtField.text = val
            }
            else{
                cell.valueTxtField.text = ""
            }
        case 4:
            if let val = Constants.appdelegateSharedInstance.initialDict[Constants.amountSlid]{
                cell.valueTxtField.text = val
            }
            else{
                cell.valueTxtField.text = ""
            }
        case 5:
            if let val = Constants.appdelegateSharedInstance.initialDict[Constants.courseLength]{
                cell.valueTxtField.text = val
            }
            else{
                cell.valueTxtField.text = ""
            }
        case 6:
            if let val = Constants.appdelegateSharedInstance.initialDict[Constants.toolFace]{
                cell.valueTxtField.text = val
            }
            else{
                cell.valueTxtField.text = ""
            }
        default:
            cell.titleLbl.text = ""
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: mainCollectionView.frame.width, height: mainCollectionView.frame.height)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    @IBAction func enterAction(_ sender: UIButton) {
        
        let currentIndex = mainCollectionView.contentOffset.x / mainCollectionView.frame.size.width;
        print(Int(currentIndex))
        let indexPath = IndexPath(item: Int(currentIndex + 1), section: 0)
        pageControl.currentPage = NSInteger(ceil(currentIndex))
        
        let indexPath1 = IndexPath(item: Int(currentIndex), section: 0)
        let cell = mainCollectionView.cellForItem(at: indexPath1) as! IntialViewCell
        switch sender.tag {
        case 0:
            mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
            if let val = Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity] {
                cell.valueTxtField.text = val
            }
            else{
                cell.valueTxtField.text = ""
                
            }
        case 1:
            mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
            if let val = Constants.appdelegateSharedInstance.initialDict[Constants.currentInclination] {
                cell.valueTxtField.text = val
            }
            else{
                cell.valueTxtField.text = ""
            }
        case 2:
            mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
            if let val = Constants.appdelegateSharedInstance.initialDict[Constants.currentAzimuth] {
                cell.valueTxtField.text = val
            }
            else{
                cell.valueTxtField.text = ""
                
            }
        case 3:
            mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
            if let val = Constants.appdelegateSharedInstance.initialDict[Constants.slideSeen] {
                cell.valueTxtField.text = val
            }
            else{
                cell.valueTxtField.text = ""
            }
        case 4:
            mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
            if let val = Constants.appdelegateSharedInstance.initialDict[Constants.amountSlid] {
                cell.valueTxtField.text = val
            }
            else{
                cell.valueTxtField.text = ""
            }
        case 5:
            mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
            if let val = Constants.appdelegateSharedInstance.initialDict[Constants.courseLength] {
                cell.valueTxtField.text = val
            }
            else{
                cell.valueTxtField.text = ""
            }
        case 6:
            let dogLegTemp = Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity]
            let currentInclinationTemp = Constants.appdelegateSharedInstance.initialDict[Constants.currentInclination]
            let currentAzimuthTemp = Constants.appdelegateSharedInstance.initialDict[Constants.currentAzimuth]
            let slideSeenTemp = Constants.appdelegateSharedInstance.initialDict[Constants.slideSeen]
            let AmountSlidTemp = Constants.appdelegateSharedInstance.initialDict[Constants.amountSlid]
            let courseLengthTemp = Constants.appdelegateSharedInstance.initialDict[Constants.courseLength]
            let ToolFaceTemp = Constants.appdelegateSharedInstance.initialDict[Constants.toolFace]
            
            if (dogLegTemp == nil && currentInclinationTemp == nil && currentAzimuthTemp == nil && slideSeenTemp == nil && AmountSlidTemp == nil && ToolFaceTemp == nil && courseLengthTemp == nil) || (dogLegTemp == "" && currentInclinationTemp == "" && currentAzimuthTemp == "" && slideSeenTemp == "" && AmountSlidTemp == "" && ToolFaceTemp == "" && courseLengthTemp == ""){
                
                let alert = UIAlertController(title:"Alert", message:"Please fill all the fields", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else if dogLegTemp == nil || dogLegTemp == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Dog Leg Severity value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else if currentInclinationTemp == nil || currentInclinationTemp == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter current inclination value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if currentAzimuthTemp == nil || currentAzimuthTemp == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Current Azimuth value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else if slideSeenTemp == nil || slideSeenTemp == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Slide Seen value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if AmountSlidTemp == nil || AmountSlidTemp == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter amount slid value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if courseLengthTemp == nil || courseLengthTemp == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter course Length value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if ToolFaceTemp == nil || ToolFaceTemp == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter tool face value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if Double(currentAzimuthTemp!)! > 359{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter current azimuth value less than 360", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if Double(AmountSlidTemp!)! > 999 {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter amount slid value less than 1000", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                let tabBarController = self.navigationController?.viewControllers[0] as! UITabBarController
                tabBarController.selectedIndex += 1
            }
        default:
            print(cell.titleLbl.text!)
            
        }
        
        // print("Current IndexPath:\(indexPath)")
    }
    
    //MARK: Page Controller methods
    
    
    

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let currentIndex = mainCollectionView.contentOffset.x / mainCollectionView.frame.size.width;
        print(Int(currentIndex))
        pageControl.currentPage = NSInteger(ceil(currentIndex))
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = mainCollectionView.contentOffset
        visibleRect.size = mainCollectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = mainCollectionView.indexPathForItem(at: visiblePoint) else {
            return
        }
        let cell = mainCollectionView.cellForItem(at: indexPath) as! IntialViewCell
        
        switch indexPath.item {
        case 0:
            cell.valueTxtField.text = Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity]
            
        case 1:
            cell.valueTxtField.text = Constants.appdelegateSharedInstance.initialDict[Constants.currentInclination]
            
        case 2:
            cell.valueTxtField.text = Constants.appdelegateSharedInstance.initialDict[Constants.currentAzimuth]
            
        case 3:
            cell.valueTxtField.text = Constants.appdelegateSharedInstance.initialDict[Constants.slideSeen]
            
        case 4:
            cell.valueTxtField.text = Constants.appdelegateSharedInstance.initialDict[Constants.amountSlid]
            
        case 5:
            cell.valueTxtField.text = Constants.appdelegateSharedInstance.initialDict[Constants.courseLength]
            
            
        case 6:

                let dogLegTemp = Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity]
                let currentInclinationTemp = Constants.appdelegateSharedInstance.initialDict[Constants.currentInclination]
                let currentAzimuthTemp = Constants.appdelegateSharedInstance.initialDict[Constants.currentAzimuth]
                let slideSeenTemp = Constants.appdelegateSharedInstance.initialDict[Constants.slideSeen]
                let AmountSlidTemp = Constants.appdelegateSharedInstance.initialDict[Constants.amountSlid]
                let courselengthTemp = Constants.appdelegateSharedInstance.initialDict[Constants.courseLength]
                let ToolFaceTemp = Constants.appdelegateSharedInstance.initialDict[Constants.toolFace]
                
                if (dogLegTemp == nil && currentInclinationTemp == nil && currentAzimuthTemp == nil && slideSeenTemp == nil && AmountSlidTemp == nil && ToolFaceTemp == nil && courselengthTemp == nil) || (dogLegTemp == "" && currentInclinationTemp == "" && currentAzimuthTemp == "" && slideSeenTemp == "" && AmountSlidTemp == "" && ToolFaceTemp == "" && courselengthTemp == ""){
                    
                    let alert = UIAlertController(title:"Alert", message:"Please fill all the fields", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else if dogLegTemp == nil || dogLegTemp == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter Dog Leg Severity value", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else if currentInclinationTemp == nil || currentInclinationTemp == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter current inclination value", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                else if currentAzimuthTemp == nil || currentAzimuthTemp == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter Current Azimuth value", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else if slideSeenTemp == nil || slideSeenTemp == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter slide seen value", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                else if AmountSlidTemp == nil || AmountSlidTemp == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter Amount slid value", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                else if ToolFaceTemp == nil || ToolFaceTemp == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter tool face value", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                else if courselengthTemp == nil || courselengthTemp == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter course length value", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                    let tabBarController = self.navigationController?.viewControllers[0] as! UITabBarController
                    tabBarController.selectedIndex += 1
                }

            
        default:
            print(cell.titleLbl.text!)
            
        }
       // print("Curren IndexPath:\(indexPath)")
    }
}
