//
//  BitRunInformationVC.swift
//  Samplesheet1
//
//  Created by Sankar on 23/7/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit

class BitRunInformationVC: UIViewController {

    @IBOutlet var ViewPostRuninformation: UIView!
    @IBOutlet var ViewScalingFactors: UIView!
    @IBOutlet var ViewHoleInformation: UIView!
    @IBOutlet var ViewActive: UIView!
    @IBOutlet var Scrollview: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Scrollview.contentSize = CGSize(width:ViewPostRuninformation.frame.origin.x+ViewPostRuninformation.frame.size.width, height: ViewPostRuninformation.frame.size.height)
        Scrollview.showsVerticalScrollIndicator = false
        Scrollview.showsHorizontalScrollIndicator = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
