//
//  ToSlideVC.swift
//  Samplesheet1
//
//  Created by Sankar on 29/5/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit

class ToSlideVC: UIViewController {

    @IBOutlet var TxtfieldSlide: GSBaseTextField!
    @IBOutlet var TxtfieldDesireDogleg: GSBaseTextField!
    @IBOutlet var TxtfieldSurveyInterval: GSBaseTextField!
    @IBOutlet var TxtfieldMotorDogleg: GSBaseTextField!
    var CalculateCondition : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CalculateCondition = 1
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func Action_Calculate(_ sender: Any) {
        if(((TxtfieldMotorDogleg.text?.count) != 0) && ((TxtfieldSurveyInterval.text?.count) != 0) && ((TxtfieldDesireDogleg.text?.count) != 0))  {
            CalculateCondition = 2
        let motordogleg = Double(TxtfieldMotorDogleg.text!)!
        let surveyInterval = Double(TxtfieldSurveyInterval.text!)!
        let DesiredDogleg = Double(TxtfieldDesireDogleg.text!)!
        
        if(DesiredDogleg == motordogleg) {
            TxtfieldSlide.text = "100%"
        }
        else {
            if(DesiredDogleg > motordogleg){
                TxtfieldSlide.text = "Impossible!"
            }
            else {
                TxtfieldSlide.text = "\(Int(DesiredDogleg/(motordogleg/100) * surveyInterval/100))"
            }
        }
        }
        
        if(CalculateCondition == 1){
            self.popupAlert()
        }
        
        
        
    }
    
    //MARK: TextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        view.endEditing(true)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        let maxLength = 5
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength && string == numberFiltered
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
