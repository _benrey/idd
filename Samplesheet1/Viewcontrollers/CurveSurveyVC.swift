//
//  CurveSurveyVC.swift
//  Samplesheet1
//
//  Created by Sankar on 12/7/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit

class CurveSurveyVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var Surveytableview: UITableView!
    @IBOutlet var Headerview: GSCornerEdgeView!
    @IBOutlet var Scrollview: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        Scrollview.contentSize = CGSize(width: Headerview.frame.size.width, height: Scrollview.frame.size.height)
        Scrollview.showsVerticalScrollIndicator = false
        Scrollview.showsHorizontalScrollIndicator = false
    }
    
     //MARK: Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Surveytableview.dequeueReusableCell(withIdentifier:"Cell", for: indexPath ) as! CurvesurveyCell
        return cell
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
