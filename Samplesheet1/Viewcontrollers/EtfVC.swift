//
//  EtfVC.swift
//  Samplesheet1
//
//  Created by Sankar on 29/5/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit

class EtfVC: UIViewController {

    @IBOutlet var ViewDoglegResponse: UIView!
    @IBOutlet var TxtfieldDogleg: GSBaseTextField!
    @IBOutlet var TxtfieldResultactuelDeltaAZ: GSBaseTextField!
    @IBOutlet var TxtfieldRawResult: GSBaseTextField!
    @IBOutlet var TxtfieldresultToolface: GSBaseTextField!
    @IBOutlet var TxtfieldMotorDogleg: GSBaseTextField!
    @IBOutlet var TxtfieldFootageSlid: GSBaseTextField!
    @IBOutlet var TxtfieldEndAZ: GSBaseTextField!
    @IBOutlet var TxtfieldEndIncl: GSBaseTextField!
    @IBOutlet var TxtfieldStartAZ: GSBaseTextField!
    @IBOutlet var TxtfieldStartIncl: GSBaseTextField!
    @IBOutlet var Scrollview: UIScrollView!
    var CalculateCondition : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CalculateCondition = 1
       self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Action_Calculate(_ sender: Any) {
        // Raw result
        if(((TxtfieldEndAZ.text?.count) != 0) && ((TxtfieldStartAZ.text?.count) != 0))  {
            CalculateCondition = 2
        let endaz = Double(TxtfieldEndAZ.text!)
        let startaz = Double(TxtfieldStartAZ.text!)
//        let  raw = Int(endaz!) - Int(startaz!)
        let  raw = endaz! - startaz!
        TxtfieldRawResult.text = "\(raw)"
        }
        
        // Dogleg Calculation
        if(((TxtfieldStartIncl.text?.count) != 0) && ((TxtfieldEndIncl.text?.count) != 0) && ((TxtfieldStartAZ.text?.count) != 0) && ((TxtfieldEndAZ.text?.count) != 0) )  {
            CalculateCondition = 2
        let startInc = Double(TxtfieldStartIncl.text!)
        let endInc = Double(TxtfieldEndIncl.text!)
        let startAzimuth = Double(TxtfieldStartAZ.text!)
        let endAzimuth = Double(TxtfieldEndAZ.text!)
        
        let Dogleg = rad2deg(acos((cos(deg2rad(startInc!)) * cos(deg2rad(endInc!))) + (sin(deg2rad(startInc!)) * sin(deg2rad(endInc!)) * cos(abs(deg2rad(endAzimuth!+0.00001))-(deg2rad(startAzimuth!))))))
       // TxtfieldDogleg.text = "\(Int(Dogleg))"
        TxtfieldDogleg.text = "\(Double(Dogleg))"
        }
        
        // Actual Delta Az Result
        if((TxtfieldRawResult.text?.count) != 0){
      //  let Rawvalue = Int(TxtfieldRawResult.text!)!
        let Rawvalue = Double(TxtfieldRawResult.text!)!
        CalculateCondition = 2
        if( Rawvalue > 180 ){
            TxtfieldResultactuelDeltaAZ.text = "\(Rawvalue - 360)"
        }
        else if (Rawvalue < -180) {
            TxtfieldResultactuelDeltaAZ.text = "\(Rawvalue + 360)"
        }
        else {
            TxtfieldResultactuelDeltaAZ.text = "\(Rawvalue)"
        }
        }
        
        // Effective Toolface result
     if(((TxtfieldStartIncl.text?.count) != 0) && ((TxtfieldEndIncl.text?.count) != 0) && ((TxtfieldDogleg.text?.count) != 0) && ((TxtfieldResultactuelDeltaAZ.text?.count) != 0) ) {
        
        CalculateCondition = 2
        let startInc = Double(TxtfieldStartIncl.text!)
        let endInc = Double(TxtfieldEndIncl.text!)
            
        let doglegvalue = Double(TxtfieldDogleg.text!)
       // let actualdeltaAZ = Int(TxtfieldResultactuelDeltaAZ.text!)
        let actualdeltaAZ = Double(TxtfieldResultactuelDeltaAZ.text!)
        if(actualdeltaAZ! < 0){
           let Toolface = -rad2deg(acos(((cos(deg2rad(startInc!))*cos(deg2rad(doglegvalue!)))-cos(deg2rad(endInc!)))/(sin(deg2rad(startInc!))*sin(deg2rad(doglegvalue!)))))
            TxtfieldresultToolface.text = "\(Toolface)"
        }
        else {
          let Toolface = rad2deg(acos(((cos(deg2rad(startInc!))*cos(deg2rad(doglegvalue!)))-cos(deg2rad(endInc!)))/(sin(deg2rad(startInc!))*sin(deg2rad(doglegvalue!)))))
            TxtfieldresultToolface.text = "\(Toolface)"
        }
        }
        
        //DEGREES(ACOS(((COS(RADIANS(B2))*COS(RADIANS(F7)))-COS(RADIANS(B4)))/(SIN(RADIANS(B2))*SIN(RADIANS(F7)))))
        
        if(CalculateCondition == 1){
            self.popupAlert()
        }
        
        
     }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Scrollview.contentSize = CGSize(width:Scrollview.frame.size.width , height: ViewDoglegResponse.frame.origin.y+ViewDoglegResponse.frame.size.height+90 )
        Scrollview.showsVerticalScrollIndicator = false
        Scrollview.showsHorizontalScrollIndicator = false
        
    }
    
    //MARK: TextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        view.endEditing(true)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        let maxLength = 5
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength && string == numberFiltered
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
