//
//  ResultsViewController.swift
//  Samplesheet1
//
//  Created by Sankar on 1/9/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController {
    
    @IBOutlet var vwCircle: UIView!
    @IBOutlet var segmentFeet: UISegmentedControl!
    @IBOutlet var txtCurrAzi: UITextField!
    @IBOutlet var txtCurrInc: UITextField!
    @IBOutlet var txtAmountSlid: UITextField!
    @IBOutlet var txtToolFace: UITextField!
    @IBOutlet var vwCurrAzi: UIView!
    @IBOutlet var vwCurrInc: UIView!
    @IBOutlet var vwAmtSlid: UIView!
    @IBOutlet var vwToolFace: UIView!
    
    var lblInclValue = UILabel()
    var lblAziValue = UILabel()
    var lbl0 = UILabel()
    var lbl90 = UILabel()
    var lbl180 = UILabel()
    var lbl270 = UILabel()
    let shapeLayer = CAShapeLayer()
    let shapeLayerAzimuth = CAShapeLayer()
    var lblCircle: CircularLabel!
    var btnSideMenu = UIButton()
    var amountYield = String()
    var motorYield = String()
    var currentInclination = String()
    var currentAzimuth = String()
    var toolface = String()
    var CalculateCondition : Int!
    var resultInclination = String()
    var resultAzimuth = String()
    var resultInclination1 = String()
    var resultAzimuth1 = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CalculateCondition = 1
        assignbackground()
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        rightSwipe.direction = .right
        view.addGestureRecognizer(rightSwipe)
        
        labelAlignment()
        
        lbl0.text = "0"
        lbl90.text = "90"
        lbl180.text = "180"
        lbl270.text = "270"
        
        txtCurrAzi.textAlignment = .center
        txtCurrInc.textAlignment = .center
        txtToolFace.textAlignment = .center
        txtAmountSlid.textAlignment = .center
        txtCurrAzi.text = currentAzimuth
        txtCurrInc.text = currentInclination
        txtToolFace.text = toolface
        txtAmountSlid.text = amountYield
        
        calculate()
        

    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    

    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        
        if (sender.direction == .right) {
            print("Swipe Right")
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
//    func layoutTheCircularViews() {
//        lblCircle.frame = CGRect(x:0, y: 0, width: 300, height: 100)
//        lblInclValue.frame = CGRect(x: 0, y: 0, width: 100, height: 30)
//        lblAziValue.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
//        lbl0.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
//        lbl90.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
//        lbl180.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
//        lbl270.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
//    }
//

    func labelAlignment(){
        
        vwAmtSlid.backgroundColor = UIColor.clear
        vwAmtSlid.layer.cornerRadius = 5.0
        vwAmtSlid.layer.borderColor = UIColor(red: 49.0/255.0, green: 181.0/255.0, blue: 223.0/255.0, alpha: 1.0).cgColor
        vwAmtSlid.layer.borderWidth = 0.5
        vwAmtSlid.clipsToBounds = true
        
        vwCurrAzi.backgroundColor = UIColor.clear
        vwCurrAzi.layer.cornerRadius = 5.0
        vwCurrAzi.layer.borderColor = UIColor(red: 49.0/255.0, green: 181.0/255.0, blue: 223.0/255.0, alpha: 1.0).cgColor
        vwCurrAzi.layer.borderWidth = 0.5
        vwCurrAzi.clipsToBounds = true
        
        vwCurrInc.backgroundColor = UIColor.clear
        vwCurrInc.layer.cornerRadius = 5.0
        vwCurrInc.layer.borderColor = UIColor(red: 49.0/255.0, green: 181.0/255.0, blue: 223.0/255.0, alpha: 1.0).cgColor
        vwCurrInc.layer.borderWidth = 0.5
        vwCurrInc.clipsToBounds = true
        
        vwToolFace.backgroundColor = UIColor.clear
        vwToolFace.layer.cornerRadius = 5.0
        vwToolFace.layer.borderColor = UIColor(red: 49.0/255.0, green: 181.0/255.0, blue: 223.0/255.0, alpha: 1.0).cgColor
        vwToolFace.layer.borderWidth = 0.5
        vwToolFace.clipsToBounds = true
    
        lblCircle = CircularLabel(frame: CGRect(x:0, y: 0, width: 300, height: 100))
        lblCircle.center = CGPoint(x: vwCircle.center.x, y: vwCircle.center.y + 5.0)
        lblCircle.textColor = .white
        lblCircle.clockwise = false
        lblCircle.textAlignment = .center
        vwCircle.addSubview(lblCircle)

        lblCircle.textColor = .red
        lblCircle.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        
        lblInclValue = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        lblInclValue.center = CGPoint(x: vwCircle.center.x + 25, y: vwCircle.center.y - 200.0 )
        lblInclValue.textAlignment = .center
        lblInclValue.textColor = .white
        lblInclValue.textColor = UIColor(red: 49.0/255.0, green: 181.0/255.0, blue: 223.0/255.0, alpha: 1.0)
        lblInclValue.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        vwCircle.addSubview(lblInclValue)
        
        
        lblAziValue = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
        lblAziValue.center = CGPoint(x: vwCircle.center.x + 25, y: vwCircle.center.y - 85.0)
        lblAziValue.textAlignment = .center
        lblAziValue.textColor = .white
        lblAziValue.textColor = UIColor(red: 49.0/255.0, green: 181.0/255.0, blue: 223.0/255.0, alpha: 1.0)
        lblAziValue.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        vwCircle.addSubview(lblAziValue)
        
        
        lbl0 = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        lbl0.center = CGPoint(x: vwCircle.center.x + 25, y: vwCircle.center.y - 345.0)
        lbl0.textAlignment = .center
        lbl0.textColor = .white
        lbl0.textColor = UIColor.gray
        lbl0.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        vwCircle.addSubview(lbl0)
        
        
        lbl90 = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        lbl90.center = CGPoint(x: vwCircle.center.x + 170, y: vwCircle.center.y - 200 )
        lbl90.textAlignment = .center
        lbl90.textColor = .white
        lbl90.textColor = UIColor.gray
        lbl90.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        vwCircle.addSubview(lbl90)
        
        
        lbl180 = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        lbl180.center = CGPoint(x: vwCircle.center.x + 25, y: vwCircle.center.y - 55)
        lbl180.textAlignment = .center
        lbl180.textColor = .white
        lbl180.textColor = UIColor.gray
        lbl180.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        vwCircle.addSubview(lbl180)
        
        
        lbl270 = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        lbl270.center = CGPoint(x: vwCircle.center.x - 125, y: vwCircle.center.y - 200 )
        lbl270.textAlignment = .center
        lbl270.textColor = .white
        lbl270.textColor = UIColor.gray
        lbl270.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        vwCircle.addSubview(lbl270)
        
    }
    
    
    func circularLayer(){
        let ceter = CGPoint(x: vwCircle.center.x + 25, y: vwCircle.center.y - 200.0 )
        
        let trackLayer = CAShapeLayer()
        let circularPath = UIBezierPath(arcCenter: ceter, radius: 130, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi , clockwise: true)
//        let circularPath1 = UIBezierPath(arcCenter: ceter, radius: 130, startAngle: -CGFloat.pi / 2, endAngle: CGFloat.pi , clockwise: true)
        let endAngle = CGFloat((resultAzimuth as NSString).floatValue)
        let circularPath1 = UIBezierPath(arcCenter: ceter, radius: 130, startAngle: -CGFloat.pi / 2, endAngle: ((endAngle)*(CGFloat.pi/180.0) - (CGFloat.pi / 2)) , clockwise: true)
        
        trackLayer.path = circularPath.cgPath
        trackLayer.path = circularPath.cgPath
        trackLayer.strokeColor = UIColor.lightGray.cgColor
        trackLayer.lineWidth = 5
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineCap = kCALineCapRound
        vwCircle.layer.addSublayer(trackLayer)
    
        
        shapeLayer.path = circularPath1.cgPath
        shapeLayer.strokeColor = UIColor(red: 49.0/255.0, green: 181.0/255.0, blue: 223.0/255.0, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 5
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = kCALineCapRound
        shapeLayer.strokeEnd = 0
        vwCircle.layer.addSublayer(shapeLayer)
        let endAngleIncl = CGFloat((resultInclination1 as NSString).floatValue)
        let circularPathAzimuth = UIBezierPath(arcCenter: ceter, radius: 75, startAngle: CGFloat.pi / 2, endAngle: ((endAngleIncl)*(CGFloat.pi/45.0) - (CGFloat.pi / 2)), clockwise: false)
        let circularPathAzimuth2 = UIBezierPath(arcCenter: ceter, radius: 75, startAngle: CGFloat.pi / 2, endAngle: CGFloat.pi, clockwise: false)
        let trackLayer1 = CAShapeLayer()
        trackLayer1.path = circularPathAzimuth2.cgPath
        trackLayer1.strokeColor = UIColor.lightGray.cgColor
        trackLayer1.lineWidth = 5
        trackLayer1.fillColor = UIColor.clear.cgColor
        trackLayer1.lineCap = kCALineCapRound
        vwCircle.layer.addSublayer(trackLayer1)
        shapeLayerAzimuth.path = circularPathAzimuth.cgPath
        shapeLayerAzimuth.strokeColor = UIColor(red: 49.0/255.0, green: 181.0/255.0, blue: 223.0/255.0, alpha: 1.0).cgColor
        shapeLayerAzimuth.lineWidth = 5
        shapeLayerAzimuth.fillColor = UIColor.clear.cgColor
        shapeLayerAzimuth.lineCap = kCALineCapRound
        shapeLayerAzimuth.strokeEnd = 0
        vwCircle.layer.addSublayer(shapeLayerAzimuth)
//        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        handleTap()
    }
    

//    @objc private func handleTap() {
//        print("Attempting to animate stroke")
//        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
//        basicAnimation.toValue = 1
//        basicAnimation.duration = 2
//        basicAnimation.fillMode = kCAFillModeForwards
//        basicAnimation.isRemovedOnCompletion = false
//        shapeLayer.add(basicAnimation, forKey: "urSoBasic")
//        shapeLayerAzimuth.add(basicAnimation, forKey: "azimuthBasic")
//    }
    
    func handleTap() {
        print("Attempting to animate stroke")
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.toValue = 1
        basicAnimation.duration = 2
        basicAnimation.fillMode = kCAFillModeForwards
        basicAnimation.isRemovedOnCompletion = false
        shapeLayer.add(basicAnimation, forKey: "urSoBasic")
        shapeLayerAzimuth.add(basicAnimation, forKey: "azimuthBasic")
    }
    
    
    func assignbackground(){
        let background = UIImage(named: "background")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    func calculate() {
        if(((amountYield.count) != 0) && ((motorYield.count) != 0))  {
            // Dogleg Per CL Result
            CalculateCondition = 2
            motorYield = "\(Double(amountYield)! * Double(motorYield)! / 100)"
        }
        
        if(((motorYield.count) != 0) && ((currentInclination.count) != 0))  {
            //Inclination Result
            CalculateCondition = 2
            let DoglegPerCL = Double(motorYield)
            let toolfacecondition = Double(toolface)
            let Inclination = rad2deg(cos(deg2rad(toolfacecondition!)) * deg2rad(DoglegPerCL!)) + Double(currentInclination)!
            resultInclination = "\(Inclination)"
        }
        
        if(((resultInclination.count) != 0) && ((currentInclination.count) != 0))  {
            //Inclination Result 2
            CalculateCondition = 2
            resultInclination1 = "\(Double(resultInclination)! - Double(currentInclination)!)"
            lblInclValue.text = resultInclination1
            circularLayer()
        }
        
        if(((motorYield.count) != 0) && ((toolface.count) != 0) && ((currentInclination.count) != 0) && ((resultInclination.count) != 0))  {
            //Azimuth Result 2
            CalculateCondition = 2
            let DoglegPerCL = Double(motorYield)
            let toolfacecondition = Double(toolface)
            if(toolfacecondition == 0){
                resultAzimuth = "\(0)"
            }
            else {
                if(toolfacecondition == 180){
                    resultAzimuth1 = "\(0)"
                }
                else {
                    let inclination = Double(currentInclination)
                    let inclinationresult1 = Double(resultInclination)
                    if(toolfacecondition! < 0) {
                        let azimuthresult2 = (-rad2deg(acos((cos(deg2rad(DoglegPerCL!))-(cos(deg2rad(inclination!))*cos(deg2rad(Double(inclinationresult1!)))))/(sin(deg2rad(inclination!))*sin(deg2rad(inclinationresult1!))))))
                        resultAzimuth1 = "\(azimuthresult2)"
                        print(resultAzimuth1, "")
                    }
                    else {
                        let azimuthresult2 = rad2deg(acos((cos(deg2rad(DoglegPerCL!))-(cos(deg2rad(inclination!))*cos(deg2rad(inclinationresult1!))))/(sin(deg2rad(inclination!))*sin(deg2rad(inclinationresult1!)))))
                        
                        // =(DEGREES(ACOS((COS(RADIANS(5.04))-(COS(RADIANS(10))*COS(RADIANS(15.039))))/(SIN(RADIANS(10))*SIN(RADIANS(15.039))))))
                        resultAzimuth1 = "\(azimuthresult2)"
                    }
                }
            }
        }
        
        if(((currentAzimuth.count) != 0) && ((resultAzimuth1.count) != 0))  {
            //Azimuth result 1
            CalculateCondition = 2
            print(Double(currentAzimuth)! + Double(resultAzimuth1)!)
            let aziresult1 = Double(currentAzimuth)! + Double(resultAzimuth1)!
            if(aziresult1 < 0) {
                resultAzimuth = "\(Double(currentAzimuth)! + Double(resultAzimuth1)! + 360)"
                lblAziValue.text = resultAzimuth
                circularLayer()
            }
            else {
                if(aziresult1 > 360){
                    resultAzimuth = "\(Double(currentAzimuth)! + Double(resultAzimuth1)! - 360)"
                    lblAziValue.text = resultAzimuth
                    circularLayer()
                }
                else {
                    resultAzimuth = "\(Double(currentAzimuth)! + Double(resultAzimuth1)!)"
                    lblAziValue.text = resultAzimuth
                    circularLayer()
                }
            }
        }
        
        if(CalculateCondition == 1){
            self.popupAlert()
            
            
        }
    }
    
    
    
// Previous Design for donut charts
    
    
    
  /*  func circularLayer(){
        let ceter = view.center
        
        let trackLayer = CAShapeLayer()
        let trackLayer2 = CAShapeLayer()
        
        let  circularPath = UIBezierPath(arcCenter: ceter, radius: 130, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi , clockwise: true)
        let  circularPath1 = UIBezierPath(arcCenter: ceter, radius: 130, startAngle: -CGFloat.pi / 2, endAngle: CGFloat.pi , clockwise: true)
        trackLayer.path = circularPath.cgPath
        let  circularPath2 = UIBezierPath(arcCenter: ceter, radius: 150, startAngle: -CGFloat.pi / 2, endAngle: CGFloat.pi , clockwise: true)
        
        trackLayer.path = circularPath.cgPath
        trackLayer.strokeColor = UIColor.lightGray.cgColor
        trackLayer.lineWidth = 5
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineCap = kCALineCapRound
        view.layer.addSublayer(trackLayer)
        var lable = UILabel()
        
        trackLayer2.path = circularPath2.cgPath
        trackLayer2.strokeColor = UIColor.clear.cgColor
        trackLayer2.lineWidth = 5
        trackLayer2.fillColor = UIColor.clear.cgColor
        trackLayer2.lineCap = kCALineCapRound
        view.layer.addSublayer(trackLayer2)
        
        shapeLayer.path = circularPath1.cgPath
        shapeLayer.strokeColor = UIColor.orange.cgColor
        shapeLayer.lineWidth = 5
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = kCALineCapRound
        shapeLayer.strokeEnd = 0
        view.layer.addSublayer(shapeLayer)
        
        
        let  circularPathAzimuth = UIBezierPath(arcCenter: ceter, radius: 75, startAngle: CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: false)
        let  circularPathAzimuth2 = UIBezierPath(arcCenter: ceter, radius: 75, startAngle: CGFloat.pi / 2, endAngle: CGFloat.pi, clockwise: false)
        let trackLayer1 = CAShapeLayer()
        trackLayer1.path = circularPathAzimuth2.cgPath
        trackLayer1.strokeColor = UIColor.lightGray.cgColor
        trackLayer1.lineWidth = 5
        trackLayer1.fillColor = UIColor.clear.cgColor
        trackLayer1.lineCap = kCALineCapRound
        view.layer.addSublayer(trackLayer1)
        shapeLayerAzimuth.path = circularPathAzimuth.cgPath
        shapeLayerAzimuth.strokeColor = UIColor.green.cgColor
        shapeLayerAzimuth.lineWidth = 5
        shapeLayerAzimuth.fillColor = UIColor.clear.cgColor
        shapeLayerAzimuth.lineCap = kCALineCapRound
        shapeLayerAzimuth.strokeEnd = 0
        view.layer.addSublayer(shapeLayerAzimuth)
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        
    }
    
    
    @objc private func handleTap() {
        print("Attempting to animate stroke")
        
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.toValue = 1
        basicAnimation.duration = 2
        basicAnimation.fillMode = kCAFillModeForwards
        basicAnimation.isRemovedOnCompletion = false
        shapeLayer.add(basicAnimation, forKey: "urSoBasic")
        shapeLayerAzimuth.add(basicAnimation, forKey: "azimuthBasic")
    }
    
    func assignbackground(){
        let background = UIImage(named: "background")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    
    
} */
    
    
   
}
