//
//  ResultFinalViewController.swift
//  Samplesheet1
//
//  Created by Sankar on 25/10/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit
import HGCircularSlider
var back = 0

var backVal = 0

class ResultFinalViewController: UIViewController, UIScrollViewDelegate, UITextFieldDelegate {

    @IBOutlet var vwMain: UIView!
    @IBOutlet var resultView: CircularSlider!
    @IBOutlet var previousResultView: UIView!
    @IBOutlet var scrollVw: UIScrollView!
    
    @IBOutlet var amountYield_lbl: UITextField!
    @IBOutlet var motorYield_lbl: UITextField!
    @IBOutlet var currentInclination_lbl: UITextField!
    @IBOutlet var currentAzimuth_lbl: UITextField!
    @IBOutlet var toolface_lbl: UITextField!
    
    @IBOutlet weak var resultInc_titleLbl:UILabel!
    @IBOutlet weak var resultAzimuth_titleLbl:UILabel!
    
    @IBOutlet weak var resultInc_valueLbl:UILabel!
    @IBOutlet weak var resultAzimuth_valueLbl:UILabel!
    
    @IBOutlet weak var resultAzimuthSlider_View: CircularSlider!
    
    var lblInclValue = UILabel()
    var lblAziValue = UILabel()
    var lbl0 = UILabel()
    var lbl90 = UILabel()
    var lbl180 = UILabel()
    var lbl270 = UILabel()
    let shapeLayer = CAShapeLayer()
    let shapeLayerAzimuth = CAShapeLayer()
    var lblCircle: CircularLabel!
    var btnSideMenu = UIButton()
  
    var lblInclValue1 = UILabel()
    var lblAziValue1 = UILabel()
    var lbl0_1 = UILabel()
    var lbl90_1 = UILabel()
    var lbl180_1 = UILabel()
    var lbl270_1 = UILabel()
    let shapeLayer1 = CAShapeLayer()
    let shapeLayerAzimuth1 = CAShapeLayer()
    let trackLay = CAShapeLayer()
    let trackLay1 = CAShapeLayer()
    
    let trackLay2 = CAShapeLayer()
    let trackLay3 = CAShapeLayer()
    
    let trackLayer = CAShapeLayer()
    let trackLayer1 = CAShapeLayer()
    
    var amountYield = String()
    var motorYield = String()
    var currentInclination = String()
    var currentAzimuth = String()
    var toolface = String()
    var CalculateCondition : Int!
    var resultInclination = String()
    var resultAzimuth = "0"
    var resultInclination1 = String()
    var resultAzimuth1 = "0"
    
    var resultInclinationVal = Float()
    var resultAzimuthVal = Float()
    var resultInclinationVal1 = Float()
    var resultAzimuthVal1 = Float()
    
    let orangeColor = UIColor(red: 247.0/255.0, green: 148.0/255.0, blue: 29.0/255.0, alpha: 1.0)
    
    var tmpGraphView: UIView!
    var graphFrame: CGRect!
    var graphCenter : CGPoint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assignbackground()
        
        amountYield_lbl.delegate = self
        motorYield_lbl.delegate = self
        currentInclination_lbl.delegate = self
        currentAzimuth_lbl.delegate = self
        toolface_lbl.delegate = self
        
        calculate()
        graphFrame = resultView.bounds
        graphCenter = resultView.center
     //   tmpGraphView = resultView
    
        //circularLayer(resultView)
       // labelAlignment(resultView)
        
//        resultView.minimumValue = 0
//        resultView.maximumValue = 360
//        resultView.endPointValue = CGFloat((resultAzimuth as NSString).floatValue)
//        resultView.isUserInteractionEnabled = false
//
//        resultAzimuthSlider_View.minimumValue = 0
//        resultAzimuthSlider_View.maximumValue = 360
//        resultAzimuthSlider_View.endPointValue = CGFloat((resultInclination as NSString).floatValue)
//        resultAzimuthSlider_View.isUserInteractionEnabled = false
//
//        resultAzimuth_valueLbl.text = String(resultAzimuth)
//        resultInc_valueLbl.text = String(resultInclination)
         drawValues()
        
        amountYield_lbl.text = amountYield
        motorYield_lbl.text = motorYield
        currentInclination_lbl.text = currentInclination
        currentAzimuth_lbl.text = currentAzimuth
        toolface_lbl.text = toolface
        
        handleTap()
        handleTap1()
        
        
//        lbl0.text = "0"
//        lbl90.text = "90"
//        lbl180.text = "180"
//        lbl270.text = "270"
        
//        lbl0.text = "N"
//        lbl90.text = "E"
//        lbl180.text = "W"
//        lbl270.text = "S"
        
        lbl0_1.text = "0"
        lbl90_1.text = "90"
        lbl180_1.text = "180"
        lbl270_1.text = "270"
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        
        resultView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        
    }
    
    
    override func viewDidLayoutSubviews() {
        
//        lbl0.text = "0"
//        lbl90.text = "90"
//        lbl180.text = "180"
//        lbl270.text = "270"
        
//        lbl0.text = "N"
//        lbl90.text = "E"
//        lbl180.text = "W"
//        lbl270.text = "S"

        
        lbl0_1.text = "0"
        lbl90_1.text = "90"
        lbl180_1.text = "180"
        lbl270_1.text = "270"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
    //    self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK: - Calculation Methods
    private func drawValues(){
//        resultView.minimumValue = 0
//        resultView.maximumValue = 360
//        resultView.endPointValue = CGFloat((resultAzimuth as NSString).floatValue)
//        resultView.isUserInteractionEnabled = false
//
//        resultAzimuthSlider_View.minimumValue = 0
//        resultAzimuthSlider_View.maximumValue = 360
//        resultAzimuthSlider_View.endPointValue = CGFloat((resultInclination as NSString).floatValue)
//        resultAzimuthSlider_View.isUserInteractionEnabled = false
        
        resultAzimuth_valueLbl.text = String(resultAzimuth)
        resultInc_valueLbl.text = String(resultInclination)
    }
    
    
    func labelAlignment(_ vw: UIView){
        
        lblInclValue = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
//        lblInclValue.center = CGPoint(x: vw
//            .frame.width / 2, y: vw.frame.height - 5 )
        
        lblInclValue.center = CGPoint(x: vw.frame.width / 2, y: vw.frame.height / 2)   //Edit:
        

        lblInclValue.textAlignment = .center
        lblInclValue.textColor = UIColor.white
        lblInclValue.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        lblInclValue.text = resultInclination
 //       lblInclValue.text = resultInclination1
        vw.addSubview(lblInclValue)
        
        
        lblAziValue = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
  //      lblAziValue.center = CGPoint(x: vw.frame.width / 2, y: vw.frame.height / 2)
        
        lblAziValue.center = CGPoint(x: vw.frame.width / 2, y: vw.frame.height - 5 )

        lblAziValue.textAlignment = .center
        lblAziValue.textColor = orangeColor
        lblAziValue.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        lblAziValue.text = resultAzimuth
        vw.addSubview(lblAziValue)
        
        lbl0 = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
 //       lbl0.center = CGPoint(x: vw.frame.width / 2, y: 10)
         lbl0.center = CGPoint(x: vw.frame.width / 2, y: 25)
        lbl0.textAlignment = .center
        lbl0.textColor = UIColor.white
//        lbl0.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        lbl0.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        vw.addSubview(lbl0)
        
        lbl90 = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
    //    lbl90.center = CGPoint(x: vw.frame.width - 10 , y: vw.frame.height / 2)
          lbl90.center = CGPoint(x: vw.frame.width - 35 , y: vw.frame.height / 2)
        lbl90.textAlignment = .center
        lbl90.textColor = UIColor.white
        lbl90.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        vw.addSubview(lbl90)
        
        lbl180 = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        //lbl180.center = CGPoint(x: vw.frame.width / 2, y: vw.frame.height - 20 )
         lbl180.center = CGPoint(x: vw.frame.width / 2, y: vw.frame.height - 35 )
        lbl180.textAlignment = .center
        lbl180.textColor = UIColor.white
        lbl180.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        vw.addSubview(lbl180)
        
        lbl270 = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
  //      lbl270.center = CGPoint(x: 5, y: vw.frame.height / 2 )
        lbl270.center = CGPoint(x: 25, y: vw.frame.height / 2 )
        lbl270.textAlignment = .center
        lbl270.textColor = UIColor.white
        lbl270.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        vw.addSubview(lbl270)
        
    }
    
    func labelAlignment1(_ vw: UIView){
        
        lblInclValue1 = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
        lblInclValue1.center = CGPoint(x: vw.frame.width / 2, y: vw.frame.height - 5)
        
        lblInclValue1.textAlignment = .center
        lblInclValue1.textColor = UIColor.white
        lblInclValue1.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
        lblInclValue1.text = currentInclination
        vw.addSubview(lblInclValue1)
        
        lblAziValue1 = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        lblAziValue1.center = CGPoint(x: vw
            .frame.width / 2, y: vw.frame.height / 2 )
       
        lblAziValue1.textAlignment = .center
        lblAziValue1.textColor = orangeColor
        lblAziValue1.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
        lblAziValue1.text = currentAzimuth
        vw.addSubview(lblAziValue1)
        
        lbl0_1 = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        lbl0_1.center = CGPoint(x: vw.frame.width / 2, y: 10)
        lbl0_1.textAlignment = .center
        lbl0_1.textColor = .white
        lbl0_1.textColor = UIColor.gray
        lbl0_1.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        vw.addSubview(lbl0_1)
        
        lbl90_1 = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        lbl90_1.center = CGPoint(x: vw.frame.width - 10 , y: vw.frame.height / 2)
        lbl90_1.textAlignment = .center
        lbl90_1.textColor = .white
        lbl90_1.textColor = UIColor.gray
        lbl90_1.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        vw.addSubview(lbl90_1)
        
        lbl180_1 = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        lbl180_1.center = CGPoint(x: vw.frame.width / 2, y: vw.frame.height - 20 )
        lbl180_1.textAlignment = .center
        lbl180_1.textColor = .white
        lbl180_1.textColor = UIColor.gray
        lbl180_1.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        vw.addSubview(lbl180_1)
        
        lbl270_1 = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        lbl270_1.center = CGPoint(x: 5, y: vw.frame.height / 2 )
        lbl270_1.textAlignment = .center
        lbl270_1.textColor = .white
        lbl270_1.textColor = UIColor.gray
        lbl270_1.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        vw.addSubview(lbl270_1)
    }
    
    //MARK: - gesture Recognizer mathods
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
          //  let tabBar = self.navigationController?.navigationController?.viewControllers[0] as? UITabBarController
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
              //   tabBar?.selectedIndex = 4
                tabBarController?.selectedIndex = 4
            self.navigationController?.popViewController(animated: true)
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
          //      tabBar?.selectedIndex = 1
                tabBarController?.selectedIndex = 1
                self.navigationController?.popViewController(animated: true)
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
        }
    
    //MARK: - Point from angle Custom method
    
    func pointFrom(angle: CGFloat, radius: CGFloat, offset: CGPoint) -> CGPoint {
     //   return CGPointMake(radius * cos(angle) + offset.x, radius * sin(angle) + offset.y)
        return CGPoint(x:offset.x, y: radius * sin(angle) + offset.y)
    }
    

    func circularLayer(_ vw: UIView ){
        
  //      let ceter = resultView.center
          let ceter = graphCenter!
    
//        let  circularPath = UIBezierPath(arcCenter: ceter, radius: vw.bounds.size.width / 2 - 60, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi , clockwise: true)
        let  circularPath = UIBezierPath(arcCenter: ceter, radius: graphFrame.size.width / 2 - 50, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi , clockwise: true)
        
   //     let endAngle = CGFloat((resultAzimuth as NSString).floatValue)
        
        var endAngle : CGFloat = 0
        if CGFloat((resultInclination as NSString).floatValue) > 0{
            endAngle = CGFloat((resultInclination as NSString).floatValue)
        }
        
        
        let circularPath1 = UIBezierPath(arcCenter: ceter, radius: graphFrame.size.width / 2 - 50, startAngle: -CGFloat.pi / 2, endAngle:((endAngle)*(CGFloat.pi/180.0) - (CGFloat.pi/2)) , clockwise: true)
        
    
//      let cen = CGPoint(x: resultView.center.x, y: resultView.frame.origin.y + 60)
        let cen = CGPoint(x: ceter.x, y: resultView.frame.origin.y + 50)
        
        
        let ballPath = UIBezierPath(arcCenter: cen, radius: 4, startAngle:-CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true)
        trackLay2.path = ballPath.cgPath
        trackLay2.lineWidth = 5
        trackLay2.strokeColor = UIColor.white.cgColor
        trackLay2.fillColor = UIColor.white.cgColor
        trackLay2.lineCap = kCALineCapRound
        vwMain.layer.addSublayer(trackLay2)
        
        
        trackLayer.path = circularPath.cgPath
        trackLayer.strokeColor = UIColor(red: 154.0/255.0, green: 154.0/255.0, blue: 154.0/255.0, alpha: 0.5).cgColor
        trackLayer.lineWidth = 5
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineCap = kCALineCapRound
        vwMain.layer.addSublayer(trackLayer)
        
        shapeLayer.path = circularPath1.cgPath
//      shapeLayer.strokeColor = orangeColor.cgColor
        shapeLayer.strokeColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 5
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = kCALineCapRound
        shapeLayer.strokeEnd = 0
        vwMain.layer.addSublayer(shapeLayer)
        
//        var endAngleIncl : CGFloat = 0
//        if CGFloat((resultInclination1 as NSString).floatValue) > 0{
//            endAngleIncl = CGFloat((resultInclination1 as NSString).floatValue)
//        }
        
          let endAngleIncl = CGFloat((resultAzimuth as NSString).floatValue)
        
//        let circularPathAzimuth = UIBezierPath(arcCenter: ceter, radius: vw.frame.width / 2 - 35, startAngle: -CGFloat.pi / 2, endAngle: ((endAngleIncl)*(CGFloat.pi/45.0) - (CGFloat.pi / 2)), clockwise: true)
        
//    let circularPathAzimuth = UIBezierPath(arcCenter: ceter, radius: vw.frame.width / 2 - 35, startAngle: -CGFloat.pi / 2, endAngle: ((endAngleIncl)*(CGFloat.pi/180.0) - (CGFloat.pi / 2)), clockwise: true)
        
        let circularPathAzimuth = UIBezierPath(arcCenter: ceter, radius: graphFrame.size.width / 2 - 30, startAngle: -CGFloat.pi / 2, endAngle: ((endAngleIncl)*(CGFloat.pi/180.0) - (CGFloat.pi / 2)), clockwise: true)
        
        
//      let cen1 = CGPoint(x: resultView.center.x, y: resultView.frame.origin.y + 35)
        let cen1 = CGPoint(x: graphCenter.x, y: resultView.frame.origin.y + 30)
        
        let ballPath1 = UIBezierPath(arcCenter: cen1, radius: 4, startAngle:-CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true)
        trackLay3.path = ballPath1.cgPath
//      trackLay3.strokeColor = UIColor.white.cgColor
//      trackLay3.fillColor = UIColor.white.cgColor
        trackLay3.strokeColor = orangeColor.cgColor
        trackLay3.fillColor = orangeColor.cgColor
        trackLay3.lineWidth = 5
        trackLay3.lineCap = kCALineCapRound
        vwMain.layer.addSublayer(trackLay3)
        
        
//        let  circularPathAzimuth2 = UIBezierPath(arcCenter: ceter, radius: vw.frame.width / 2 - 35, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi , clockwise: true)
        let  circularPathAzimuth2 = UIBezierPath(arcCenter: ceter, radius: graphFrame.size.width / 2 - 30, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi , clockwise: true)
        

//      let trackLayer1 = CAShapeLayer()
        trackLayer1.path = circularPathAzimuth2.cgPath
        trackLayer1.strokeColor = UIColor(red: 154.0/255.0, green: 154.0/255.0, blue: 154.0/255.0, alpha: 0.5).cgColor
        trackLayer1.lineWidth = 5
        trackLayer1.fillColor = UIColor.clear.cgColor
        trackLayer1.lineCap = kCALineCapRound
        vwMain.layer.addSublayer(trackLayer1)
        
        shapeLayerAzimuth.path = circularPathAzimuth.cgPath
//      shapeLayerAzimuth.strokeColor = UIColor.white.cgColor
        shapeLayerAzimuth.strokeColor = orangeColor.cgColor
        shapeLayerAzimuth.lineWidth = 5
        shapeLayerAzimuth.fillColor = UIColor.clear.cgColor
        shapeLayerAzimuth.lineCap = kCALineCapRound
        shapeLayerAzimuth.strokeEnd = 0
        vwMain.layer.addSublayer(shapeLayerAzimuth)
   
    }
    
    func circularLayer1(_ vw: UIView ){
        
        let ceter = previousResultView.center
        
        let trackLayer = CAShapeLayer()
        
        
        let  circularPath = UIBezierPath(arcCenter: ceter, radius: vw.frame.width / 2 - 50, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi , clockwise: true)
        
        
        let endAngle = CGFloat((currentAzimuth as NSString).floatValue)
        
        let circularPath1 = UIBezierPath(arcCenter: ceter, radius: vw.frame.width / 2 - 50, startAngle: -CGFloat.pi / 2, endAngle: ((endAngle)*(CGFloat.pi/180.0) - (CGFloat.pi / 2)) , clockwise: true)
        

        let cen = CGPoint(x: previousResultView.center.x, y: previousResultView.frame.origin.y + 50)
       
        
        let ballPath = UIBezierPath(arcCenter: cen, radius: 2, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true)
        trackLay.path = ballPath.cgPath
        trackLay.strokeColor = orangeColor.cgColor
        trackLay.lineWidth = 5
        trackLay.fillColor = orangeColor.cgColor
        trackLay.lineCap = kCALineCapRound
        vwMain.layer.addSublayer(trackLay)
        
        
        trackLayer.path = circularPath.cgPath
        trackLayer.strokeColor = UIColor(red: 154.0/255.0, green: 154.0/255.0, blue: 154.0/255.0, alpha: 0.5).cgColor
        trackLayer.lineWidth = 5
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineCap = kCALineCapRound
        vwMain.layer.addSublayer(trackLayer)
        
        shapeLayer1.path = circularPath1.cgPath
        shapeLayer1.strokeColor = orangeColor.cgColor
        shapeLayer1.lineWidth = 5
        shapeLayer1.fillColor = UIColor.clear.cgColor
        shapeLayer1.lineCap = kCALineCapRound
        shapeLayer1.strokeEnd = 0
        vwMain.layer.addSublayer(shapeLayer1)
        
        var endAngleIncl : CGFloat = 0
        if CGFloat((currentInclination as NSString).floatValue) > 0{
            endAngleIncl = CGFloat((currentInclination as NSString).floatValue)
        }
        let circularPathAzimuth = UIBezierPath(arcCenter: ceter, radius: vw.frame.width / 2 - 35, startAngle: -CGFloat.pi / 2, endAngle: ((endAngleIncl)*(CGFloat.pi/45.0)) - (CGFloat.pi / 2), clockwise: true)
        
        
        let cen1 = CGPoint(x: previousResultView.center.x, y: previousResultView.frame.origin.y + 35)
        
        
        let ballPath1 = UIBezierPath(arcCenter: cen1, radius: 2, startAngle:-CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true)
        //        trackLay = CAShapeLayer()
        trackLay1.path = ballPath1.cgPath
        trackLay1.strokeColor = UIColor.white.cgColor
        trackLay1.lineWidth = 5
        trackLay1.fillColor = UIColor.white.cgColor
        trackLay1.lineCap = kCALineCapRound
        vwMain.layer.addSublayer(trackLay1)
        
        let  circularPathAzimuth2 = UIBezierPath(arcCenter: ceter, radius: vw.frame.width / 2 - 35, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi , clockwise: true)
        let trackLayer1 = CAShapeLayer()
        trackLayer1.path = circularPathAzimuth2.cgPath
        trackLayer1.strokeColor = UIColor(red: 154.0/255.0, green: 154.0/255.0, blue: 154.0/255.0, alpha: 0.5).cgColor
        trackLayer1.lineWidth = 5
        trackLayer1.fillColor = UIColor.clear.cgColor
        trackLayer1.lineCap = kCALineCapRound
        vwMain.layer.addSublayer(trackLayer1)
        
        shapeLayerAzimuth1.path = circularPathAzimuth.cgPath
        shapeLayerAzimuth1.strokeColor = UIColor.white.cgColor
        shapeLayerAzimuth1.lineWidth = 5
        shapeLayerAzimuth1.fillColor = UIColor.clear.cgColor
        shapeLayerAzimuth1.lineCap = kCALineCapRound
        shapeLayerAzimuth1.strokeEnd = 0
        vwMain.layer.addSublayer(shapeLayerAzimuth1)
        
        previousResultView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap1)))
        
        resultView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if amountYield_lbl.text == "" || motorYield_lbl.text == "" || currentInclination_lbl.text == "" || currentAzimuth_lbl.text == "" || toolface_lbl.text == ""{
//
//
//        }
//        else{
        
           amountYield = amountYield_lbl.text ??  ""
            motorYield = motorYield_lbl.text ??  ""
            currentInclination = currentInclination_lbl.text ?? ""
            currentAzimuth = currentAzimuth_lbl.text ?? ""
            toolface = toolface_lbl.text ?? ""
            
            switch textField{
            case amountYield_lbl:
                if string == ""{
                    amountYield.removeLast()
                }else{
                amountYield = amountYield + string
                }
            case motorYield_lbl:
                if string == ""{
                    motorYield.removeLast()
                }else{
                motorYield = motorYield + string
                }
            case currentInclination_lbl:
                if string == ""{
                    currentInclination.removeLast()
                    }else{
                currentInclination = currentInclination + string
                    }
            case currentAzimuth_lbl:
                    if string == ""{
                    currentAzimuth.removeLast()
                    }else{
                currentAzimuth = currentAzimuth + string
                    }
            case toolface_lbl:
                    if string == ""{
                    toolface.removeLast()
                    }else if string == "-"{
                    toolface = string + toolface
                    }
                    else{
                    toolface = toolface + string
                    }
            default:
                print("")
            }
            
            if  amountYield != "" && Double(amountYield)! > 1000 {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter amount slid value less than 1000", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if motorYield != "" && Double(motorYield)! > 40 {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter motor slid value less than 40", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else if currentAzimuth != "" && Double(currentAzimuth)! >= 360 {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter azimuth value less than 360", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
   //     }
        if textField == toolface_lbl{
            
            if toolface != "-" , string.containsValidCharacter{
                calculate()
                //    removeLayersFromView()
                //   circularLayer(resultView)
                drawValues()
                handleTap()
            }
            guard string != "" else { return true }
            return string.containsValidCharacter
        }else{
        return true
            
        }
    }
    
    @available(iOS 10.0, *)
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        
        switch textField{
        case amountYield_lbl:
            if  amountYield == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Feet slid value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if Double(amountYield)! > 1000 {
            
            }
            else{
                calculate()
//                removeLayersFromView()
//                circularLayer(resultView)
                drawValues()
                handleTap()
            }
            
        case motorYield_lbl:
            
            if motorYield == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter motor yield value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if Double(motorYield)! > 40 {
                
            }
            else{
                calculate()
//                removeLayersFromView()
//                circularLayer(resultView)
                drawValues()
                handleTap()
            }
            
        case currentInclination_lbl:
            if currentInclination == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter current inclination value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            
        case currentAzimuth_lbl:
            if currentAzimuth == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter current azimuth value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
          else if Double(currentAzimuth)! >= 360{
                
            }
            else{
                calculate()
//                removeLayersFromView()
//                circularLayer(resultView)
                drawValues()
                handleTap()
            }
        case toolface_lbl:
            if toolface == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter toolface value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
//            else if toolface.first == "-" {
//                let tmp = toolface.dropFirst()
//                let resultValue = 360 - Double(tmp)!
//                toolface = String(resultValue)
//                print(resultValue)
//                toolface_lbl.text = toolface
//                calculate()
////                removeLayersFromView()
////                circularLayer(resultView)
//                drawValues()
//                handleTap()
//            }
            else{
                if toolface != "-",textField.text?.containsValidCharacter ?? false{
                calculate()
               // removeLayersFromView()
               // circularLayer(resultView)
                drawValues()
                handleTap()
                }
            }
        default:
            print("")
        }
    }
    func removeLayersFromView(){
        trackLay2.removeFromSuperlayer()
        shapeLayer.removeFromSuperlayer()
        trackLay1.removeFromSuperlayer()
        shapeLayerAzimuth.removeFromSuperlayer()
        trackLayer.removeFromSuperlayer()
        trackLayer1.removeFromSuperlayer()
    }
    
    @objc private func handleTap() {
     //   print("Attempting to animate stroke")
        
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.toValue = 1
        basicAnimation.duration = 2
        basicAnimation.fillMode = kCAFillModeForwards
        basicAnimation.isRemovedOnCompletion = false
        shapeLayer.add(basicAnimation, forKey: "urSoBasic")
        shapeLayerAzimuth.add(basicAnimation, forKey: "azimuthBasic")
    }
    @objc private func handleTap1() {
      //  print("Attempting to animate stroke")
        
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.toValue = 1
        basicAnimation.duration = 4
        basicAnimation.fillMode = kCAFillModeForwards
        basicAnimation.isRemovedOnCompletion = false
        shapeLayer1.add(basicAnimation, forKey: "urSoBasic")
        shapeLayerAzimuth1.add(basicAnimation, forKey: "azimuthBasic")
    }
    func assignbackground(){
        let background = UIImage(named: "background")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
     @IBAction func calculate(_ sender: UIButton) {
        
        if amountYield_lbl.text == "" || motorYield_lbl.text == "" || currentInclination_lbl.text == "" || currentAzimuth_lbl.text == "" || toolface_lbl.text == ""{
            
            let alert = UIAlertController(title:"Alert", message:"Please fill all the fields", preferredStyle: .alert)
            let action = UIAlertAction(title:"OK", style: .default)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        else{
        
            amountYield = amountYield_lbl.text!
            motorYield = motorYield_lbl.text!
            currentInclination = currentInclination_lbl.text!
            currentAzimuth = currentAzimuth_lbl.text!
            toolface = toolface_lbl.text!
            calculate()
            
        }
    }
    
    func calculate() {
        
        var motorYield1 = motorYield
        
        if(((amountYield.count) != 0) && ((motorYield.count) != 0))  {
            // Dogleg Per CL Result
            CalculateCondition = 2
            motorYield1 = "\(Double(amountYield)! * Double(motorYield)! / 100)"
        }
        
        if (motorYield1.count) != 0 && (currentInclination.count) != 0 && (toolface.count) != 0 && (currentAzimuth.count) != 0 {
            //Inclination Result
            CalculateCondition = 2
            let DoglegPerCL = Double(motorYield1)
            var toolfacecondition = Double(toolface)
            
            //edit
            if 180 ... 360 ~= toolfacecondition! || toolfacecondition! < 0 {
                toolfacecondition = Double(currentAzimuth)! - toolfacecondition!
            }
            
            
            let Inclination = rad2deg(cos(deg2rad(toolfacecondition!)) * deg2rad(DoglegPerCL!)) + Double(currentInclination)!
            resultInclinationVal = Float(Inclination)
            resultInclination = String(format:"%.2f", Inclination)
            lblInclValue.text = resultInclination
        }
        
        if(((resultInclination.count) != 0) && ((currentInclination.count) != 0))  {
            //Inclination Result 2
            CalculateCondition = 2
           resultInclination1 = String(format:"%.2f", Double(resultInclination)! - Double(currentInclination)!)
            resultInclinationVal1 = Float(Double(resultInclination)! - Double(currentInclination)!)
           // lblInclValue.text = resultInclination1
            lblInclValue1.text = currentInclination
        }
        
        if(((motorYield1.count) != 0) && ((toolface.count) != 0) && ((currentInclination.count) != 0) && ((resultInclination.count) != 0) && ((currentAzimuth.count) != 0))  {
            //Azimuth Result 2
            CalculateCondition = 2
            let DoglegPerCL = Double(motorYield1)
            var toolfacecondition = Double(toolface)
            
            //Edit
            if 180 ... 360 ~= toolfacecondition! || toolfacecondition! < 0 {
                toolfacecondition = Double(currentAzimuth)! - toolfacecondition!
            }
            
            if(toolfacecondition == 0){
                //resultAzimuth = "0"
                resultAzimuth1 = "0"
            }
            else {
                if(toolfacecondition == 180){
                    resultAzimuth1 = "0"
                }
                else {
                    let inclination = Double(currentInclination)
                    let inclinationresult1 = Double(resultInclination)
                    if(toolfacecondition! < 0) {
                        let azimuthresult2 = (-rad2deg(acos((cos(deg2rad(DoglegPerCL!))-(cos(deg2rad(inclination!))*cos(deg2rad(Double(inclinationresult1!)))))/(sin(deg2rad(inclination!))*sin(deg2rad(inclinationresult1!))))))
                        
                        resultAzimuth1 = String(format:"%.2f", azimuthresult2)
                }
                    else {
                        let azimuthresult2 = rad2deg(acos((cos(deg2rad(DoglegPerCL!))-(cos(deg2rad(inclination!))*cos(deg2rad(inclinationresult1!))))/(sin(deg2rad(inclination!))*sin(deg2rad(inclinationresult1!)))))
                        
                        resultAzimuth1 = String(format:"%.2f", azimuthresult2)
                    }
                }
            }
        }
        
        if(((currentAzimuth.count) != 0) && ((resultAzimuth1.count) != 0))  {
            //Azimuth result 1
            CalculateCondition = 2
            print(Double(currentAzimuth)! + Double(resultAzimuth1)!)
            let aziresult1 = Double(currentAzimuth)! + Double(resultAzimuth1)!
            if(aziresult1 < 0) {
               
                resultAzimuth =  String(format:"%.2f", Double(currentAzimuth)! + Double(resultAzimuth1)! + 360)
                resultAzimuthVal = Float(Double(Double(currentAzimuth)! + Double(resultAzimuth1)! + 360))
                lblAziValue.text = resultAzimuth
            }
            else {
                if(aziresult1 > 360){
//                    resultAzimuth = "\(Double(currentAzimuth)! + Double(resultAzimuth1)! - 360)"
                    resultAzimuth = String(format: "%.2f", Double(currentAzimuth)! + Double(resultAzimuth1)! - 360)
                    resultAzimuthVal = Float(Double(Double(currentAzimuth)! + Double(resultAzimuth1)! - 360))
                    lblAziValue.text = resultAzimuth
                }
                else {
                  resultAzimuth =  String(format:"%.2f", Double(currentAzimuth)! + Double(resultAzimuth1)!)
                    resultAzimuthVal1 = Float(Double(currentAzimuth)!)
//                    lblAziValue1.text = currentAzimuth
                    lblAziValue.text = resultAzimuth
                }
            }
            lblAziValue1.text = currentAzimuth
        }
        
//        if(CalculateCondition == 1){
//            self.popupAlert()
//
//            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ResultsViewController") as? ResultFinalViewController
//            vc?.amountYield = amountYield
//            vc?.motorYield = motorYield
//            vc?.currentInclination = currentInclination
//            vc?.currentAzimuth = currentAzimuth
//            vc?.toolface = toolface
//            vc?.resultAzimuth = resultAzimuth
//            vc?.resultInclination = resultInclination
//            vc?.resultAzimuth1 = resultAzimuth1
//            vc?.resultInclination = resultInclination
//            self.navigationController?.pushViewController(vc!, animated: true)
//        }
    }
    
    @IBAction func feetSlidAction(_ sender: Any) {
        
        back =  1
        backVal = 1
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func motorYieldAction(_ sender: Any) {
        back =  1
        backVal = 2
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func currentIncAction(_ sender: Any) {
        
        back =  1
        backVal = 3
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func currentAzAction(_ sender: Any) {
        
        back =  1
        backVal = 4
        
         self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func toolfaceAction(_ sender: Any) {
        
        back =  1
        backVal = 5
        
         self.dismiss(animated: true, completion: nil)
    }
    
    
}

extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}
