//
//  OuijaVC.swift
//  Samplesheet1
//
//  Created by Sankar on 29/5/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit

class OuijaVC: UIViewController {

    @IBOutlet var ViewDoglogResponse: UIView!
    @IBOutlet var TxtfieldDoglegperCL: GSBaseTextField!
    @IBOutlet var TxtfieldAzimuthResult2: GSBaseTextField!
    @IBOutlet var TxtfieldAzimuthResult1: GSBaseTextField!
    @IBOutlet var TxtfieldInclinationResult2: GSBaseTextField!
    @IBOutlet var TxtfieldInclinationResult1: GSBaseTextField!
    @IBOutlet var TxtfieldToolface: GSBaseTextField!
    @IBOutlet var TxtfieldAzimuth: GSBaseTextField!
    @IBOutlet var TxtfieldInclination: GSBaseTextField!
    @IBOutlet var TxtfieldMotorDogleg: GSBaseTextField!
    @IBOutlet var TxtfieldSlidelength: GSBaseTextField!
    @IBOutlet var Scrollview: UIScrollView!
    var CalculateCondition : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CalculateCondition = 1
       self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        Scrollview.contentSize = CGSize(width:Scrollview.frame.size.width , height: ViewDoglogResponse.frame.origin.y+ViewDoglogResponse.frame.size.height+90)
        Scrollview.showsVerticalScrollIndicator = false
        Scrollview.showsHorizontalScrollIndicator = false
        
        
 
    }
    @IBAction func ActionCalculation(_ sender: Any) {
        if(((TxtfieldSlidelength.text?.count) != 0) && ((TxtfieldMotorDogleg.text?.count) != 0))  {
        // Dogleg Per CL Result
            CalculateCondition = 2
        TxtfieldDoglegperCL.text = "\(Double(TxtfieldSlidelength.text!)! * Double(TxtfieldMotorDogleg.text!)! / 100)"
        }
        
        if(((TxtfieldDoglegperCL.text?.count) != 0) && ((TxtfieldInclination.text?.count) != 0))  {
        //Inclination Result
            CalculateCondition = 2
         let DoglegPerCL = Double(TxtfieldDoglegperCL.text!)
         let toolfacecondition = Double(TxtfieldToolface.text!)
            let Inclination = rad2deg(cos(deg2rad(toolfacecondition!)) * deg2rad(DoglegPerCL!)) + Double(TxtfieldInclination.text!)!
         TxtfieldInclinationResult1.text = "\(Inclination)"
        }
        
        if(((TxtfieldInclinationResult1.text?.count) != 0) && ((TxtfieldInclination.text?.count) != 0))  {
        //Inclination Result 2
            CalculateCondition = 2
        TxtfieldInclinationResult2.text = "\(Double(TxtfieldInclinationResult1.text!)! - Double(TxtfieldInclination.text!)!)"
        }
        
        if(((TxtfieldDoglegperCL.text?.count) != 0) && ((TxtfieldToolface.text?.count) != 0) && ((TxtfieldInclination.text?.count) != 0) && ((TxtfieldInclinationResult1.text?.count) != 0))  {
        //Azimuth Result 2
            CalculateCondition = 2
        let DoglegPerCL = Double(TxtfieldDoglegperCL.text!)
        let toolfacecondition = Double(TxtfieldToolface.text!)
        if(toolfacecondition == 0){
            TxtfieldAzimuthResult2.text = "\(0)"
        }
        else {
            if(toolfacecondition == 180){
                TxtfieldAzimuthResult2.text = "\(0)"
            }
            else {
                let inclination = Double(TxtfieldInclination.text!)
                let inclinationresult1 = Double(TxtfieldInclinationResult1.text!)
                if(toolfacecondition! < 0) {
                    let azimuthresult2 = (-rad2deg(acos((cos(deg2rad(DoglegPerCL!))-(cos(deg2rad(inclination!))*cos(deg2rad(Double(inclinationresult1!)))))/(sin(deg2rad(inclination!))*sin(deg2rad(inclinationresult1!))))))
                    TxtfieldAzimuthResult2.text = "\(azimuthresult2)"
                    print(TxtfieldAzimuthResult2.text ?? "")
                }
                else {
                    let azimuthresult2 = rad2deg(acos((cos(deg2rad(DoglegPerCL!))-(cos(deg2rad(inclination!))*cos(deg2rad(inclinationresult1!))))/(sin(deg2rad(inclination!))*sin(deg2rad(inclinationresult1!)))))
                    
                    // =(DEGREES(ACOS((COS(RADIANS(5.04))-(COS(RADIANS(10))*COS(RADIANS(15.039))))/(SIN(RADIANS(10))*SIN(RADIANS(15.039))))))
                    TxtfieldAzimuthResult2.text = "\(azimuthresult2)"
                }
            }
        }
        }
       
    if(((TxtfieldAzimuth.text?.count) != 0) && ((TxtfieldAzimuthResult2.text?.count) != 0))  {
     //Azimuth result 1
        CalculateCondition = 2
        print(Double(TxtfieldAzimuth.text!)! + Double(TxtfieldAzimuthResult2.text!)!)
      let aziresult1 = Double(TxtfieldAzimuth.text!)! + Double(TxtfieldAzimuthResult2.text!)!
        if(aziresult1 < 0) {
            TxtfieldAzimuthResult1.text = "\(Double(TxtfieldAzimuth.text!)! + Double(TxtfieldAzimuthResult2.text!)! + 360)"
        }
        else {
            if(aziresult1 > 360){
                 TxtfieldAzimuthResult1.text = "\(Double(TxtfieldAzimuth.text!)! + Double(TxtfieldAzimuthResult2.text!)! - 360)"
            }
            else {
                TxtfieldAzimuthResult1.text = "\(Double(TxtfieldAzimuth.text!)! + Double(TxtfieldAzimuthResult2.text!)!)"
            }
        }
        }
        
        if(CalculateCondition == 1){
            self.popupAlert()
        }
    }
    
    //MARK: TextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        view.endEditing(true)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        let maxLength = 5
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength && string == numberFiltered
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
