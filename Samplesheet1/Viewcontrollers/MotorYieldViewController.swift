//
//  MotorYieldViewController.swift
//  Samplesheet1
//
//  Created by Sankar on 31/8/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit

class MotorYieldViewController: UIViewController {

    @IBOutlet var vwTextFiels: UIView!
    @IBOutlet var txtAmount: UITextField!
    @IBOutlet var lblQuestion: UILabel!
    var amountYield = String()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        uiEliments()
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        view.addGestureRecognizer(leftSwipe)
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        rightSwipe.direction = .right
        view.addGestureRecognizer(rightSwipe)
        
    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        
        if (sender.direction == .right) {
            print("Swipe Right")
            self.navigationController?.popViewController(animated: true)
        }
        
        if (sender.direction == .left) {
            print("Swipe Left")
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CurrentInclinationViewController") as? CurrentInclinationViewController
            vc?.amountYield = amountYield
            vc?.motorYield = txtAmount.text!
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }
 
    
    }
    
    func uiEliments(){
        
        txtAmount.textAlignment = .center
        
        // border radius
        vwTextFiels.layer.cornerRadius = 15.0
        
        // border
        vwTextFiels.layer.borderColor = UIColor.lightGray.cgColor
        vwTextFiels.layer.borderWidth = 1.5
        
        // drop shadow
        vwTextFiels.layer.shadowColor = UIColor.black.cgColor
        vwTextFiels.layer.shadowOpacity = 0.8
        vwTextFiels.layer.shadowRadius = 3.0
        vwTextFiels.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
