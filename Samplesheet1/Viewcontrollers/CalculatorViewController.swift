//
//  CalculatorViewController.swift
//  Samplesheet1
//
//  Created by Ratheesh TR on 11/10/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UIScrollViewDelegate,UICollectionViewDelegateFlowLayout{

    @IBOutlet var calcButtons: [UIButton]!
    let titleText = ["Amount slid?","Motor yield?","Current Inc?","Current Az?","Toolface? +/- "]
    
    @IBOutlet weak var mainCollectionView: UICollectionView!
  
  @IBOutlet weak var pageControl: UIPageControl!
    
    
    var isIncreasing = true
   // @IBOutlet weak var mainStackView: UIStackView!
    var operation = false
    var changingSign = false
    var curretPage:Int = 0
    var slideValue = slideVlaues()
    var slide = 0
    
    override func viewWillAppear(_ animated: Bool) {
//        if back == 1{
//            let currentIndex = mainCollectionView.contentOffset.x / mainCollectionView.frame.size.width;
//            print(Int(currentIndex))
//            let indexPath = IndexPath(item: Int(backVal - 1), section: 0)
//
//            pageControl.currentPage = NSInteger(ceil(currentIndex))
//
//            mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: false)
//            if let cell = mainCollectionView.cellForItem(at: indexPath) as? CalculatorCollectionViewCell{
//
//            switch backVal {
//            case 1:
//                cell.valueLbl.text = slideValue.amoundslideValue
//            case 2:
//                cell.valueLbl.text = slideValue.motorYieldValue
//            case 3:
//                cell.valueLbl.text = slideValue.currenIncValue
//            case 4:
//                cell.valueLbl.text = slideValue.currentAzimuthValue
//            case 5:
//                cell.valueLbl.text = slideValue.toolFaceValue
//
//            default:
//                cell.valueLbl.text = ""
//            }
//            }
//            back = 0
//        }
        
        self.tabBarController?.tabBar.isHidden = false
        slideValue.amoundslideValue = Constants.appdelegateSharedInstance.initialDict[Constants.amountSlid] ?? ""
 //       slideValue.motorYieldValue = getValue(key: Constants.motorYield) ?? ""
        slideValue.motorYieldValue  = Constants.appdelegateSharedInstance.initialDict[Constants.motorYield] ?? ""
        slideValue.currenIncValue = Constants.appdelegateSharedInstance.initialDict[Constants.currentInclination] ?? ""
        slideValue.currentAzimuthValue = Constants.appdelegateSharedInstance.initialDict[Constants.currentAzimuth] ?? ""
        slideValue.toolFaceValue = Constants.appdelegateSharedInstance.initialDict[Constants.toolFace] ?? ""
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func calcButtonTapped(_ sender: UIButton) {

        let buttonposition = sender.convert(CGPoint.zero, to: mainCollectionView)
        let index = mainCollectionView.indexPathForItem(at: buttonposition)
    
        switch sender.tag {

        case 0:
            Addnumberfunc("0", index!)
        case 1:
            Addnumberfunc("1", index!)
        case 2:
            Addnumberfunc("2", index!)
        case 3:
            Addnumberfunc("3", index!)
        case 4:
            Addnumberfunc("4", index!)
        case 5:
            Addnumberfunc("5", index!)
        case 6:
            Addnumberfunc("6", index!)
        case 7:
            Addnumberfunc("7", index!)
        case 8:
            Addnumberfunc("8", index!)
        case 9:
            Addnumberfunc("9", index!)
        case 11:
            decimalPointPressed(index!)
        case 10:
            plusMinusAction(index!)
        default:
            print("dfgdfg")
        }
    }
    
    func decimalPointPressed(_ index:IndexPath)
    {
        let cell = mainCollectionView.cellForItem(at: index) as! CalculatorCollectionViewCell
        guard let text = cell.valueLbl.text, !text.contains(".") else {
            return }
        if cell.valueLbl.text?.count == 0
        {
          cell.valueLbl.text = "0."
        }
        else
        {
           cell.valueLbl.text = text + "."
        }
    }
    
    @IBAction func acaction(_ sender: UIButton) {
        let buttonposition = sender.convert(CGPoint.zero, to: mainCollectionView)
        let index = mainCollectionView.indexPathForItem(at: buttonposition)
        let cell = mainCollectionView.cellForItem(at: index!) as! CalculatorCollectionViewCell
        
        
        switch index?.item {
        case 0:
            slideValue.amoundslideValue = nil
            Constants.appdelegateSharedInstance.initialDict[Constants.amountSlid] = nil
            
        case 1:
            slideValue.motorYieldValue = nil
         //   storeValue(val:"", Key: Constants.motorYield)
            Constants.appdelegateSharedInstance.initialDict[Constants.motorYield] = nil
        case 2:
            slideValue.currenIncValue = nil
            Constants.appdelegateSharedInstance.initialDict[Constants.currentInclination] = nil
        case 3:
            slideValue.currentAzimuthValue = nil
            Constants.appdelegateSharedInstance.initialDict[Constants.currentAzimuth] = nil
        case 4:
            slideValue.toolFaceValue = nil
            Constants.appdelegateSharedInstance.initialDict[Constants.toolFace] = nil
        default:
            print("default")
        }
        cell.valueLbl.text = ""
    }
    
    @IBAction func bsaction(_ sender: UIButton) {
        let buttonposition = sender.convert(CGPoint.zero, to: mainCollectionView)
        let index = mainCollectionView.indexPathForItem(at: buttonposition)

        let cell = mainCollectionView.cellForItem(at: index!) as! CalculatorCollectionViewCell
        
        if cell.valueLbl.text != ""
        {
            cell.valueLbl.text?.removeLast()
            setvalues(cell.valueLbl.text!, index!)
        }
        else {
            setvalues("", index!)
        }
    }
    
    func plusMinusAction(_ index:IndexPath)
    {
        let cell = mainCollectionView.cellForItem(at: index) as! CalculatorCollectionViewCell
        
        if (cell.valueLbl.text == "") {
            return
        } else {
            if !changingSign {
                changingSign = true
                cell.valueLbl.text = "-" + cell.valueLbl.text!
            } else {
                changingSign = false
                cell.valueLbl.text?.removeFirst()
            }
        }
         setvalues(cell.valueLbl.text!, index)
    }
    
    @IBAction func percentageAction(_ sender: Any) {

        
    }
    
    func Addnumberfunc(_ number:String, _ index:IndexPath)
    {
    
       let cell = mainCollectionView.cellForItem(at: index) as! CalculatorCollectionViewCell
        var textnum = ""
        if let num = cell.valueLbl.text{
            
     //   textnum = String(cell.valueLbl.text!)
            textnum = num
        }
        if operation {

            textnum = ""
            operation = false
        }
        textnum = textnum + number

        cell.valueLbl.text = textnum
        cell.valueLbl.layer.borderWidth = 0.6
//        cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
        cell.valueLbl.layer.masksToBounds = true
        
        setvalues(textnum,index)
    }


    func setvalues(_ textnum : String ,_ index:IndexPath){
        switch index.item {
        case 0:
            slideValue.amoundslideValue = textnum
            Constants.appdelegateSharedInstance.initialDict[Constants.amountSlid] = textnum
            
        case 1:
            slideValue.motorYieldValue = textnum
//            storeValue(val: textnum, Key: Constants.motorYield)
            Constants.appdelegateSharedInstance.initialDict[Constants.motorYield] = textnum
        case 2:
            slideValue.currenIncValue = textnum
            Constants.appdelegateSharedInstance.initialDict[Constants.currentInclination] = textnum
            
        case 3:
            slideValue.currentAzimuthValue = textnum
            Constants.appdelegateSharedInstance.initialDict[Constants.currentAzimuth] = textnum
        case 4:
//            if textnum.first == "-" {
//                let tmp = textnum.dropFirst()
//                let resultValue = 360 - Int(tmp)!
//                slideValue.toolFaceValue = String(resultValue)
//                print(resultValue)
//            }else{
                slideValue.toolFaceValue = textnum
            Constants.appdelegateSharedInstance.initialDict[Constants.toolFace] = textnum
      //      }
            
            
        default:
            print("default")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CalculatorCollectionViewCell
        cell.enterBtn.tag = indexPath.item
        cell.titleLbl.text = titleText[indexPath.item]
        
        if indexPath.item == 4{
            cell.signBtn.isEnabled = true
        }
        else{
            cell.signBtn.isEnabled = false
        }
        cell.valueLbl.layer.borderWidth = 0
        cell.valueLbl.layer.borderColor = UIColor.clear.cgColor
        cell.valueLbl.layer.masksToBounds = true
        
        switch indexPath.item {
        case 0:
            if let val = slideValue.amoundslideValue{
                cell.valueLbl.text = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
        case 1:
            if let val = slideValue.motorYieldValue{
                cell.valueLbl.text = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
        case 2:
            if let val = slideValue.currenIncValue{
                cell.valueLbl.text = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
        case 3:
            if let val = slideValue.currentAzimuthValue{
                cell.valueLbl.text = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
        case 4:
            if let val = slideValue.toolFaceValue{
                cell.valueLbl.text = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
            
        default:
            cell.titleLbl.text = ""
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        
        return CGSize(width: mainCollectionView.frame.width, height: mainCollectionView.frame.height)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    @IBAction func enterAction(_ sender: UIButton) {
        
        let currentIndex = mainCollectionView.contentOffset.x / mainCollectionView.frame.size.width;
        print(Int(currentIndex))
        let indexPath = IndexPath(item: Int(currentIndex + 1), section: 0)
        pageControl.currentPage = NSInteger(ceil(currentIndex))
       
        let indexPath1 = IndexPath(item: Int(currentIndex), section: 0)
        let cell = mainCollectionView.cellForItem(at: indexPath1) as! CalculatorCollectionViewCell
        switch sender.tag {
        case 0:
             mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
             if let val = slideValue.motorYieldValue {
                cell.valueLbl.text = val
             }
             else{
                cell.valueLbl.text = ""
             }
           // print(cell.titleLbl.text!)
        case 1:
             mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
             if let val = slideValue.currenIncValue {
                cell.valueLbl.text = val
             }
             else{
                cell.valueLbl.text = ""
             }
         //   print(cell.titleLbl.text!)
        case 2:
             mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
        //    print(cell.titleLbl.text!)
             if let val = slideValue.currentAzimuthValue {
                cell.valueLbl.text = val
            }
             else{
                cell.valueLbl.text = ""
            }
        case 3:
             mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
       //     print(cell.titleLbl.text!)
             if let val = slideValue.toolFaceValue {
                cell.valueLbl.text = val
            }
             else{
                cell.valueLbl.text = ""
            }
        case 4:
            if (slideValue.toolFaceValue == nil && slideValue.amoundslideValue == nil && slideValue.motorYieldValue == nil && slideValue.currenIncValue == nil && slideValue.currentAzimuthValue == nil) || (slideValue.toolFaceValue == "" && slideValue.amoundslideValue == "" && slideValue.motorYieldValue == "" && slideValue.currenIncValue == "" && slideValue.currentAzimuthValue == ""){
                
                let alert = UIAlertController(title:"Alert", message:"Please fill all the fields", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else if slideValue.amoundslideValue == nil || slideValue.amoundslideValue == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter amount slid value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
                //                let alert = UIAlertController(title:"Alert", message:"Please fill all the fields", preferredStyle: .alert)
                //                let action = UIAlertAction(title:"OK", style: .default)
                //                alert.addAction(action)
                //                self.present(alert, animated: true, completion: nil)
            }
            else if slideValue.motorYieldValue == nil || slideValue.motorYieldValue == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter motor yield value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if slideValue.currenIncValue == nil || slideValue.currenIncValue == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter current inclination value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if slideValue.currentAzimuthValue == nil || slideValue.currentAzimuthValue == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter current azimuth value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else if slideValue.toolFaceValue == nil || slideValue.toolFaceValue == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter toolface value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else if Double(slideValue.amoundslideValue!)! > 999 {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter amount slid value less than 1000", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else if Double(slideValue.motorYieldValue!)! > 39 {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter motor slid value less than 40", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else if Double(slideValue.currentAzimuthValue!)! > 359 {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter azimuth value less than 360", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ResultFinalViewController") as? ResultFinalViewController
                
//                if slideValue.toolFaceValue!.first == "-" {
//                    let tmp = slideValue.toolFaceValue!.dropFirst()
//                    let resultValue = 360 - Int(tmp)!
//                    slideValue.toolFaceValue = String(resultValue)
//                    print(resultValue)
//                }  
                
                vc?.amountYield = slideValue.amoundslideValue!
                vc?.motorYield = slideValue.motorYieldValue!
                vc?.currentInclination = slideValue.currenIncValue!
                vc?.currentAzimuth = slideValue.currentAzimuthValue!
                vc?.toolface = slideValue.toolFaceValue!
                
               // self.present(vc!, animated: true, completion: nil)
                self.navigationController?.pushViewController(vc!, animated: true)
                slide = 0
            }
        default:
            print(cell.titleLbl.text!)
            
        }
        
       // print("Current IndexPath:\(indexPath)")
    }
 
    //MARK: Page Controller methods
    

    
    @IBAction func currentPageIndexMethod(_ sender: Any) {
        let currentIndex = mainCollectionView.contentOffset.x / mainCollectionView.frame.size.width
        print(Int(currentIndex))
        
        if isIncreasing  && currentIndex <= 4{
            
        let indexPath1 = IndexPath(item: Int(currentIndex), section: 0)
        let cell = mainCollectionView.cellForItem(at: indexPath1) as! CalculatorCollectionViewCell
    
        self.enterAction(cell.enterBtn)
            if currentIndex == 4{
                isIncreasing = false
            }
        }
        else{
            if currentIndex > 0 {
            let indexPath = IndexPath(item: Int(currentIndex - 1), section: 0)
            let indexPath1 = IndexPath(item: Int(currentIndex), section: 0)
          // pageControl.currentPage = NSInteger(ceil(currentIndex - 1))
            let cell = mainCollectionView.cellForItem(at: indexPath1) as! CalculatorCollectionViewCell
                switch currentIndex - 1 {
                case 0:
                    mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
                    if let val = slideValue.motorYieldValue {
                        cell.valueLbl.text = val
                    }
                    else{
                        cell.valueLbl.text = ""
                    }
                case 1:
                    mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
                    if let val = slideValue.currenIncValue {
                        cell.valueLbl.text = val
                    }
                    else{
                        cell.valueLbl.text = ""
                    }
                case 2:
                    mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
                    if let val = slideValue.currentAzimuthValue {
                        cell.valueLbl.text = val
                    }
                    else{
                        cell.valueLbl.text = ""
                    }
                case 3:
                    mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
                    if let val = slideValue.toolFaceValue {
                        cell.valueLbl.text = val
                    }
                    else{
                        cell.valueLbl.text = ""
                    }
                case 4:
                 print(cell.titleLbl.text!)
                default:
                print(cell.titleLbl.text!)
                }
            }
            else{
                isIncreasing = true
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let currentIndex = mainCollectionView.contentOffset.x / mainCollectionView.frame.size.width
        print(Int(currentIndex))
        pageControl.currentPage = NSInteger(ceil(currentIndex))
     }
 
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = mainCollectionView.contentOffset
        visibleRect.size = mainCollectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = mainCollectionView.indexPathForItem(at: visiblePoint) else {
            return
        }
        let cell = mainCollectionView.cellForItem(at: indexPath) as! CalculatorCollectionViewCell
        
        switch indexPath.item {
        case 0:
            cell.valueLbl.text =  slideValue.amoundslideValue
            print(cell.titleLbl.text!)
        case 1:
            cell.valueLbl.text =  slideValue.motorYieldValue
            print(cell.titleLbl.text!)
        case 2:
            cell.valueLbl.text =  slideValue.currenIncValue
            print(cell.titleLbl.text!)
        case 3:
            cell.valueLbl.text =  slideValue.currentAzimuthValue
            print(cell.titleLbl.text!)
            slide = 0
        case 4:
            if slide == 1{
            if (slideValue.toolFaceValue == nil && slideValue.amoundslideValue == nil && slideValue.motorYieldValue == nil && slideValue.currenIncValue == nil && slideValue.currentAzimuthValue == nil) || (slideValue.toolFaceValue == "" && slideValue.amoundslideValue == "" && slideValue.motorYieldValue == "" && slideValue.currenIncValue == "" && slideValue.currentAzimuthValue == ""){
                
                let alert = UIAlertController(title:"Alert", message:"Please fill all the fields", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
//                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ResultFinalViewController") as? ResultFinalViewController
//                vc?.amountYield = slideValue.amoundslideValue!
//                vc?.motorYield = slideValue.motorYieldValue!
//                vc?.currentInclination = slideValue.currenIncValue!
//                vc?.currentAzimuth = slideValue.currentAzimuthValue!
//                vc?.toolface = slideValue.toolFaceValue!
//
//                self.present(vc!, animated: true, completion: nil)
//                slide = 0
            }
           else if slideValue.amoundslideValue == nil || slideValue.amoundslideValue == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter amount slid value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
//                let alert = UIAlertController(title:"Alert", message:"Please fill all the fields", preferredStyle: .alert)
//                let action = UIAlertAction(title:"OK", style: .default)
//                alert.addAction(action)
//                self.present(alert, animated: true, completion: nil)
            }
            else if slideValue.motorYieldValue == nil || slideValue.motorYieldValue == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter motor yield value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if slideValue.currenIncValue == nil || slideValue.currenIncValue == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter current inclination value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if slideValue.currentAzimuthValue == nil || slideValue.currentAzimuthValue == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter current azimuth value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else if slideValue.toolFaceValue == nil || slideValue.toolFaceValue == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter toolface value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else if Double(slideValue.amoundslideValue!)! > 1000 {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter amount slid value less than 1000", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else if Double(slideValue.motorYieldValue!)! > 40 {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter motor slid value less than 40", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
                }
            else if Double(slideValue.currentAzimuthValue!)! >= 360 {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter azimuth value less than 360", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
                }
            else{
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ResultFinalViewController") as? ResultFinalViewController
                
//                if slideValue.toolFaceValue!.first == "-" {
//                    let tmp = slideValue.toolFaceValue!.dropFirst()
//                    let resultValue = 360 - Int(tmp)!
//                    slideValue.toolFaceValue = String(resultValue)
//                    print(resultValue)
//                }
                
                vc?.amountYield = slideValue.amoundslideValue!
                vc?.motorYield = slideValue.motorYieldValue!
                vc?.currentInclination = slideValue.currenIncValue!
                vc?.currentAzimuth = slideValue.currentAzimuthValue!
                vc?.toolface = slideValue.toolFaceValue!
                
        //        self.present(vc!, animated: true, completion: nil)
                self.navigationController?.pushViewController(vc!, animated: true)
                slide = 0
                }
            }
            else{
                slide = 1
            }
           print(cell.titleLbl.text!)
            
        default:
           print(cell.titleLbl.text!)
        }
    //    print("Curren IndexPath:\(indexPath)")
    }
}



