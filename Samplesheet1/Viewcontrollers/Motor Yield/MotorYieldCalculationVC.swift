//
//  MotorYieldCaculationVC.swift
//  Samplesheet1
//
//  Created by Ratheesh TR on 03/01/19.
//  Copyright © 2019 Sankar. All rights reserved.
//

import UIKit

class MotorYieldCalculationVC: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UIScrollViewDelegate,UICollectionViewDelegateFlowLayout {


    @IBOutlet var calcButtons: [UIButton]!
    let titleText = ["Dog Leg Severity?","Slide Seen?","Course Length?"]
    
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
 
    var isIncreasing = true
    // @IBOutlet weak var mainStackView: UIStackView!
    var operation = false
    var changingSign = false
    var curretPage:Int = 0
    var motorYield = motorYieldValues()
    var slide = 0
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        motorYield.dogLegSeverity = Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity] ?? ""
        motorYield.slideSeen = Constants.appdelegateSharedInstance.initialDict[Constants.slideSeen] ?? ""
        motorYield.courselength = Constants.appdelegateSharedInstance.initialDict[Constants.courseLength] ?? ""
//        mainCollectionView.delegate = self
//        mainCollectionView.dataSource = self
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func calcButtonTapped(_ sender: UIButton) {
        
        let buttonposition = sender.convert(CGPoint.zero, to: mainCollectionView)
        let index = mainCollectionView.indexPathForItem(at: buttonposition)
        
        switch sender.tag {
            
        case 0:
            Addnumberfunc("0", index!)
        case 1:
            Addnumberfunc("1", index!)
        case 2:
            Addnumberfunc("2", index!)
        case 3:
            Addnumberfunc("3", index!)
        case 4:
            Addnumberfunc("4", index!)
        case 5:
            Addnumberfunc("5", index!)
        case 6:
            Addnumberfunc("6", index!)
        case 7:
            Addnumberfunc("7", index!)
        case 8:
            Addnumberfunc("8", index!)
        case 9:
            Addnumberfunc("9", index!)
        case 11:
            decimalPointPressed(index!)
        case 10:
            plusMinusAction(index!)
        default:
            print("dfgdfg")
        }
    }
    
    func decimalPointPressed(_ index:IndexPath)
    {
        let cell = mainCollectionView.cellForItem(at: index) as! CalculatorCollectionViewCell
        guard let text = cell.valueLbl.text, !text.contains(".") else {
            return }
        if cell.valueLbl.text?.count == 0
        {
            cell.valueLbl.text = "0."
        }
        else
        {
            cell.valueLbl.text = text + "."
        }
    }
    
    @IBAction func acaction(_ sender: UIButton) {
        let buttonposition = sender.convert(CGPoint.zero, to: mainCollectionView)
        let index = mainCollectionView.indexPathForItem(at: buttonposition)
        let cell = mainCollectionView.cellForItem(at: index!) as! CalculatorCollectionViewCell
        
        
        switch index?.item {
        case 0:
            motorYield.dogLegSeverity = nil
            Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity] = nil
        case 1:
            motorYield.slideSeen = nil
            Constants.appdelegateSharedInstance.initialDict[Constants.slideSeen] = nil
        case 2:
            motorYield.courselength = nil
            Constants.appdelegateSharedInstance.initialDict[Constants.courseLength] = nil
        default:
            print("default")
        }
        cell.valueLbl.text = ""
    }
    
    @IBAction func bsaction(_ sender: UIButton) {
        let buttonposition = sender.convert(CGPoint.zero, to: mainCollectionView)
        let index = mainCollectionView.indexPathForItem(at: buttonposition)
        
        let cell = mainCollectionView.cellForItem(at: index!) as! CalculatorCollectionViewCell
        
        if cell.valueLbl.text != ""
        {
            cell.valueLbl.text?.removeLast()
            setvalues(cell.valueLbl.text!, index!)
        }
        else {
            setvalues("", index!)
        }
    }
    
    func plusMinusAction(_ index:IndexPath)
    {
        let cell = mainCollectionView.cellForItem(at: index) as! CalculatorCollectionViewCell
        
        if (cell.valueLbl.text == "") {
            return
        } else {
            if !changingSign {
                changingSign = true
                cell.valueLbl.text = "-" + cell.valueLbl.text!
            } else {
                changingSign = false
                cell.valueLbl.text?.removeFirst()
            }
        }
        setvalues(cell.valueLbl.text!, index)
    }
    
    @IBAction func percentageAction(_ sender: Any) {
        
        
    }
    
    func Addnumberfunc(_ number:String, _ index:IndexPath)
    {
        
        let cell = mainCollectionView.cellForItem(at: index) as! CalculatorCollectionViewCell
        var textnum = ""
        if let num = cell.valueLbl.text{
            
            //   textnum = String(cell.valueLbl.text!)
            textnum = num
        }
        if operation {
            
            textnum = ""
            operation = false
        }
        textnum = textnum + number
        
        cell.valueLbl.text = textnum
        
        cell.valueLbl.layer.borderWidth = 0.6
//        cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
        cell.valueLbl.layer.masksToBounds = true
        
        setvalues(textnum,index)
    }
    
    
    func setvalues(_ textnum : String ,_ index:IndexPath){
        switch index.item {
        case 0:
            motorYield.dogLegSeverity = textnum
            Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity] = textnum
            
        case 1:
            motorYield.slideSeen = textnum
            Constants.appdelegateSharedInstance.initialDict[Constants.slideSeen] = textnum
            
        case 2:
            motorYield.courselength = textnum
            Constants.appdelegateSharedInstance.initialDict[Constants.courseLength] = textnum
        default:
            print("default")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CalculatorCollectionViewCell
        cell.enterBtn.tag = indexPath.item
        cell.titleLbl.text = titleText[indexPath.item]
        cell.signBtn.isEnabled = false

        cell.valueLbl.layer.borderWidth = 0
        cell.valueLbl.layer.borderColor = UIColor.clear.cgColor
        cell.valueLbl.layer.masksToBounds = true
        switch indexPath.item {
        case 0:
            if let val = motorYield.dogLegSeverity{
                cell.valueLbl.text = val
                //Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity] = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
        case 1:
            if let val = motorYield.slideSeen{
                cell.valueLbl.text = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
        case 2:
            if let val = motorYield.courselength{
                cell.valueLbl.text = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
        default:
            cell.titleLbl.text = ""
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: mainCollectionView.frame.width, height: mainCollectionView.frame.height)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    @IBAction func enterAction(_ sender: UIButton) {
        
        let currentIndex = mainCollectionView.contentOffset.x / mainCollectionView.frame.size.width;
        print(Int(currentIndex))
        let indexPath = IndexPath(item: Int(currentIndex + 1), section: 0)
        pageControl.currentPage = NSInteger(ceil(currentIndex))
        
        let indexPath1 = IndexPath(item: Int(currentIndex), section: 0)
        let cell = mainCollectionView.cellForItem(at: indexPath1) as! CalculatorCollectionViewCell
        switch sender.tag {
        case 0:
            mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
            if let val = motorYield.dogLegSeverity {
                cell.valueLbl.text = val
            }
            else{
                cell.valueLbl.text = ""
                
            }
        // print(cell.titleLbl.text!)
        case 1:
            mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
            if let val = motorYield.slideSeen {
                cell.valueLbl.text = val
            }
            else{
                cell.valueLbl.text = ""
            }
        //   print(cell.titleLbl.text!)
        case 2:
            if (motorYield.dogLegSeverity == nil && motorYield.slideSeen == nil && motorYield.courselength == nil) || (motorYield.dogLegSeverity == "" && motorYield.slideSeen == "" && motorYield.courselength == ""){
                
                let alert = UIAlertController(title:"Alert", message:"Please fill all the fields", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else if motorYield.dogLegSeverity == nil || motorYield.dogLegSeverity == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Dog Leg Severity value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else if motorYield.slideSeen == nil || motorYield.slideSeen == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Slide seen value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if motorYield.courselength == nil || motorYield.courselength == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter course length value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MotorYieldResultVC") as? MotorYieldResultVC

                vc?.dogLegSeverity = motorYield.dogLegSeverity!
                vc?.slideSeen = motorYield.slideSeen!
                vc?.courseLength = motorYield.courselength!

                // self.present(vc!, animated: true, completion: nil)
                self.navigationController?.pushViewController(vc!, animated: true)
                slide = 0
            }


        default:
            print(cell.titleLbl.text!)
            
        }
        
        // print("Current IndexPath:\(indexPath)")
    }
    
    //MARK: Page Controller methods
    
    
    
    @IBAction func currentPageIndexMethod(_ sender: Any) {
        let currentIndex = mainCollectionView.contentOffset.x / mainCollectionView.frame.size.width
        print(Int(currentIndex))
        
        if isIncreasing  && currentIndex <= 2{
            
            let indexPath1 = IndexPath(item: Int(currentIndex), section: 0)
            let cell = mainCollectionView.cellForItem(at: indexPath1) as! CalculatorCollectionViewCell
            
            self.enterAction(cell.enterBtn)
            if currentIndex == 2{
                isIncreasing = false
            }
        }
        else{
            if currentIndex > 0 {
                let indexPath = IndexPath(item: Int(currentIndex - 1), section: 0)
                let indexPath1 = IndexPath(item: Int(currentIndex), section: 0)
                // pageControl.currentPage = NSInteger(ceil(currentIndex - 1))
                let cell = mainCollectionView.cellForItem(at: indexPath1) as! CalculatorCollectionViewCell
                switch currentIndex - 1 {
                case 0:
                    mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
                    if let val = motorYield.dogLegSeverity {
                        cell.valueLbl.text = val
                        Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity] = val
                    }
                    else{
                        cell.valueLbl.text = ""
                        Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity] = ""
                    }
                case 1:
                    mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
                    if let val = motorYield.slideSeen {
                        cell.valueLbl.text = val
                    }
                    else{
                        cell.valueLbl.text = ""
                    }
                default:
                    print(cell.titleLbl.text!)
                }
            }
            else{
                isIncreasing = true
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let currentIndex = mainCollectionView.contentOffset.x / mainCollectionView.frame.size.width;
        print(Int(currentIndex))
        pageControl.currentPage = NSInteger(ceil(currentIndex))
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = mainCollectionView.contentOffset
        visibleRect.size = mainCollectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = mainCollectionView.indexPathForItem(at: visiblePoint) else {
            return
        }
        let cell = mainCollectionView.cellForItem(at: indexPath) as! CalculatorCollectionViewCell
        
        switch indexPath.item {
        case 0:
            cell.valueLbl.text =  motorYield.dogLegSeverity
           // print(cell.titleLbl.text!)
        case 1:
            cell.valueLbl.text = motorYield.slideSeen
           // print(cell.titleLbl.text!)
        case 2:
            if slide == 1{
                if (motorYield.dogLegSeverity == nil && motorYield.slideSeen == nil && motorYield.courselength == nil) || (motorYield.dogLegSeverity == "" && motorYield.slideSeen == "" && motorYield.courselength == ""){
                    
                    let alert = UIAlertController(title:"Alert", message:"Please fill all the fields", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else if motorYield.dogLegSeverity == nil || motorYield.dogLegSeverity == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter dog leg severity value", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                    
                    //                let alert = UIAlertController(title:"Alert", message:"Please fill all the fields", preferredStyle: .alert)
                    //                let action = UIAlertAction(title:"OK", style: .default)
                    //                alert.addAction(action)
                    //                self.present(alert, animated: true, completion: nil)
                }
                else if motorYield.slideSeen == nil || motorYield.slideSeen == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter motor yield value", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                else if motorYield.courselength == nil || motorYield.courselength == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter course length value", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MotorYieldResultVC") as? MotorYieldResultVC
                    
                    vc?.dogLegSeverity = motorYield.dogLegSeverity!
                    vc?.slideSeen = motorYield.slideSeen!
                    vc?.courseLength = motorYield.courselength!
                    
                    // self.present(vc!, animated: true, completion: nil)
                    self.navigationController?.pushViewController(vc!, animated: true)
                    slide = 0
                }
            }
            else{
                slide = 1
            }
            
        default:
            print(cell.titleLbl.text!)
            
        }
    //    print("Curren IndexPath:\(indexPath)")
    }
}


