//
//  MotorYieldResultVC.swift
//  Samplesheet1
//
//  Created by Ratheesh TR on 03/01/19.
//  Copyright © 2019 Sankar. All rights reserved.
//

import UIKit

class MotorYieldResultVC: UIViewController,UIScrollViewDelegate, UITextFieldDelegate  {

    @IBOutlet var vwMain: UIView!
    @IBOutlet var scrollVw: UIScrollView!
    @IBOutlet weak var lblResultValue: UILabel!
    
    @IBOutlet var dogLegSeverity_txt: UITextField!
    @IBOutlet var slideSeen_txt: UITextField!
    @IBOutlet var courseLength_txt: UITextField!
    
    var projectionBit = ProjectionToBit()
    
    var dogLegSeverity = String()
    var slideSeen = String()
    var courseLength = String()
    var resultValue_Str = String()
    var resultValue_Double = Double()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Do any additional setup after loading the view.
        assignbackground()
        
        dogLegSeverity_txt.delegate = self
        slideSeen_txt.delegate = self
        courseLength_txt.delegate = self
        
        calculate()

        
        dogLegSeverity_txt.text = dogLegSeverity
        slideSeen_txt.text = slideSeen
        courseLength_txt.text = courseLength
        

        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        //resultView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        
    }
    
    override func viewDidLayoutSubviews() {
        

        
    }
    
    //MARK: - Calculation Methods
    func calculate(){
        if dogLegSeverity.count != 0 && slideSeen.count != 0 && courseLength.count != 0 {
            let doglegSeverity = Double(dogLegSeverity)
            let slideSeenValue = Double(slideSeen)
            let courseLengthValue = Double(courseLength)
            let result = (doglegSeverity! / slideSeenValue!) * courseLengthValue!
            resultValue_Double = Double(result)
            let resultCheck =  (resultValue_Double*100).rounded()/100
            resultValue_Str = String(resultCheck)
       //     storeValue(val: resultValue_Str, Key: Constants.motorYield)
            Constants.appdelegateSharedInstance.initialDict[Constants.motorYield] = resultValue_Str
            print("\(resultValue_Str)")
            
            lblResultValue.text = resultValue_Str
        }
    }
    

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        dogLegSeverity = dogLegSeverity_txt.text ??  ""
        slideSeen = slideSeen_txt.text ??  ""
        courseLength = courseLength_txt.text ?? ""
        
        switch textField{
        case dogLegSeverity_txt:
            if string == ""{
                dogLegSeverity.removeLast()
            }else{
                dogLegSeverity = dogLegSeverity + string
            }
        case slideSeen_txt:
            if string == ""{
                slideSeen.removeLast()
            }else{
                slideSeen = slideSeen + string
            }
        case courseLength_txt:
            if string == ""{
                courseLength.removeLast()
            }else{
                courseLength = courseLength + string
            }
            
        default:
            print("")
        }
        
        calculate()

        return true
    }
    
    @available(iOS 10.0, *)
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        
        switch textField{
        case dogLegSeverity_txt:
            if  dogLegSeverity == ""  {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter DogLegSeverity", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                calculate()
            }
            
        case slideSeen_txt:
            
            if slideSeen == "" {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter slideSeen value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else{
                calculate()

            }
            
        case courseLength_txt:
            if courseLength == "" {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter courseLength value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                calculate()
            }
            
        default:
            print("")
        }
    }

    
    func assignbackground(){
        let background = UIImage(named: "background")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    //MARK: - Gesture Recognizer methods
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
//                let tabBar = self.navigationController?.viewControllers[0] as? UITabBarController
//                if (tabBar?.selectedIndex)! > 0 {
//                tabBar?.selectedIndex -= 1
//                }
                self.tabBarController?.selectedIndex = 0
                self.navigationController?.popViewController(animated: true)
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
//                let tabBar = self.navigationController?.viewControllers[0] as? UITabBarController
//                tabBar?.selectedIndex += 1
                self.tabBarController?.selectedIndex += 1
                self.navigationController?.popViewController(animated: true)
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
}


