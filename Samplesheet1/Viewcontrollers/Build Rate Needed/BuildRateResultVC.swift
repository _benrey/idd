//
//  BuildRateResultVC.swift
//  Samplesheet1
//
//  Created by Ratheesh TR on 09/01/19.
//  Copyright © 2019 Sankar. All rights reserved.
//

import UIKit

class BuildRateResultVC: UIViewController,UIScrollViewDelegate, UITextFieldDelegate {

    @IBOutlet var vwMain: UIView!
    @IBOutlet var scrollVw: UIScrollView!
    @IBOutlet weak var lblResultValue: UILabel!
    
    @IBOutlet weak var currentInclination_txt: UITextField!
    @IBOutlet weak var currentTVD_txt: UITextField!
    @IBOutlet weak var targetInclination_txt: UITextField!
    @IBOutlet weak var targetTVD_txt: UITextField!
    
//    var projectionBit = ProjectionToBit()
    
    var currentInc = String()
    var currentTvd = String()
    var targetInc = String()
    var targetTvd = String()
    var resultValue_Str = String()
    var resultValue_Double = Double()
    
    var graphFrame:CGRect!
    var graphCenter : CGPoint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        assignbackground()
        
        currentInclination_txt.delegate = self
        currentTVD_txt.delegate = self
        targetInclination_txt.delegate = self
        targetTVD_txt.delegate = self
        
        calculate()
        
        currentInclination_txt.text = currentInc
        currentTVD_txt.text = currentTvd
        targetInclination_txt.text = targetInc
        targetTVD_txt.text = targetTvd
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
    
        
    }
    

    //MARK: - Calculation Methods
    func calculate(){
        if currentInc.count != 0 && currentTvd.count != 0 && targetInc.count != 0 && targetTvd.count != 0 {
            let currentIncValue = Double(currentInc)
            let currentTVDVal = Double(currentTvd)
            let targetIncVal = Double(targetInc)
            let targetTvdVal = Double(targetTvd)
            let buildrate = ((sin(deg2rad(targetIncVal!))-sin(deg2rad(currentIncValue!)))*5729.58) / (Double(targetTvdVal!) - Double(currentTVDVal!))
            resultValue_Double = Double(buildrate)
            let resultCheck =  (resultValue_Double*100).rounded()/100
            resultValue_Str = String(resultCheck)
            lblResultValue.text = resultValue_Str
            Constants.appdelegateSharedInstance.initialDict[Constants.buildRateNeeded] = resultValue_Str
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        currentInc = currentInclination_txt.text ??  ""
        currentTvd = currentTVD_txt.text ??  ""
        targetInc = targetInclination_txt.text ?? ""
        targetTvd = targetTVD_txt.text ?? ""
        
        
        switch textField{
        case currentInclination_txt:
            if string == ""{
                currentInc.removeLast()
            }else{
                currentInc = currentInc + string
            }
        case currentTVD_txt:
            if string == ""{
                currentTvd.removeLast()
            }else{
                currentTvd = currentTvd + string
            }
        case targetInclination_txt:
            if string == ""{
                targetInc.removeLast()
            }else{
                targetInc = targetInc + string
            }
        case targetTVD_txt:
            if string == ""{
                targetTvd.removeLast()
            }else{
                targetTvd = targetTvd + string
            }
            
        default:
            print("")
        }
        calculate()
        return true
    }
    
    
    @available(iOS 10.0, *)
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        
        switch textField{
        case currentInclination_txt:
            if  currentInc == ""  {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter current inclination value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                calculate()
            }
            
        case currentTVD_txt:
            
            if currentTvd == "" {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter currentTVD value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else{
                calculate()
            }
            
        case targetInclination_txt:
            if targetInc == "" {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter target inclination value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                calculate()
            }
        case targetTVD_txt:
            if targetTvd == "" {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter target TVD value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                calculate()
            }
        default:
            print("")
        }
    }
    
    
    func assignbackground(){
        let background = UIImage(named: "background")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    //MARK: - gesture Recognizer methods
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
//                let tabBar = self.navigationController?.viewControllers[0] as? UITabBarController
//                if (tabBar?.selectedIndex)! > 0 {
//                    tabBar?.selectedIndex -= 1
//                }
                self.tabBarController?.selectedIndex -= 1
                self.navigationController?.popViewController(animated: true)
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
//                let tabBar = self.navigationController?.viewControllers[0] as? UITabBarController
//                tabBar?.selectedIndex += 1
                self.tabBarController?.selectedIndex += 1
                self.navigationController?.popViewController(animated: true)
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }


}
