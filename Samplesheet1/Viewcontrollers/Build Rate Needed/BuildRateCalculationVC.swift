//
//  BuildRateCalculationVC.swift
//  Samplesheet1
//
//  Created by Ratheesh TR on 09/01/19.
//  Copyright © 2019 Sankar. All rights reserved.
//

import UIKit

class BuildRateCalculationVC: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UIScrollViewDelegate,UICollectionViewDelegateFlowLayout  {
  //  let titleText = ["Current Inclination?","Current TVD?","Target Inclination?","Target TVD?"]
     let titleText = ["Projected Inclination?","Current TVD?","Target Inclination?","Target TVD?"]
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    var isIncreasing = true
    // @IBOutlet weak var mainStackView: UIStackView!
    var operation = false
    var changingSign = false
    var curretPage:Int = 0
    var buildRateNeeded = BuildRateNeeded()
    var slide = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
    self.tabBarController?.tabBar.isHidden = false
   // buildRateNeeded.currentInclination = Constants.appdelegateSharedInstance.initialDict[Constants.currentInclination] ?? ""
        buildRateNeeded.currentInclination = Constants.appdelegateSharedInstance.initialDict[Constants.projectedInc] ?? ""
    buildRateNeeded.currentTVD = Constants.appdelegateSharedInstance.initialDict[Constants.TVD] ?? ""
    buildRateNeeded.targetInclination = Constants.appdelegateSharedInstance.initialDict[Constants.targetInclination] ?? ""
    buildRateNeeded.targetTVD = Constants.appdelegateSharedInstance.initialDict[Constants.targetTVD] ?? ""
        
    }
    

    //MARK: - Button Methods
    @IBAction func calcButtonTapped(_ sender: UIButton) {
        
        let buttonposition = sender.convert(CGPoint.zero, to: mainCollectionView)
        let index = mainCollectionView.indexPathForItem(at: buttonposition)
        
        switch sender.tag {
            
        case 0:
            Addnumberfunc("0", index!)
        case 1:
            Addnumberfunc("1", index!)
        case 2:
            Addnumberfunc("2", index!)
        case 3:
            Addnumberfunc("3", index!)
        case 4:
            Addnumberfunc("4", index!)
        case 5:
            Addnumberfunc("5", index!)
        case 6:
            Addnumberfunc("6", index!)
        case 7:
            Addnumberfunc("7", index!)
        case 8:
            Addnumberfunc("8", index!)
        case 9:
            Addnumberfunc("9", index!)
        case 11:
            decimalPointPressed(index!)
        case 10:
            plusMinusAction(index!)
        default:
            print("dfgdfg")
        }
    }
    
    func decimalPointPressed(_ index:IndexPath)
    {
        let cell = mainCollectionView.cellForItem(at: index) as! CalculatorCollectionViewCell
        guard let text = cell.valueLbl.text, !text.contains(".") else {
            return }
        if cell.valueLbl.text?.count == 0
        {
            cell.valueLbl.text = "0."
        }
        else
        {
            cell.valueLbl.text = text + "."
        }
    }
    
    @IBAction func acaction(_ sender: UIButton) {
        let buttonposition = sender.convert(CGPoint.zero, to: mainCollectionView)
        let index = mainCollectionView.indexPathForItem(at: buttonposition)
        let cell = mainCollectionView.cellForItem(at: index!) as! CalculatorCollectionViewCell
        
        
        switch index?.item {
        case 0:
            buildRateNeeded.currentInclination = nil
           // Constants.appdelegateSharedInstance.initialDict[Constants.currentInclination] = nil
            Constants.appdelegateSharedInstance.initialDict[Constants.projectedInc] = nil
        case 1:
            buildRateNeeded.currentTVD = nil
            
        case 2:
            buildRateNeeded.targetInclination = nil
        
        case 3:
            buildRateNeeded.targetTVD = nil
            
        default:
            print("default")
        }
        cell.valueLbl.text = ""
    }
    
    @IBAction func bsaction(_ sender: UIButton) {
        let buttonposition = sender.convert(CGPoint.zero, to: mainCollectionView)
        let index = mainCollectionView.indexPathForItem(at: buttonposition)
        
        let cell = mainCollectionView.cellForItem(at: index!) as! CalculatorCollectionViewCell
        
        if cell.valueLbl.text != ""
        {
            cell.valueLbl.text?.removeLast()
            setvalues(cell.valueLbl.text!, index!)
        }
        else {
            setvalues("", index!)
        }
    }
    
    func plusMinusAction(_ index:IndexPath)
    {
        let cell = mainCollectionView.cellForItem(at: index) as! CalculatorCollectionViewCell
        
        if (cell.valueLbl.text == "") {
            return
        } else {
            if !changingSign {
                changingSign = true
                cell.valueLbl.text = "-" + cell.valueLbl.text!
            } else {
                changingSign = false
                cell.valueLbl.text?.removeFirst()
            }
        }
        setvalues(cell.valueLbl.text!, index)
    }
    
    @IBAction func percentageAction(_ sender: Any) {
        
        
    }
    
    func Addnumberfunc(_ number:String, _ index:IndexPath)
    {
        
        let cell = mainCollectionView.cellForItem(at: index) as! CalculatorCollectionViewCell
        var textnum = ""
        if let num = cell.valueLbl.text{
            
            //   textnum = String(cell.valueLbl.text!)
            textnum = num
        }
        if operation {
            
            textnum = ""
            operation = false
        }
        textnum = textnum + number
        
        cell.valueLbl.text = textnum
        
        cell.valueLbl.layer.borderWidth = 0.6
//        cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
        cell.valueLbl.layer.masksToBounds = true
        
        setvalues(textnum,index)
    }
    
    
    func setvalues(_ textnum : String ,_ index:IndexPath){
        switch index.item {
        case 0:
            buildRateNeeded.currentInclination = textnum
           // Constants.appdelegateSharedInstance.initialDict[Constants.currentInclination] = textnum
            Constants.appdelegateSharedInstance.initialDict[Constants.projectedInc] = textnum
        case 1:
            buildRateNeeded.currentTVD = textnum
            
        case 2:
            buildRateNeeded.targetInclination = textnum
            
        case 3:
            buildRateNeeded.targetTVD = textnum
            
        default:
            print("default")
        }
    }
    
    //MARK: Collection View Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titleText.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CalculatorCollectionViewCell
        cell.enterBtn.tag = indexPath.item
        cell.titleLbl.text = titleText[indexPath.item]
        
        cell.signBtn.isEnabled = false
        cell.valueLbl.layer.borderWidth = 0
        cell.valueLbl.layer.borderColor = UIColor.clear.cgColor
        cell.valueLbl.layer.masksToBounds = true
        
        switch indexPath.item {
        case 0:
            if let val = buildRateNeeded.currentInclination{
                cell.valueLbl.text = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
        case 1:
            if let val = buildRateNeeded.currentTVD{
                cell.valueLbl.text = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
        case 2:
            if let val = buildRateNeeded.targetInclination{
                cell.valueLbl.text = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
        case 3:
            if let val = buildRateNeeded.targetTVD{
                cell.valueLbl.text = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
        default:
            cell.titleLbl.text = ""
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
//        print ("Collection View Height :\(mainCollectionView.frame.height)")
        return CGSize(width: mainCollectionView.frame.width, height: mainCollectionView.frame.height)
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    @IBAction func enterAction(_ sender: UIButton) {
        
        let currentIndex = mainCollectionView.contentOffset.x / mainCollectionView.frame.size.width;
        print(Int(currentIndex))
        let indexPath = IndexPath(item: Int(currentIndex + 1), section: 0)
        pageControl.currentPage = NSInteger(ceil(currentIndex))
        
        let indexPath1 = IndexPath(item: Int(currentIndex), section: 0)
        let cell = mainCollectionView.cellForItem(at: indexPath1) as! CalculatorCollectionViewCell
        switch sender.tag {
        case 0:
            mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
            if let val = buildRateNeeded.currentInclination {
                cell.valueLbl.text = val
            }
            else{
                cell.valueLbl.text = ""
            }
        // print(cell.titleLbl.text!)
        case 1:
            mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
            if let val = buildRateNeeded.currentTVD {
                cell.valueLbl.text = val
            }
            else{
                cell.valueLbl.text = ""
            }
        //   print(cell.titleLbl.text!)
            
        case 2:
            mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
            if let val = buildRateNeeded.targetInclination {
                cell.valueLbl.text = val
            }
            else{
                cell.valueLbl.text = ""
            }
            
        case 3:
            if (buildRateNeeded.currentInclination == nil && buildRateNeeded.currentTVD == nil && buildRateNeeded.targetInclination == nil && buildRateNeeded.targetTVD == nil) || (buildRateNeeded.currentInclination == "" && buildRateNeeded.currentTVD == "" && buildRateNeeded.targetInclination == "" && buildRateNeeded.targetTVD == ""){
                
                let alert = UIAlertController(title:"Alert", message:"Please fill all the fields", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if buildRateNeeded.currentInclination == nil || buildRateNeeded.currentInclination == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter current inclination value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if buildRateNeeded.currentTVD == nil || buildRateNeeded.currentTVD == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter current TVD value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if buildRateNeeded.targetInclination == nil || buildRateNeeded.targetInclination == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter target inclination value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if buildRateNeeded.targetTVD == nil || buildRateNeeded.targetTVD == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter target TVD value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "BuildRateResultVC") as? BuildRateResultVC
                
//                vc?.motorYield = projectionBit.motorYield!
//                vc?.amountSlid = projectionBit.amountSlid!
//                vc?.currentInc = projectionBit.currentInclination!
                
                vc?.currentInc = buildRateNeeded.currentInclination!
                vc?.currentTvd = buildRateNeeded.currentTVD!
                vc?.targetInc = buildRateNeeded.targetInclination!
                vc?.targetTvd = buildRateNeeded.targetTVD!
                // self.present(vc!, animated: true, completion: nil)
                self.navigationController?.pushViewController(vc!, animated: true)
                slide = 0
            }
            
        default:
            print(cell.titleLbl.text!)
            
        }
    }
    
    //MARK: - Page Controller Methods
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let currentIndex = mainCollectionView.contentOffset.x / mainCollectionView.frame.size.width;
        print(Int(currentIndex))
        pageControl.currentPage = NSInteger(ceil(currentIndex))
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = mainCollectionView.contentOffset
        visibleRect.size = mainCollectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = mainCollectionView.indexPathForItem(at: visiblePoint) else {
            return
        }
        let cell = mainCollectionView.cellForItem(at: indexPath) as! CalculatorCollectionViewCell
        
        switch indexPath.item {
        case 0:
            cell.valueLbl.text =  buildRateNeeded.currentInclination
        case 1:
            cell.valueLbl.text = buildRateNeeded.currentTVD
        case 2:
            cell.valueLbl.text = buildRateNeeded.targetInclination
        case 3:
            if slide == 1{
                if (buildRateNeeded.currentInclination == nil && buildRateNeeded.currentTVD == nil && buildRateNeeded.targetInclination == nil && buildRateNeeded.targetTVD == nil) || (buildRateNeeded.currentInclination == "" && buildRateNeeded.currentTVD == "" && buildRateNeeded.targetInclination == "" && buildRateNeeded.targetTVD == ""){
                    
                    let alert = UIAlertController(title:"Alert", message:"Please fill all the fields", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                else if buildRateNeeded.currentInclination == nil || buildRateNeeded.currentInclination == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter current inclination value", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                else if buildRateNeeded.currentTVD == nil || buildRateNeeded.currentTVD == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter current TVD value", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                else if buildRateNeeded.targetInclination == nil || buildRateNeeded.targetInclination == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter target inclination value", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                else if buildRateNeeded.targetTVD == nil || buildRateNeeded.targetTVD == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter target TVD value", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "BuildRateResultVC") as? BuildRateResultVC
                    
                    vc?.currentInc = buildRateNeeded.currentInclination!
                    vc?.currentTvd = buildRateNeeded.currentTVD!
                    vc?.targetInc = buildRateNeeded.targetInclination!
                    vc?.targetTvd = buildRateNeeded.targetTVD!
                    // self.present(vc!, animated: true, completion: nil)
                    self.navigationController?.pushViewController(vc!, animated: true)
                    slide = 0
                }
            }
            else{
                slide = 1
            }
            
        default:
            print(cell.titleLbl.text!)
            
        }
      //  print("Curren IndexPath:\(indexPath)")
    }

}
