//
//  DCSheetVC.swift
//  Samplesheet1
//
//  Created by Sankar on 7/6/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit

class DCSheetVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {

    @IBOutlet var TableviewSheet: UITableView!
    @IBOutlet var Headerview: UIView!
    @IBOutlet var Scrollview: UIScrollView!
    
    var ArrayJointNumber  = NSMutableArray()
    var ArrayJointLength  = NSMutableArray()
    var ArrayDouble = NSMutableArray()
    var ArrayDoubleOne = NSMutableArray()
    var ArraySingle = NSMutableArray()
    var ArraySingleone = NSMutableArray()
    
    var ArrayPipeOnly = NSMutableArray()
    var ArrayTotalString = NSMutableArray()
    var ArrayKellydown = NSMutableArray()
    var ArraySurveyDepth = NSMutableArray()
    
    var n3 : Int!
    var D4 : Double!
    var F3 : Int!
    var G3 : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        n3 = 2
        D4 = 88.21
        F3 = 0
        G3 = 72
        self.hideKeyboardWhenTappedAround()
        UpdateArrayvalue()
        // Do any additional setup after loading the view.
    }
    
    func UpdateArrayvalue () {
        for i in 0...99 {
            ArrayJointNumber.add("")
            ArrayJointLength.add("")
            ArrayDouble.add("")
            ArraySingle.add("")
            ArraySingleone.add("")
            if(i==0){
            ArrayPipeOnly.add("0")
            }
            else {
             ArrayPipeOnly.add("")
            }
            ArrayTotalString.add("")
            ArrayKellydown.add("")
            ArraySurveyDepth.add("")
            ArrayDoubleOne.add("")
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Scrollview.contentSize = CGSize(width:Headerview.frame.size.width , height: Scrollview.frame.size.height)
        Scrollview.showsVerticalScrollIndicator = false
        Scrollview.showsHorizontalScrollIndicator = false
    }
    
    //MARK: Tableview Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = TableviewSheet.dequeueReusableCell(withIdentifier:"Cell", for: indexPath ) as! DcsheetCell
        
        cell.TxtfieldJointLength.text = ""
        cell.LblJointnumber.text = ""
        cell.LblOne.text = ""
        cell.LblCurveshoe.text = ""
        cell.LblThree.text = ""
        cell.LblFour.text = ""
        cell.LblTwo.text = ""
        cell.LblStdNo.text = ""
        cell.TxtfieldRemarks.text = ""
        cell.LblSurveyDepth.text = ""
        cell.LblKellyDown.text = ""
        cell.LblTotalString.text = ""
        cell.LblPipeOnly.text = ""
        cell.TxtfieldJointLength.text = ""
        cell.LblJointnumber.text = ""
        
        if((ArrayJointLength[indexPath.row] as! String != "")){
            cell.TxtfieldJointLength.text = ArrayJointLength[indexPath.row] as? String
        }
        
        if((ArrayJointNumber[indexPath.row] as! String != "")){
            cell.LblJointnumber.text = ArrayJointNumber[indexPath.row] as? String
            cell.LblCurveshoe.text = ArrayJointNumber[indexPath.row] as? String
        }
        
        if((ArraySingle[indexPath.row] as! String != "")){
            cell.LblOne.text = ArraySingle[indexPath.row] as? String
        }
        
        if((ArraySingleone[indexPath.row] as! String != "")){
            cell.LblTwo.text = ArraySingleone[indexPath.row] as? String
        }
        
        if((ArrayPipeOnly[indexPath.row] as! String != "")){
            cell.LblPipeOnly.text = ArrayPipeOnly[indexPath.row] as? String
        }
        
        if((ArrayTotalString[indexPath.row] as! String != "")){
            cell.LblTotalString.text = ArrayTotalString[indexPath.row] as? String
        }
        
        if((ArrayKellydown[indexPath.row] as! String != "")){
            cell.LblKellyDown.text = ArrayKellydown[indexPath.row] as? String
        }
        
        if((ArraySurveyDepth[indexPath.row] as! String != "")){
            cell.LblSurveyDepth.text = ArraySurveyDepth[indexPath.row] as? String
        }
        
        if((ArrayDouble[indexPath.row] as! String != "")){
            cell.LblFour.text = ArrayDouble[indexPath.row] as? String
        }
        
        if((ArrayDoubleOne[indexPath.row] as! String != "")){
            cell.LblThree.text = ArrayDoubleOne[indexPath.row] as? String
        }
        if(n3 == 3){
            cell.LblStdNo.text = ArrayDoubleOne[indexPath.row] as? String
        }
        else {
           cell.LblStdNo.text = ArrayDouble[indexPath.row] as? String
        }
        
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: TextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        view.endEditing(true)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        let pointInTable = textField.convert(textField.bounds.origin, to: self.TableviewSheet)
        let textFieldIndexPath = self.TableviewSheet.indexPathForRow(at: pointInTable)
        print(textFieldIndexPath!.row)
        if(textField.tag == 22){
            ArrayJointLength.replaceObject(at:textFieldIndexPath!.row, with: textField.text as Any)
        }
        if((ArrayJointLength[textFieldIndexPath!.row] as! String != "")){
            let stop = ArrayJointLength[textFieldIndexPath!.row] as! String
            let row : Int! = textFieldIndexPath!.row
            let a:Double = Double(stop)!
            if(a > 0) {
            ArrayJointNumber.replaceObject(at:textFieldIndexPath!.row, with:String(row+1) as Any)
            let Stringvalue  = ArrayJointNumber[textFieldIndexPath!.row] as! String
            let number = Float(Stringvalue)!
            let value = Float(number/2)
            let Valueone = round(Float(value+0.2))
                
            // Double Calculation M column
            if(value == Valueone){
                ArrayDoubleOne.replaceObject(at:textFieldIndexPath!.row, with:String(Valueone) as Any)
            }
            else {
                if(value < (Valueone-0.5)){
                     ArrayDoubleOne.replaceObject(at:textFieldIndexPath!.row, with:String("X") as Any)
                }
                else {
                     ArrayDoubleOne.replaceObject(at:textFieldIndexPath!.row, with:String("XX") as Any)
                }
            }
            // Double Calculation
            if(value < Valueone){
              ArrayDouble.replaceObject(at:textFieldIndexPath!.row, with:String("X") as Any)
            }
            else {
              ArrayDouble.replaceObject(at:textFieldIndexPath!.row, with:String(Valueone) as Any)
            }
            
            // pipeonly
            let joint = ArrayJointLength[textFieldIndexPath!.row] as! String
            let Jointlength: Double! = Double(joint)
                let pipe : String!
                if(row == 0){
                 pipe = ArrayPipeOnly[textFieldIndexPath!.row] as! String
                }
                else {
                  pipe = ArrayPipeOnly[textFieldIndexPath!.row - 1] as! String
                }
            let pipeone : Double! = Double(pipe)
            let pipevalue : Double! = Double(pipeone)
            let pipeonly : Double! = Double(Jointlength) + Double(pipevalue)
            let TotalString : Double = Double(pipeonly) + D4
            let Kellydown : Double = Double(TotalString) + Double(F3)
            let SurveyDepth : Double = Double(Kellydown) - Double(G3)
            ArraySingle.replaceObject(at:textFieldIndexPath!.row, with:String(value) as Any)
            ArraySingleone.replaceObject(at:textFieldIndexPath!.row, with:String(Valueone) as Any)
            ArrayPipeOnly.replaceObject(at:textFieldIndexPath!.row, with:String(pipeonly) as Any)
            if(pipeonly == 0){
                ArrayTotalString.replaceObject(at:textFieldIndexPath!.row, with:String(0) as Any)
            }
            else {
                ArrayTotalString.replaceObject(at:textFieldIndexPath!.row, with:String(TotalString) as Any)
            }
            if(TotalString == 0){
                    ArrayKellydown.replaceObject(at:textFieldIndexPath!.row, with:String(0) as Any)
            }
            else {
                    ArrayKellydown.replaceObject(at:textFieldIndexPath!.row, with:String(Kellydown) as Any)
            }
            if(Kellydown == 0){
                    ArraySurveyDepth.replaceObject(at:textFieldIndexPath!.row, with:String(0) as Any)
            }
            else {
                    ArraySurveyDepth.replaceObject(at:textFieldIndexPath!.row, with:String(SurveyDepth) as Any)
            }
            }
            TableviewSheet.reloadData()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let aSet = NSCharacterSet(charactersIn:"0123456789.").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        let maxLength = 5
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength && string == numberFiltered
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
