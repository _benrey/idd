//
//  ProjectAheadVc.swift
//  Samplesheet1
//
//  Created by Sankar on 24/5/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit

func deg2rad(_ number: Double) -> Double {
    return number * .pi / 180
}
func rad2deg(_ number: Double) -> Double {
    return number * 180 / .pi
}

class ProjectAheadVc: UIViewController {
    @IBOutlet var TxtfieldInclinationresult2: GSBaseTextField!
    @IBOutlet var TxtfieldActualDeltaAz: GSBaseTextField!
    @IBOutlet var TxtfieldRawresult: GSBaseTextField!
    @IBOutlet var TxtfieldAzimuthresultfirst: GSBaseTextField!
    @IBOutlet var TxtfieldAzimuthresultlast: GSBaseTextField!
    @IBOutlet var TxtfieldProjectto2: GSBaseTextField!
    @IBOutlet var TxtfieldProjectto1: GSBaseTextField!
    @IBOutlet var TxtfieldDoglegperCL: GSBaseTextField!
    @IBOutlet var TxtfieldInclinationResult: GSBaseTextField!
    @IBOutlet var TxtfieldDogleg: GSBaseTextField!
    @IBOutlet var TxtfieldStartInclination: GSBaseTextField!
    @IBOutlet var TxtfieldEndInclination: GSBaseTextField!
    @IBOutlet var TxtfieldStartAzimuth: GSBaseTextField!
    @IBOutlet var TxtfieldEndAzimuth: GSBaseTextField!
    
    
    @IBOutlet var Scrollview: UIScrollView!
    @IBOutlet var ViewProjectToResult: UIView!
    @IBOutlet var ActionCalculation: UIButton!
    
    
    var  CalculateCondition : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         CalculateCondition = 1
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        Scrollview.contentSize = CGSize(width:Scrollview.frame.size.width , height: ViewProjectToResult.frame.origin.y+ViewProjectToResult.frame.size.height)
        Scrollview.showsVerticalScrollIndicator = false
        Scrollview.showsHorizontalScrollIndicator = false
  
    }
    
    @IBAction func Calculation(_ sender: Any) {
        
        if(((TxtfieldStartInclination.text?.count) != 0) && ((TxtfieldEndInclination.text?.count) != 0) && ((TxtfieldStartAzimuth.text?.count) != 0) && ((TxtfieldEndAzimuth.text?.count) != 0))  {
        // Dogleg Calculation
        CalculateCondition = 2
        let startInc = Double(TxtfieldStartInclination.text!)
        let endInc = Double(TxtfieldEndInclination.text!)
        let startAzimuth = Double(TxtfieldStartAzimuth.text!)
        let endAzimuth = Double(TxtfieldEndAzimuth.text!)
        
        let Dogleg = rad2deg(acos((cos(deg2rad(startInc!)) * cos(deg2rad(endInc!))) + (sin(deg2rad(startInc!)) * sin(deg2rad(endInc!)) * cos(abs(deg2rad(endAzimuth!+0.00001))-(deg2rad(startAzimuth!))))))
       // TxtfieldDogleg.text = "\(Int(Dogleg))"
        TxtfieldDogleg.text = "\(Double(Dogleg))"
        }
        
        if(((TxtfieldProjectto1.text?.count) != 0) && ((TxtfieldProjectto2.text?.count) != 0)){
        // Doglegper CL Calculation
        CalculateCondition = 2
        let projectto1 = Int(TxtfieldProjectto1.text!)
        let projectto2 = Int(TxtfieldProjectto2.text!)
        let Endazimuth2 = Int(19)
        print(projectto1 as Any)
  //      TxtfieldDoglegperCL.text = "\(Int(projectto2!*Endazimuth2/100))"
        TxtfieldDoglegperCL.text = "\(Double(projectto2!*Endazimuth2/100))"
        }
        
        if(((TxtfieldDoglegperCL.text?.count) != 0) && ((TxtfieldEndInclination.text?.count) != 0) ){
       // Inclination result
         CalculateCondition = 2
         let doglegpercl = Double(TxtfieldDoglegperCL.text!)
         let Inclination = rad2deg(cos(deg2rad(0)) * deg2rad(doglegpercl!)) + Double(TxtfieldEndInclination.text!)!
//         TxtfieldInclinationResult.text = "\(Inclination)"
          TxtfieldInclinationResult.text = "\(Double(Inclination))"
        }
        
        
        if(((TxtfieldDoglegperCL.text?.count) != 0) && ((TxtfieldEndInclination.text?.count) != 0) ){
            // Inclination Second result
            CalculateCondition = 2
            let doglegpercl = Double(TxtfieldDoglegperCL.text!)
            let Inclination = rad2deg(cos(deg2rad(0)) * deg2rad(doglegpercl!)) + Double(TxtfieldEndInclination.text!)!
        //    TxtfieldInclinationresult2.text = "\(Int(Inclination) - Int(TxtfieldEndInclination.text!)!)"
        TxtfieldInclinationresult2.text = "\(Double(Inclination) - Double(TxtfieldEndInclination.text!)!)"
        }
        
        
        if(((TxtfieldDoglegperCL.text?.count) != 0) && ((TxtfieldEndInclination.text?.count) != 0) && ((TxtfieldInclinationResult.text?.count) != 0) ) {
       // Azimuth Second  result
        CalculateCondition = 2
        let inclinationlast = 10
        let doglegpercl = Double(TxtfieldDoglegperCL.text!)
        let endInc = Double(TxtfieldEndInclination.text!)
        if(inclinationlast < 0) {
            let azimuthresultlast = ( -rad2deg(acos((cos(deg2rad(doglegpercl!))-(cos(deg2rad(endInc!))*cos(deg2rad(Double(TxtfieldInclinationResult.text!)!))))/(sin(deg2rad(endInc!))*sin(deg2rad(Double(TxtfieldInclinationResult.text!)!))))))
        //    TxtfieldAzimuthresultlast.text = "\(Int(azimuthresultlast))"
              TxtfieldAzimuthresultlast.text = "\(Double(azimuthresultlast))"
        }
        else {
            let azimuthresultlast = rad2deg(acos((cos(deg2rad(doglegpercl!))-(cos(deg2rad(endInc!))*cos(deg2rad(Double(TxtfieldInclinationResult.text!)!))))/(sin(deg2rad(endInc!))*sin(deg2rad(Double(TxtfieldInclinationResult.text!)!)))))
          //  TxtfieldAzimuthresultlast.text = "\(Int(azimuthresultlast))"
            TxtfieldAzimuthresultlast.text = "\(Double(azimuthresultlast))"
        }
        }
        
        if(((TxtfieldAzimuthresultlast.text?.count) != 0) && ((TxtfieldEndAzimuth.text?.count) != 0)) {
        //Azimuth First result
             CalculateCondition = 2
       // let azimuthfirstresult =  Int(TxtfieldAzimuthresultlast.text!)! + Int(TxtfieldEndAzimuth.text!)!
         let azimuthfirstresult =  Double(TxtfieldAzimuthresultlast.text!)! + Double(TxtfieldEndAzimuth.text!)!
        if(azimuthfirstresult < 0){
          //  TxtfieldAzimuthresultfirst.text = "\(Int(TxtfieldAzimuthresultlast.text!)! + Int(TxtfieldEndAzimuth.text!)! + 360)"
            TxtfieldAzimuthresultfirst.text = "\(Double(TxtfieldAzimuthresultlast.text!)! + Double(TxtfieldEndAzimuth.text!)! + 360)"
        }
        else{
            if(azimuthfirstresult > 360){
//                TxtfieldAzimuthresultfirst.text = "\(Int(TxtfieldAzimuthresultlast.text!)! + Int(TxtfieldEndAzimuth.text!)! - 360)"
                TxtfieldAzimuthresultfirst.text = "\(Double(TxtfieldAzimuthresultlast.text!)! + Double(TxtfieldEndAzimuth.text!)! - 360)"
            }
            else {
//                TxtfieldAzimuthresultfirst.text = "\(Int(TxtfieldAzimuthresultlast.text!)! + Int(TxtfieldEndAzimuth.text!)!)"
                 TxtfieldAzimuthresultfirst.text = "\(Double(TxtfieldAzimuthresultlast.text!)! + Double(TxtfieldEndAzimuth.text!)!)"
            }
        }
        }
        
        
        if(((TxtfieldStartAzimuth.text?.count) != 0) && ((TxtfieldEndAzimuth.text?.count) != 0)) {
        //Raw result
         CalculateCondition = 2
      //  TxtfieldRawresult.text = "\(Int(TxtfieldEndAzimuth.text!)! - Int(TxtfieldStartAzimuth.text!)!)"
        TxtfieldRawresult.text = "\(Double(TxtfieldEndAzimuth.text!)! - Double(TxtfieldStartAzimuth.text!)!)"
        }
        
        
        if(((TxtfieldRawresult.text?.count) != 0)){
        // Actual Delta Az Result
             CalculateCondition = 2
     //   let Rawvalue = Int(TxtfieldRawresult.text!)!
        let Rawvalue = Double(TxtfieldRawresult.text!)!
        if( Rawvalue > 180 ){
           TxtfieldActualDeltaAz.text = "\(Rawvalue - 360)"
        }
        else if (Rawvalue < -180) {
            TxtfieldActualDeltaAz.text = "\(Rawvalue + 360)"
        }
        else {
            TxtfieldActualDeltaAz.text = "\(Rawvalue)"
        }
        }
        
        if(CalculateCondition == 1){
            self.popupAlert()
        }
     
    }
    
    
    //MARK: TextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        view.endEditing(true)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        let maxLength = 5
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength && string == numberFiltered
    }
    
  

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 

}
