//
//  ProjectionResultVC.swift
//  Samplesheet1
//
//  Created by Ratheesh TR on 07/01/19.
//  Copyright © 2019 Sankar. All rights reserved.
//

import UIKit

class ProjectionResultVC: UIViewController,UIScrollViewDelegate, UITextFieldDelegate {
    
    @IBOutlet var vwMain: UIView!
    @IBOutlet var resultView: UIView!
    @IBOutlet var previousResultView: UIView!
    @IBOutlet var scrollVw: UIScrollView!
    @IBOutlet weak var lblResultValue: UILabel!
    
    @IBOutlet var motorYield_txt: UITextField!
    @IBOutlet var amountSlid_txt: UITextField!
    @IBOutlet var currentInc_txt: UITextField!
    
    var projectionBit = ProjectionToBit()
    
    var motorYield = String()
    var amountSlid = String()
    var currentInc = String()
    var resultValue_Str = String()
    var resultValue_Double = Double()
    
    var graphFrame:CGRect!
    var graphCenter : CGPoint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        assignbackground()
        
        motorYield_txt.delegate = self
        amountSlid_txt.delegate = self
        currentInc_txt.delegate = self
        
        calculate()
        
        motorYield_txt.text = motorYield
        amountSlid_txt.text = amountSlid
        currentInc_txt.text = currentInc
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
      //  resultView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        
    }
    
    override func viewDidLayoutSubviews() {
        
        
    }
    
    //MARK: - Calculation Methods
    func calculate(){
        if motorYield.count != 0 && amountSlid.count != 0 && currentInc.count != 0 {
            let motoryield = Double(motorYield)
            let amountSlidValue = Double(amountSlid)
            let currentIncValue = Double(currentInc)
            let result = (motoryield! / 100 * amountSlidValue!) + currentIncValue!
            resultValue_Double = Double(result)
            let resultCheck =  (resultValue_Double*100).rounded()/100
            resultValue_Str = String(resultCheck)
            let motorYield_Str = String(motorYield)
       //     storeValue(val: motorYield_Str, Key: Constants.motorYield)
            Constants.appdelegateSharedInstance.initialDict[Constants.motorYield] = motorYield_Str
            
        //    print("\(resultCheck)")
            lblResultValue.text = resultValue_Str
            Constants.appdelegateSharedInstance.initialDict[Constants.projectedInc] = resultValue_Str
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        motorYield = motorYield_txt.text ??  ""
        amountSlid = amountSlid_txt.text ??  ""
        currentInc = currentInc_txt.text ??  ""
        
        
        switch textField{
        case motorYield_txt:
            if string == ""{
                motorYield.removeLast()
            }else{
                motorYield = motorYield + string
            }
        case amountSlid_txt:
            if string == ""{
                amountSlid.removeLast()
            }else{
                amountSlid = amountSlid + string
            }
        case currentInc_txt:
            if string == ""{
                currentInc.removeLast()
            }else{
                currentInc = currentInc + string
            }
        default:
            print("")
            
        }
        calculate()
        return true
    }
    
    
    @available(iOS 10.0, *)
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        
        switch textField{
        case motorYield_txt:
            if  motorYield == ""  {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter motorYield value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                calculate()
            }
            
        case amountSlid_txt:
            
            if amountSlid == "" {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter amountSlid value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else{
                calculate()
            }
            
        case currentInc_txt:
            if currentInc == "" {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Current Inclination value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                calculate()
            }
        default:
            print("")
        }
    }
    
    
    func assignbackground(){
        let background = UIImage(named: "background")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    //MARK: - gesture Recognizer methods
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
//                let tabBar = self.navigationController?.viewControllers[0] as? UITabBarController
//                if (tabBar?.selectedIndex)! > 0 {
//                    tabBar?.selectedIndex -= 1
//                }
                if (tabBarController?.selectedIndex)! > 0 {
                    tabBarController?.selectedIndex -= 1
                }
                self.navigationController?.popViewController(animated: true)
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
//                let tabBar = self.navigationController?.viewControllers[0] as? UITabBarController
//                tabBar?.selectedIndex += 1
                tabBarController?.selectedIndex += 1
                self.navigationController?.popViewController(animated: true)
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }

}
