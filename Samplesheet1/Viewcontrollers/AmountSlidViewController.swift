//
//  AmountSlidViewController.swift
//  Samplesheet1
//
//  Created by Sankar on 31/8/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit

class AmountSlidViewController: UIViewController {

    @IBOutlet var vwTextFiels: UIView!
    @IBOutlet var txtAmount: UITextField!
    @IBOutlet var lblQuestion: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uiEliments()
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        view.addGestureRecognizer(leftSwipe)
    
    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        
        print("Swipe Left")
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MotorYieldViewController") as? MotorYieldViewController
        vc?.amountYield = txtAmount.text!
        self.navigationController?.pushViewController(vc!, animated: true)
        
        
    }
    
    
    func uiEliments(){
        
        txtAmount.textAlignment = .center
        
        // border radius
        vwTextFiels.layer.cornerRadius = 15.0
        
        // border
        vwTextFiels.layer.borderColor = UIColor.lightGray.cgColor
        vwTextFiels.layer.borderWidth = 1.5
        
        // drop shadow
        vwTextFiels.layer.shadowColor = UIColor.black.cgColor
        vwTextFiels.layer.shadowOpacity = 0.8
        vwTextFiels.layer.shadowRadius = 3.0
        vwTextFiels.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                //right view controller
                self.navigationController?.popViewController(animated: true)
            case UISwipeGestureRecognizerDirection.left:
                //left view controller
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CurrentInclinationViewController") as? CurrentInclinationViewController
                vc?.amountYield = txtAmount.text!
                self.navigationController?.pushViewController(vc!, animated: true)
            default:
                break
            }
        }
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
