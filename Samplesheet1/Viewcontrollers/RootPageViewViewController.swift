//
//  RootPageViewViewController.swift
//  Samplesheet1
//
//  Created by Sankar on 11/9/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit

class RootPageViewViewController: UIPageViewController, UIPageViewControllerDataSource {
   
    lazy var viewControllerList:[UIViewController] = {
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        
        let vc1 = sb.instantiateViewController(withIdentifier: "AmountSlidViewController")
        let vc2 = sb.instantiateViewController(withIdentifier: "MotorYieldViewController")
        let vc3 = sb.instantiateViewController(withIdentifier: "CurrentInclinationViewController")
        let vc4 = sb.instantiateViewController(withIdentifier: "CurrentAzimuthViewController")
        let vc5 = sb.instantiateViewController(withIdentifier: "ToolfaceViewController")
        let vc6 = sb.instantiateViewController(withIdentifier: "ResultsViewController")
        
        return [vc1, vc2, vc3, vc4, vc5]
        
    }()
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = self
        
        if let firstViewController = viewControllerList.first {
            self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let vcIndex = viewControllerList.index(of: viewController) else {return nil}
        
        let previousIndex = vcIndex - 1
        
        guard previousIndex >= 0 else {return nil}
        
        guard viewControllerList.count > previousIndex else {return nil}
        
        return viewControllerList[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let vcIndex = viewControllerList.index(of: viewController) else {return nil}
        
        let nextIndex = vcIndex + 1
        
        guard viewControllerList.count != nextIndex else {return nil}
        
        guard viewControllerList.count > nextIndex else {return nil}

        return viewControllerList[nextIndex]
    }

 

}
