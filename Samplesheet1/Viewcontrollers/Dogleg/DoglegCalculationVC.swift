//
//  DoglegCalculationVC.swift
//  Samplesheet1
//
//  Created by Mohanapriya on 10/05/19.
//  Copyright © 2019 Sankar. All rights reserved.
//

import UIKit

class DoglegCalculationVC: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UIScrollViewDelegate,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var calcButtons: [UIButton]!
    let titleText = ["Depth(ft) - Survey1 ?","Inclination (degree) - Survey1 ?","Azimuth (degree) - Surey1 ?", "Depth(ft) - Survey2 ?","Inclination (degree) - Survey2 ?","Azimuth (degree) - Surey2 ?"]
    
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    var changingSign = false
    var operation = false
    var isIncreasing = true
    var curretPage:Int = 0
    var slide = 0
    var dogLeg = DogLegValues()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        dogLeg.depth = Constants.appdelegateSharedInstance.initialDict[Constants.depth] ?? ""
        dogLeg.inclination = Constants.appdelegateSharedInstance.initialDict[Constants.inclination] ?? ""
        dogLeg.azimuth = Constants.appdelegateSharedInstance.initialDict[Constants.azimuth] ?? ""
        dogLeg.depth1 = Constants.appdelegateSharedInstance.initialDict[Constants.depth1] ?? ""
        dogLeg.inclination1 = Constants.appdelegateSharedInstance.initialDict[Constants.inclination1] ?? ""
        dogLeg.azimuth1 = Constants.appdelegateSharedInstance.initialDict[Constants.azimuth1] ?? ""
    }
    
    @IBAction func calcButtonTapped(_ sender: UIButton) {
        
        let buttonposition = sender.convert(CGPoint.zero, to: mainCollectionView)
        let index = mainCollectionView.indexPathForItem(at: buttonposition)
        
        switch sender.tag {
            
        case 0:
            Addnumberfunc("0", index!)
        case 1:
            Addnumberfunc("1", index!)
        case 2:
            Addnumberfunc("2", index!)
        case 3:
            Addnumberfunc("3", index!)
        case 4:
            Addnumberfunc("4", index!)
        case 5:
            Addnumberfunc("5", index!)
        case 6:
            Addnumberfunc("6", index!)
        case 7:
            Addnumberfunc("7", index!)
        case 8:
            Addnumberfunc("8", index!)
        case 9:
            Addnumberfunc("9", index!)
        case 11:
            decimalPointPressed(index!)
        case 10:
            plusMinusAction(index!)
        default:
            print("dfgdfg")
        }
    }
    
    
    
    @IBAction func bsaction(_ sender: UIButton) {
        let buttonposition = sender.convert(CGPoint.zero, to: mainCollectionView)
        let index = mainCollectionView.indexPathForItem(at: buttonposition)
        
        let cell = mainCollectionView.cellForItem(at: index!) as! CalculatorCollectionViewCell
        
        if cell.valueLbl.text != ""
        {
            cell.valueLbl.text?.removeLast()
            setvalues(cell.valueLbl.text!, index!)
        }
        else {
            setvalues("", index!)
        }
    }
    
    func plusMinusAction(_ index:IndexPath)
    {
        let cell = mainCollectionView.cellForItem(at: index) as! CalculatorCollectionViewCell
        
        if (cell.valueLbl.text == "") {
            return
        } else {
            if !changingSign {
                changingSign = true
                cell.valueLbl.text = "-" + cell.valueLbl.text!
            } else {
                changingSign = false
                cell.valueLbl.text?.removeFirst()
            }
        }
        setvalues(cell.valueLbl.text!, index)
    }
    
    @IBAction func percentageAction(_ sender: Any) {
        
        
    }
    
    func Addnumberfunc(_ number:String, _ index:IndexPath)
    {
        
        let cell = mainCollectionView.cellForItem(at: index) as! CalculatorCollectionViewCell
        var textnum = ""
        if let num = cell.valueLbl.text{
            
            //   textnum = String(cell.valueLbl.text!)
            textnum = num
        }
        if operation {
            
            textnum = ""
            operation = false
        }
        textnum = textnum + number
        
        cell.valueLbl.text = textnum
        
        cell.valueLbl.layer.borderWidth = 0.6
//        cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
        cell.valueLbl.layer.masksToBounds = true
        
        setvalues(textnum,index)
    }
    
    
    func setvalues(_ textnum : String ,_ index:IndexPath){
        switch index.item {
        case 0:
            dogLeg.depth = textnum
            Constants.appdelegateSharedInstance.initialDict[Constants.depth] = textnum
            
        case 1:
            dogLeg.inclination = textnum
            Constants.appdelegateSharedInstance.initialDict[Constants.inclination] = textnum
            
        case 2:
            dogLeg.azimuth = textnum
            Constants.appdelegateSharedInstance.initialDict[Constants.azimuth] = textnum
            
        case 3:
            dogLeg.depth1 = textnum
            Constants.appdelegateSharedInstance.initialDict[Constants.depth1] = textnum
            
        case 4:
            dogLeg.inclination1 = textnum
            Constants.appdelegateSharedInstance.initialDict[Constants.inclination1] = textnum
            
        case 5:
            dogLeg.azimuth1 = textnum
            Constants.appdelegateSharedInstance.initialDict[Constants.azimuth1] = textnum
        default:
            print("default")
        }
    }
    
    func decimalPointPressed(_ index:IndexPath)
    {
        let cell = mainCollectionView.cellForItem(at: index) as! CalculatorCollectionViewCell
        guard let text = cell.valueLbl.text, !text.contains(".") else {
            return }
        if cell.valueLbl.text?.count == 0
        {
            cell.valueLbl.text = "0."
        }
        else
        {
            cell.valueLbl.text = text + "."
        }
    }
    
    @IBAction func acaction(_ sender: UIButton) {
        let buttonposition = sender.convert(CGPoint.zero, to: mainCollectionView)
        let index = mainCollectionView.indexPathForItem(at: buttonposition)
        let cell = mainCollectionView.cellForItem(at: index!) as! CalculatorCollectionViewCell
        
        
        switch index?.item {
        case 0:
            dogLeg.depth = nil
            Constants.appdelegateSharedInstance.initialDict[Constants.depth] = nil
        case 1:
            dogLeg.inclination = nil
            Constants.appdelegateSharedInstance.initialDict[Constants.inclination] = nil
        case 2:
            dogLeg.azimuth = nil
            Constants.appdelegateSharedInstance.initialDict[Constants.azimuth] = nil
            
        case 3:
            dogLeg.depth1 = nil
            Constants.appdelegateSharedInstance.initialDict[Constants.depth1] = nil
        case 4:
            dogLeg.inclination1 = nil
            Constants.appdelegateSharedInstance.initialDict[Constants.inclination1] = nil
        case 5:
            dogLeg.azimuth1 = nil
            Constants.appdelegateSharedInstance.initialDict[Constants.azimuth1] = nil
        default:
            print("default")
        }
        cell.valueLbl.text = ""
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CalculatorCollectionViewCell
        cell.enterBtn.tag = indexPath.item
        cell.titleLbl.text = titleText[indexPath.item]
        cell.signBtn.isEnabled = false
        
        cell.valueLbl.layer.borderWidth = 0
        cell.valueLbl.layer.borderColor = UIColor.clear.cgColor
        cell.valueLbl.layer.masksToBounds = true
        switch indexPath.item {
        case 0:
            if let val = dogLeg.depth{
                cell.valueLbl.text = val
                Constants.appdelegateSharedInstance.initialDict[Constants.depth] = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
        case 1:
            if let val = dogLeg.inclination{
                cell.valueLbl.text = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
        case 2:
            if let val = dogLeg.azimuth{
                cell.valueLbl.text = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
            
        case 3:
            if let val = dogLeg.depth1{
                cell.valueLbl.text = val
                Constants.appdelegateSharedInstance.initialDict[Constants.depth1] = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
        case 4:
            if let val = dogLeg.inclination1{
                cell.valueLbl.text = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
        case 5:
            if let val = dogLeg.azimuth1{
                cell.valueLbl.text = val
                cell.valueLbl.layer.borderWidth = 0.6
//                cell.valueLbl.layer.borderColor = UIColor.darkGray.cgColor
                cell.valueLbl.layer.masksToBounds = true
            }
            else{
                cell.valueLbl.text = ""
            }
        default:
            cell.titleLbl.text = ""
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: mainCollectionView.frame.width, height: mainCollectionView.frame.height)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    @IBAction func enterAction(_ sender: UIButton) {
        
        let currentIndex = mainCollectionView.contentOffset.x / mainCollectionView.frame.size.width;
        print(Int(currentIndex))
        let indexPath = IndexPath(item: Int(currentIndex + 1), section: 0)
        pageControl.currentPage = NSInteger(ceil(currentIndex))
        
        let indexPath1 = IndexPath(item: Int(currentIndex), section: 0)
        let cell = mainCollectionView.cellForItem(at: indexPath1) as! CalculatorCollectionViewCell
        switch sender.tag {
        case 0:
            mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
            if let val = dogLeg.depth {
                cell.valueLbl.text = val
            }
            else{
                cell.valueLbl.text = ""
                
            }
        // print(cell.titleLbl.text!)
        case 1:
            mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
            if let val = dogLeg.inclination {
                cell.valueLbl.text = val
            }
            else{
                cell.valueLbl.text = ""
            }
            
        case 2:
            mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
            if let val = dogLeg.azimuth {
                cell.valueLbl.text = val
            }
            else{
                cell.valueLbl.text = ""
                
            }
        // print(cell.titleLbl.text!)
        case 3:
            mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
            if let val = dogLeg.depth1 {
                cell.valueLbl.text = val
            }
            else{
                cell.valueLbl.text = ""
            }
        //   print(cell.titleLbl.text!)
            
        case 4:
            mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
            if let val = dogLeg.inclination1 {
                cell.valueLbl.text = val
            }
            else{
                cell.valueLbl.text = ""
            }
        case 5:
            if (dogLeg.depth == nil && dogLeg.inclination == nil && dogLeg.azimuth == nil && dogLeg.depth1 == nil && dogLeg.inclination1 == nil && dogLeg.azimuth1 == nil) || (dogLeg.depth == "" && dogLeg.inclination == "" && dogLeg.azimuth == "" && dogLeg.depth1 == "" && dogLeg.inclination1 == "" && dogLeg.azimuth1 == ""){
                
                let alert = UIAlertController(title:"Alert", message:"Please fill all the fields", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else if dogLeg.depth == nil || dogLeg.depth == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Depth value for survey 1", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else if dogLeg.inclination == nil || dogLeg.inclination == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Inclination value for survey 1", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if dogLeg.azimuth == nil || dogLeg.azimuth == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Azimuth value for survey 1", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
                
            else if dogLeg.depth1 == nil || dogLeg.depth1 == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Depth value for survey 2", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else if dogLeg.inclination1 == nil || dogLeg.inclination1 == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Inclination value for survey 2", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else if dogLeg.azimuth1 == nil || dogLeg.azimuth1 == ""{
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Azimuth value for survey 2", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DoglegResultVC") as? DoglegResultVC

                vc?.depth = dogLeg.depth!
                vc?.inclination = dogLeg.inclination!
                vc?.azimuth = dogLeg.azimuth!
                
                vc?.depth1 = dogLeg.depth1!
                vc?.inclination1 = dogLeg.inclination1!
                vc?.azimuth1 = dogLeg.azimuth1!

                // self.present(vc!, animated: true, completion: nil)
                self.navigationController?.pushViewController(vc!, animated: true)
                slide = 0
            }
            
            
        default:
            print(cell.titleLbl.text!)
            
        }
        
        // print("Current IndexPath:\(indexPath)")
    }
    
    //MARK: Page Controller methods
    
    
    
    @IBAction func currentPageIndexMethod(_ sender: Any) {
        let currentIndex = mainCollectionView.contentOffset.x / mainCollectionView.frame.size.width
        print(Int(currentIndex))
        
        if isIncreasing  && currentIndex <= 5{
            
            let indexPath1 = IndexPath(item: Int(currentIndex), section: 0)
            let cell = mainCollectionView.cellForItem(at: indexPath1) as! CalculatorCollectionViewCell
            
            self.enterAction(cell.enterBtn)
            if currentIndex == 5{
                isIncreasing = false
            }
        }
        else{
            if currentIndex > 0 {
                let indexPath = IndexPath(item: Int(currentIndex - 1), section: 0)
                let indexPath1 = IndexPath(item: Int(currentIndex), section: 0)
                // pageControl.currentPage = NSInteger(ceil(currentIndex - 1))
                let cell = mainCollectionView.cellForItem(at: indexPath1) as! CalculatorCollectionViewCell
                switch currentIndex - 1 {
                case 0:
                    mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
                    if let val = dogLeg.depth {
                        cell.valueLbl.text = val
                        Constants.appdelegateSharedInstance.initialDict[Constants.depth] = val
                    }
                    else{
                        cell.valueLbl.text = ""
                        Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity] = ""
                    }
                case 1:
                    mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
                    if let val = dogLeg.inclination {
                        cell.valueLbl.text = val
                    }
                    else{
                        cell.valueLbl.text = ""
                    }
                    
                case 2:
                    mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
                    if let val = dogLeg.azimuth {
                        cell.valueLbl.text = val
                        Constants.appdelegateSharedInstance.initialDict[Constants.depth] = val
                    }
                    else{
                        cell.valueLbl.text = ""
                        Constants.appdelegateSharedInstance.initialDict[Constants.dogLegSeverity] = ""
                    }
                case 3:
                    mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
                    if let val = dogLeg.depth1 {
                        cell.valueLbl.text = val
                    }
                    else{
                        cell.valueLbl.text = ""
                    }
                    
                case 4:
                    mainCollectionView.scrollToItem(at: indexPath , at: .centeredHorizontally , animated: true)
                    if let val = dogLeg.inclination1 {
                        cell.valueLbl.text = val
                    }
                    else{
                        cell.valueLbl.text = ""
                    }
                default:
                    print(cell.titleLbl.text!)
                }
            }
            else{
                isIncreasing = true
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let currentIndex = mainCollectionView.contentOffset.x / mainCollectionView.frame.size.width;
        print(Int(currentIndex))
        pageControl.currentPage = NSInteger(ceil(currentIndex))
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = mainCollectionView.contentOffset
        visibleRect.size = mainCollectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = mainCollectionView.indexPathForItem(at: visiblePoint) else {
            return
        }
        let cell = mainCollectionView.cellForItem(at: indexPath) as! CalculatorCollectionViewCell
        
        switch indexPath.item {
        case 0:
            cell.valueLbl.text =  dogLeg.depth
        // print(cell.titleLbl.text!)
        case 1:
            cell.valueLbl.text = dogLeg.inclination
        case 2:
            cell.valueLbl.text = dogLeg.azimuth
        case 3:
            cell.valueLbl.text = dogLeg.depth1
        case 4:
            cell.valueLbl.text = dogLeg.inclination1
        // print(cell.titleLbl.text!)
        case 5:
            if slide == 1{
                if (dogLeg.depth == nil && dogLeg.inclination == nil && dogLeg.azimuth == nil && dogLeg.depth1 == nil && dogLeg.inclination1 == nil && dogLeg.azimuth1 == nil) || (dogLeg.depth == "" && dogLeg.inclination == "" && dogLeg.azimuth == "" && dogLeg.depth1 == "" && dogLeg.inclination1 == "" && dogLeg.azimuth1 == ""){
                    
                    let alert = UIAlertController(title:"Alert", message:"Please fill all the fields", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else if dogLeg.depth == nil || dogLeg.depth == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter Depth value for survey 1", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                    
                    self.present(alert, animated: true, completion: nil)
                }
                else if dogLeg.inclination == nil || dogLeg.inclination == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter Inclination value for survey 1", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                else if dogLeg.azimuth == nil || dogLeg.azimuth == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter course length value", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                else if dogLeg.depth1 == nil || dogLeg.depth1 == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter Azimuth value for survey 1", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                    
                    self.present(alert, animated: true, completion: nil)
                }
                else if dogLeg.inclination1 == nil || dogLeg.inclination1 == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter Inclination value for survey 2", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                else if dogLeg.azimuth1 == nil || dogLeg.azimuth1 == ""{
                    
                    let alert = UIAlertController(title:"Alert", message:"Please enter Azimuth value for survey 2", preferredStyle: .alert)
                    let action = UIAlertAction(title:"OK", style: .default)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DoglegResultVC") as? DoglegResultVC

                    vc?.depth = dogLeg.depth!
                    vc?.inclination = dogLeg.inclination!
                    vc?.azimuth = dogLeg.azimuth!
                    
                    vc?.depth1 = dogLeg.depth1!
                    vc?.inclination1 = dogLeg.inclination1!
                    vc?.azimuth1 = dogLeg.azimuth1!

                    // self.present(vc!, animated: true, completion: nil)
                    self.navigationController?.pushViewController(vc!, animated: true)
                    slide = 0
                }
            }
            else{
                slide = 1
            }
            
        default:
            print(cell.titleLbl.text!)
            
        }
        //    print("Curren IndexPath:\(indexPath)")
    }
    
}
