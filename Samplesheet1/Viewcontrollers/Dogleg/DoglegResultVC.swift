//
//  DoglegResultVC.swift
//  Samplesheet1
//
//  Created by Mohanapriya on 10/05/19.
//  Copyright © 2019 Sankar. All rights reserved.
//

import UIKit

class DoglegResultVC: UIViewController,UIScrollViewDelegate, UITextFieldDelegate {
    
    @IBOutlet var vwMain: UIView!
    @IBOutlet var scrollVw: UIScrollView!
    @IBOutlet weak var lblResultValue: UILabel!
    
    @IBOutlet var depth_txt: UITextField!
    @IBOutlet var inclination_txt: UITextField!
    @IBOutlet var azimuth_txt: UITextField!
    
    @IBOutlet var depth1_txt: UITextField!
    @IBOutlet var inclination1_txt: UITextField!
    @IBOutlet var azimuth1_txt: UITextField!
    
    var dogLeg = DogLegValues()
    
    var depth = String()
    var inclination = String()
    var azimuth = String()
    
    var depth1 = String()
    var inclination1 = String()
    var azimuth1 = String()
    
    var resultValue_Str = String()
    var resultValue_Double = Double()

    override func viewDidLoad() {
        super.viewDidLoad()

        assignbackground()
        
        depth_txt.delegate = self
        inclination_txt.delegate = self
        azimuth_txt.delegate = self
        
        depth1_txt.delegate = self
        inclination1_txt.delegate = self
        azimuth1_txt.delegate = self
        
        calculate()
        
        
        depth_txt.text = depth
        inclination_txt.text = inclination
        azimuth_txt.text = azimuth
        
        depth1_txt.text = depth1
        inclination1_txt.text = inclination1
        azimuth1_txt.text = azimuth1
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        //resultView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
    }
    

    override func viewDidLayoutSubviews() {
        
        
        
    }
    
    //MARK: - Calculation Methods
    func calculate(){
        if depth.count != 0 && inclination.count != 0 && azimuth.count != 0 && depth1.count != 0 && inclination1.count != 0 && azimuth1.count != 0 {
            let depthD = Double(depth)
            let inclinationD = Double(inclination)
            let azimuthD = Double(azimuth)
            let depth1D = Double(depth1)
            let inclination1D = Double(inclination1)
            let azimuth1D = Double(azimuth1)
            let val1 = depth1D! - depthD!
            
          
            let val2 = acos((cos(inclinationD! * Double.pi / 180) * cos(inclination1D! * Double.pi / 180)) + (sin(inclinationD! * Double.pi / 180) * sin(inclination1D! * Double.pi / 180) * cos((azimuth1D! - azimuthD!) * Double.pi / 180)))
            let result  = (100 / val1) * val2  * (180 / Double.pi)
            
            resultValue_Double = Double(result)
            let resultCheck =  (resultValue_Double*100).rounded()/100
            resultValue_Str = String(resultCheck)
            //     storeValue(val: resultValue_Str, Key: Constants.motorYield)
            Constants.appdelegateSharedInstance.initialDict[Constants.motorYield] = resultValue_Str
            print("\(resultValue_Str)")
            
            lblResultValue.text = resultValue_Str
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        depth = depth_txt.text ??  ""
        inclination = inclination_txt.text ??  ""
        azimuth = azimuth_txt.text ?? ""
        depth1 = depth1_txt.text ??  ""
        inclination1 = inclination1_txt.text ??  ""
        azimuth1 = azimuth1_txt.text ?? ""
        
        switch textField{
        case depth_txt:
            if string == ""{
                depth.removeLast()
            }else{
                depth = depth + string
            }
        case inclination_txt:
            if string == ""{
                inclination.removeLast()
            }else{
                inclination = inclination + string
            }
        case azimuth_txt:
            if string == ""{
                azimuth.removeLast()
            }else{
                azimuth = azimuth + string
            }
            
        case depth1_txt:
            if string == ""{
                depth1.removeLast()
            }else{
                depth1 = depth1 + string
            }
        case inclination1_txt:
            if string == ""{
                inclination1.removeLast()
            }else{
                inclination1 = inclination1 + string
            }
        case azimuth1_txt:
            if string == ""{
                azimuth1.removeLast()
            }else{
                azimuth1 = azimuth1 + string
            }
            
        default:
            print("")
        }
        
        calculate()
        
        return true
    }
    
    @available(iOS 10.0, *)
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        
        switch textField{
        case depth_txt:
            if  depth == ""  {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Depth value for Survey 1", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                calculate()
            }
            
        case inclination_txt:
            
            if inclination == "" {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Inclination value for survey 1", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else{
                calculate()
                
            }
            
        case azimuth_txt:
            if azimuth == "" {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Azimuth value for survey1", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                calculate()
            }
            
        case depth1_txt:
            if  depth1 == ""  {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Depth value for Survey 2", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                calculate()
            }
            
        case inclination1_txt:
            
            if inclination1 == "" {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Inclination value for survey 2", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else{
                calculate()
                
            }
            
        case azimuth1_txt:
            if azimuth1 == "" {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter Azimuth value for survey 2", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                calculate()
            }
            
        default:
            print("")
        }
    }
    
    
    func assignbackground(){
        let background = UIImage(named: "background")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    //MARK: - Gesture Recognizer methods
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
                //                let tabBar = self.navigationController?.viewControllers[0] as? UITabBarController
                //                if (tabBar?.selectedIndex)! > 0 {
                //                tabBar?.selectedIndex -= 1
                //                }
                self.tabBarController?.selectedIndex = 0
                self.navigationController?.popViewController(animated: true)
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                //                let tabBar = self.navigationController?.viewControllers[0] as? UITabBarController
                //                tabBar?.selectedIndex += 1
                self.tabBarController?.selectedIndex += 1
                self.navigationController?.popViewController(animated: true)
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }

}
