//
//  ToolfaceViewController.swift
//  Samplesheet1
//
//  Created by Sankar on 31/8/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit

class ToolfaceViewController: UIViewController {

    @IBOutlet var vwTextFiels: UIView!
    @IBOutlet var txtAmount: UITextField!
    @IBOutlet var lblQuestion: UILabel!
    var amountYield = String()
    var motorYield = String()
    var currentInclination = String()
    var currentAzimuth = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
         uiEliments()

        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        view.addGestureRecognizer(leftSwipe)
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        rightSwipe.direction = .right
        view.addGestureRecognizer(rightSwipe)
        

    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
    
        if (sender.direction == .right) {
            print("Swipe Right")
            self.navigationController?.popViewController(animated: true)
        }
        
        if (sender.direction == .left) {
        print("Swipe Left")
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ResultsViewController") as? ResultsViewController
            vc?.amountYield = amountYield
            vc?.motorYield = motorYield
            vc?.currentInclination = currentInclination
            vc?.currentAzimuth = currentAzimuth
            vc?.toolface = txtAmount.text!
        self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
    func uiEliments(){
        
        txtAmount.textAlignment = .center
        
        // border radius
        vwTextFiels.layer.cornerRadius = 15.0
        
        // border
        vwTextFiels.layer.borderColor = UIColor.lightGray.cgColor
        vwTextFiels.layer.borderWidth = 1.5
        
        // drop shadow
        vwTextFiels.layer.shadowColor = UIColor.black.cgColor
        vwTextFiels.layer.shadowOpacity = 0.8
        vwTextFiels.layer.shadowRadius = 3.0
        vwTextFiels.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
    }


}
