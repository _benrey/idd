//
//  BuildrateVC.swift
//  Samplesheet1
//
//  Created by Sankar on 24/5/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit



class BuildrateVC: UIViewController {

    @IBOutlet var TxtfieldBuildrate: GSBaseTextField!
    @IBOutlet var TxtfieldTargetTVD: GSBaseTextField!
    @IBOutlet var TxtfieldTargetIncl: GSBaseTextField!
    @IBOutlet var TxtfieldcurrentTVD: GSBaseTextField!
    @IBOutlet var TxtfieldCurrentIncl: GSBaseTextField!
    var  CalculateCondition : Int!
    override func viewDidLoad() {
        super.viewDidLoad()
         CalculateCondition = 1
        self.hideKeyboardWhenTappedAround()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func Action_Calculate(_ sender: Any) {
      if(((TxtfieldCurrentIncl.text?.count) != 0) && ((TxtfieldcurrentTVD.text?.count) != 0) && ((TxtfieldTargetIncl.text?.count) != 0) && ((TxtfieldTargetTVD.text?.count) != 0))  {
        CalculateCondition = 2
       let currentIncl = Double(TxtfieldCurrentIncl.text!)
       let currentTVD = Double(TxtfieldcurrentTVD.text!)
       let targetIncl = Double(TxtfieldTargetIncl.text!)
       let targetTVD = Double(TxtfieldTargetTVD.text!)
        
       let buildrate = ((sin(deg2rad(targetIncl!))-sin(deg2rad(currentIncl!)))*5729.58) / (Double(targetTVD!) - Double(currentTVD!))
        
       TxtfieldBuildrate.text = "\(Float(buildrate))"
        }
        
        if(CalculateCondition == 1){
            self.popupAlert()
        }
        
    }
    
    //MARK: TextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        view.endEditing(true)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        let maxLength = 5
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength && string == numberFiltered
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
