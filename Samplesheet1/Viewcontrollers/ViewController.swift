//
//  ViewController.swift
//  Samplesheet1
//
//  Created by Sankar on 21/5/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {

    @IBOutlet var DetailsTblview: UITableView!
    @IBOutlet var Details_View: UIView!
    @IBOutlet var Scrollview: UIScrollView!
    @IBOutlet var TxtfieldBitSensor: GSBaseTextField!
    @IBOutlet var btnEye: UIButton!
    
    var count = 0
    var timer:Timer?
    var timerAfterFiveSec:Timer?
    var secondsCount = 0
    var twoTapsCount = 0
    var totalSeconds = 1
    var CourselengthArray  = NSMutableArray()
    var DepthArray         = NSMutableArray()
    var SurveyArray        = NSMutableArray()
    var IncArray           = NSMutableArray()
    var AziArray           = NSMutableArray()
    var SeenArray          = NSMutableArray()
    var DiffArray          = NSMutableArray()
    var AheadArray         = NSMutableArray()
    var AvgTFArray         = NSMutableArray()
    var FTSlideArray       = NSMutableArray()
    var StartSLArray       = NSMutableArray()
    var StopSLArray        = NSMutableArray()
    var BRFTArray          = NSMutableArray()
    var BR30Array          = NSMutableArray()
    var RequireArray       = NSMutableArray()
    var lblAheadArray      = NSMutableArray()
    var lblSeenArray       = NSMutableArray()
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        UpdateArrayvalue()
        self.tabBarController?.tabBar.isHidden = true
    }
   
    func UpdateArrayvalue () {
     for i in 0...99 {
        RequireArray.add("")
        BR30Array.add("")
        BRFTArray.add("")
        if(i == 0){
        StopSLArray.add("0")
        }
        else {
         StopSLArray.add("")
        }
        StartSLArray.add("")
        FTSlideArray.add("")
        AvgTFArray.add("")
        AheadArray.add("")
        DiffArray.add("")
        SeenArray.add("")
        AziArray.add("")
        IncArray.add("")
        SurveyArray.add("")
        DepthArray.add("")
        CourselengthArray.add("")
        lblAheadArray.add("")
        lblSeenArray.add("")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
      
        Scrollview.contentSize = CGSize(width: Details_View.frame.size.width , height: Scrollview.frame.size.height)
        Scrollview.showsVerticalScrollIndicator = false
        Scrollview.showsHorizontalScrollIndicator = false
    }
    
    //MARK: Tableview Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = DetailsTblview.dequeueReusableCell(withIdentifier:"Cell", for: indexPath ) as! DetailTableViewCell
        cell.lblSerialNo.text = String(indexPath.row+1)
        cell.TxtfieldStopSL.delegate = self
        cell.TxtfieldDepth.delegate = self
        cell.TxtfieldINC.delegate = self
        cell.TxtFieldAZI.delegate = self
        cell.TxtfieldStartSL.delegate = self
        if(indexPath.row == 0){
            cell.TxtfieldStopSL.isUserInteractionEnabled = false
            cell.TxtfieldStartSL.isUserInteractionEnabled = false
            cell.TxtfieldINC.isUserInteractionEnabled = false
            cell.TxtFieldAZI.isUserInteractionEnabled = false
        }
        else {
            cell.TxtfieldStopSL.isUserInteractionEnabled = true
            cell.TxtfieldStartSL.isUserInteractionEnabled = true
            cell.TxtfieldINC.isUserInteractionEnabled = true
            cell.TxtFieldAZI.isUserInteractionEnabled = true
        }
    //    if((StopSLArray[indexPath.row] as! String != "") && (StartSLArray[indexPath.row] as! String != "")){
            cell.lblFTSlide.text = FTSlideArray[indexPath.row] as? String
   //     }
        cell.TxtfieldDepth.text = DepthArray[indexPath.row] as? String
        cell.TxtfieldStartSL.text = StartSLArray[indexPath.row] as? String
        cell.TxtfieldStopSL.text = StopSLArray[indexPath.row] as? String
        cell.lblSurvey.text = SurveyArray[indexPath.row] as? String
        cell.lblCourseLength.text = CourselengthArray[indexPath.row] as? String
        if (indexPath.row != 0){
            cell.lblDiff.text = DiffArray[indexPath.row-1] as? String
        }
          cell.lblAhead.text = lblAheadArray[indexPath.row] as? String
        cell.lblSeen.text = lblSeenArray[indexPath.row] as? String
        cell.TxtfieldINC.text = IncArray [indexPath.row] as? String
        return cell
    }
    
    //MARK: TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method

    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        view.endEditing(true)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        if(textField.tag != 666 ){
        let pointInTable = textField.convert(textField.bounds.origin, to: self.DetailsTblview)
        let textFieldIndexPath = self.DetailsTblview.indexPathForRow(at: pointInTable)
        if(textField.tag == 22){
            StopSLArray.replaceObject(at:textFieldIndexPath!.row, with: textField.text as Any)
        }
        else if(textField.tag == 12){
            StartSLArray.replaceObject(at:textFieldIndexPath!.row, with: textField.text as Any)
        }
        else if(textField.tag == 2){
            DepthArray.replaceObject(at:textFieldIndexPath!.row, with: textField.text as Any)
        }
        else if(textField.tag == 15){
            IncArray.replaceObject(at:textFieldIndexPath!.row, with: textField.text as Any)
            }
        if((StopSLArray[textFieldIndexPath!.row] as! String != "") && (StartSLArray[textFieldIndexPath!.row] as! String != "")){
            let stop = StopSLArray[textFieldIndexPath!.row] as! String
            let start = StartSLArray[textFieldIndexPath!.row] as! String
            let a:Int! = Int(stop)
            let b:Int! = Int(start)
            FTSlideArray.replaceObject(at:textFieldIndexPath!.row, with: String(a-b) as Any)
        }
        if((DepthArray[textFieldIndexPath!.row] as! String != "") && (TxtfieldBitSensor.text != "")){
                let depth = DepthArray[textFieldIndexPath!.row] as! String
                let a:Int! = Int(depth)
                let b:Int! = Int(TxtfieldBitSensor.text!)
                SurveyArray.replaceObject(at:textFieldIndexPath!.row, with: String(a-b) as Any)
               // cell.lblSurvey.text = SurveyArray[indexPath.row] as? String
        }
          if(textFieldIndexPath!.row != 0 && (DepthArray[textFieldIndexPath!.row] as! String != "")){
                let depth = DepthArray[textFieldIndexPath!.row] as! String
                let prevDepth = DepthArray[textFieldIndexPath!.row-1] as! String
                let a:Int! = Int(depth)
                let b:Int! = Int(prevDepth)
                CourselengthArray.replaceObject(at:textFieldIndexPath!.row, with: String(a-b) as Any)
//                cell.lblCourseLength.text = CourselengthArray[indexPath.row] as? String
            }
         if((DepthArray[textFieldIndexPath!.row] as! String != "") && (StopSLArray[textFieldIndexPath!.row] as! String != "")){
            if ((textFieldIndexPath!.row == 0) && (SurveyArray[textFieldIndexPath!.row+1] as! String != "")) {
                    let prevStop = "0"
                    let survey = SurveyArray[textFieldIndexPath!.row+1] as! String
                    let a:Int! = Int(prevStop)
                    let b:Int! = Int(survey)
                    //cell.lblDiff.text = String(a-b)
                    if(prevStop == "0"){
                        DiffArray.replaceObject(at:textFieldIndexPath!.row, with: String(a-b) as Any)
                    }
                }
                else {
                    let prevStop = StopSLArray[textFieldIndexPath!.row] as! String
                    let survey = SurveyArray[textFieldIndexPath!.row+1] as! String
                    let a:Int! = Int(prevStop)
                    let b:Int! = Int(survey)
                    if(survey != ""){
                        DiffArray.replaceObject(at:textFieldIndexPath!.row, with: String(a-b) as Any)
                       // cell.lblDiff.text = DiffArray[textFieldIndexPath!.row-1] as? String
                    }
                }
            }
            if((SurveyArray[textFieldIndexPath!.row] as! String != "") && (StopSLArray[textFieldIndexPath!.row] as! String != "") && (FTSlideArray[textFieldIndexPath!.row] as! String != "") && (StartSLArray[textFieldIndexPath!.row] as! String != "") ){
                let prevStop = StopSLArray[textFieldIndexPath!.row] as! String
                let Stop = StopSLArray[textFieldIndexPath!.row+1] as! String
                let survey = SurveyArray[textFieldIndexPath!.row+1] as! String
                let FTSlide =  FTSlideArray[textFieldIndexPath!.row+1] as! String
                let Start =   StartSLArray[textFieldIndexPath!.row+1] as! String
                let a:Int! = Int(prevStop)
                let b:Int! = Int(survey)
                let c:Int! = Int(FTSlide)
                let d:Int! = Int(Stop)
                let e:Int! = Int(Start)
                if((survey != "") && (FTSlide != "")) {
                    if(a >= b){
                    //    cell.lblAhead.text = String(a-b+c)
                         lblAheadArray.replaceObject(at:textFieldIndexPath!.row, with: String(a-b+c) as Any)
                    }
                    else {
                        //cell.lblAhead.text = String(d-e)
                         lblAheadArray.replaceObject(at:textFieldIndexPath!.row, with: String(d-e) as Any)
                    }
                }
            }
            
            if((SurveyArray[textFieldIndexPath!.row] as! String != "") && (StopSLArray[textFieldIndexPath!.row] as! String != "") && (DiffArray[textFieldIndexPath!.row] as! String != "") && (StartSLArray[textFieldIndexPath!.row] as! String != "")){
                let prevStop = StopSLArray[textFieldIndexPath!.row] as! String
                let survey = SurveyArray[textFieldIndexPath!.row+1] as! String
                let FTSlide =  DiffArray[textFieldIndexPath!.row] as! String
                let Start =   StartSLArray[textFieldIndexPath!.row] as! String
                let a:Int! = Int(prevStop)
                let b:Int! = Int(survey)
                let c:Int! = Int(FTSlide)
                let e:Int! = Int(Start)
                if((survey != "") && (FTSlide != "") && (Start != "")) {
                    if(a >= b){
                //        cell.lblSeen.text = String((a-e+c)-(a-b))
//                        lblSeenArray.replaceObject(at:textFieldIndexPath!.row, with: String((a-e+c)-(a-b)) as Any)
                    }
                    else {
                  //      cell.lblSeen.text = String(a-e)
//                        lblSeenArray.replaceObject(at:textFieldIndexPath!.row, with: String(a-e) as Any)
                    }
                }
            }
        DetailsTblview.reloadData()
        }

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        let maxLength = 5
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength && string == numberFiltered
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnActnEye(_ sender: Any) {
        timer?.invalidate()
        timer = nil
        
        print(count)
        
        if count == 4 {
            
            timerAfterFiveSec?.invalidate()
            timerAfterFiveSec = nil
            
            timerAfterFiveSec = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(methodToMakeCountZeroIfThereIsNoTouchWithinSecondAfterFiveTouches), userInfo: nil, repeats: true)
            count += 1
            return
        } else if count > 4 {
            
            timerAfterFiveSec?.invalidate()
            timerAfterFiveSec = nil
            
            if secondsCount < totalSeconds {
                count = 0
                secondsCount = 0
                print("Seconds Count : \(secondsCount)")
                print("Didnt enter into unhidden due to tapping ")
                print(count)
                return
            } else {
                twoTapsCount += 1
                if twoTapsCount > 1 {
                    self.tabBarController?.tabBar.isHidden = false
                    self.btnEye.isHidden = true
                    print("Entered in unhidden ")
                    twoTapsCount = 0
                    count = 0
                    secondsCount = 0
                    return
                }
                
                //                perform(#selector(twoTapFunctionAfterCompletionOfFiveTapsWithOneSecond), with: nil, afterDelay: 2)
            }
        }
        count += 1
        
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(touchCountRestriction), userInfo: nil, repeats: true)
        }
    }
    
    @objc func touchCountRestriction() {
        count = 0
        print(count)
        timer?.invalidate()
        timer = nil
    }
    
    @objc func methodToMakeCountZeroIfThereIsNoTouchWithinSecondAfterFiveTouches() {
        print("Seconds Count : \(secondsCount) and Count: \(count)")
        secondsCount += 1
        
        if secondsCount == totalSeconds {
            timerAfterFiveSec?.invalidate()
            timerAfterFiveSec = nil
        }
    }
    
    @objc func twoTapFunctionAfterCompletionOfFiveTapsWithOneSecond() {
        if twoTapsCount < 2 {
            count = 0
            twoTapsCount = 0
            secondsCount = 0
        }
    }
    
}

