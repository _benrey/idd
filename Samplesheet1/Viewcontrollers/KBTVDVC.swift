//
//  KBTVDVC.swift
//  Samplesheet1
//
//  Created by Sankar on 2/6/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit

class KBTVDVC: UIViewController {

    @IBOutlet var viewwindowResponse: UIView!
    @IBOutlet var TxtfieldWindowresult2: GSBaseTextField!
    @IBOutlet var TxtfieldWindowResult1: GSBaseTextField!
    @IBOutlet var TxtfieldTargetcenter: GSBaseTextField!
    @IBOutlet var TxtfieldRiseDrop: GSBaseTextField!
    @IBOutlet var Txtfieldhypotenuse: GSBaseTextField!
    @IBOutlet var Txtfieldminus: GSBaseTextField!
    @IBOutlet var TxtfieldSurvey: GSBaseTextField!
    @IBOutlet var TxtfieldPlus: GSBaseTextField!
    @IBOutlet var TxtfieldDipAngle: GSBaseTextField!
    @IBOutlet var TxtfieldKBTVD: GSBaseTextField!
    @IBOutlet var Scrollview: UIScrollView!
    
    var CalculateCondition : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CalculateCondition = 1
       self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    @IBAction func Action_Calculate(_ sender: Any) {
        
        if(((TxtfieldSurvey.text?.count) != 0) && ((TxtfieldDipAngle.text?.count) != 0))  {
        // Hypotenus result
        CalculateCondition = 2
        let survey = Double(TxtfieldSurvey.text!)
        let hypotenus = (survey!*1/cos(deg2rad(abs(90-Double(TxtfieldDipAngle.text!)!))))
        Txtfieldhypotenuse.text = "\(Float(hypotenus))"
        }
        
        // Rise/drop result
        if(((TxtfieldSurvey.text?.count) != 0) && ((Txtfieldhypotenuse.text?.count) != 0))  {
        let survey = Double(TxtfieldSurvey.text!)
        CalculateCondition = 2
        let hypotenusValue = Double(Txtfieldhypotenuse.text!)
        let riseDrop = sqrt((hypotenusValue! * hypotenusValue!)-(survey! * survey!))
        TxtfieldRiseDrop.text = "\(Float(riseDrop))"
        }
        
    //Target Center result
     if(((TxtfieldDipAngle.text?.count) != 0) && ((TxtfieldKBTVD.text?.count) != 0) && ((TxtfieldRiseDrop.text?.count) != 0))  {
        let text = Double(TxtfieldDipAngle.text!)
        let Dipangle:Int? = Int(text!)
        CalculateCondition = 2
        if(Dipangle! > 90){
            let targetcenter = Double(TxtfieldKBTVD.text!)! - Double(TxtfieldRiseDrop.text!)!
            TxtfieldTargetcenter.text = "\(targetcenter)"
        }
        else {
            let targetcenter = Double(TxtfieldKBTVD.text!)! + Double(TxtfieldRiseDrop.text!)!
            TxtfieldTargetcenter.text = "\(targetcenter)"
        }
    }
        
    //Window result
     if(((TxtfieldTargetcenter.text?.count) != 0) && ((TxtfieldPlus.text?.count) != 0))  {
        let one = Double(TxtfieldTargetcenter.text!)! - Double(TxtfieldPlus.text!)!
        TxtfieldWindowResult1.text = "\(one)"
        let two = Double(TxtfieldTargetcenter.text!)! + Double(Txtfieldminus.text!)!
        TxtfieldWindowresult2.text = "\(two)"
        }
        
        if(CalculateCondition == 1){
            self.popupAlert()
        }
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        Scrollview.contentSize = CGSize(width:Scrollview.frame.size.width , height: viewwindowResponse.frame.origin.y+viewwindowResponse.frame.size.height+90)
        Scrollview.showsVerticalScrollIndicator = false
        Scrollview.showsHorizontalScrollIndicator = false
        
    }
    
    //MARK: TextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        view.endEditing(true)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        let maxLength = 5
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength && string == numberFiltered
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
