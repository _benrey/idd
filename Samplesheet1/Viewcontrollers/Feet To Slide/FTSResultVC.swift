//
//  FTSResultVC.swift
//  Samplesheet1
//
//  Created by Ratheesh TR on 02/02/19.
//  Copyright © 2019 Sankar. All rights reserved.
//

import UIKit

class FTSResultVC: UIViewController,UIScrollViewDelegate, UITextFieldDelegate  {
    
    @IBOutlet var vwMain: UIView!
    @IBOutlet var scrollVw: UIScrollView!
    @IBOutlet weak var lblResultValue: UILabel!
    
    @IBOutlet var motorYield_txt: UITextField!
    @IBOutlet var buildRateNeeded_txt: UITextField!
    @IBOutlet var courseLength_txt: UITextField!
    
    var motorYield = String()
    var buildRateNeeded = String()
    var courseLength = String()
    var resultValue_Str = String()
    var resultValue_Double = Double()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        assignbackground()
        
        motorYield_txt.delegate = self
        buildRateNeeded_txt.delegate = self
        courseLength_txt.delegate = self
        
        calculate()
        
        
        motorYield_txt.text = motorYield
        buildRateNeeded_txt.text = buildRateNeeded
        courseLength_txt.text = courseLength
        
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        //resultView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        
    }
    override func viewWillAppear(_ animated: Bool) {
        //self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        
        
        
    }
    
    //MARK: - Calculation Methods
    func calculate(){
        if motorYield.count != 0 && buildRateNeeded.count != 0 && courseLength.count != 0 {
            let motoryield = Double(motorYield)
            let buildrateneeded = Double(buildRateNeeded)
            let courseLengthValue = Double(courseLength)
            let result = (buildrateneeded! / motoryield!) * courseLengthValue!
            resultValue_Double = Double(result)
            let resultCheck =  (resultValue_Double*100).rounded()/100
            resultValue_Str = String(resultCheck)
            lblResultValue.text = resultValue_Str
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        motorYield = motorYield_txt.text ??  ""
        buildRateNeeded = buildRateNeeded_txt.text ??  ""
        courseLength = courseLength_txt.text ?? ""
        
        switch textField{
        case motorYield_txt:
            if string == ""{
                motorYield.removeLast()
            }else{
                motorYield = motorYield + string
            }
        case buildRateNeeded_txt:
            if string == ""{
                buildRateNeeded.removeLast()
            }else{
                buildRateNeeded = buildRateNeeded + string
            }
        case courseLength_txt:
            if string == ""{
                courseLength.removeLast()
            }else{
                courseLength = courseLength + string
            }
            
        default:
            print("")
        }
        
        calculate()
        
        return true
    }
    
    @available(iOS 10.0, *)
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        
        switch textField{
        case motorYield_txt:
            if  motorYield == ""  {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter motor yield value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                calculate()
            }
            
        case buildRateNeeded_txt:
            
            if buildRateNeeded == "" {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter build rate needed value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
            else{
                calculate()
                
            }
            
        case courseLength_txt:
            if courseLength == "" {
                
                let alert = UIAlertController(title:"Alert", message:"Please enter courseLength value", preferredStyle: .alert)
                let action = UIAlertAction(title:"OK", style: .default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                calculate()
            }
            
        default:
            print("")
        }
    }
    
    
    func assignbackground(){
        let background = UIImage(named: "background")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    //MARK: - Gesture Recognizer methods
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
          //  let tabBar = self.navigationController?.navigationController?.viewControllers[0] as? UITabBarController
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
//                tabBar?.selectedIndex = 3
                tabBarController?.selectedIndex = 3
                self.navigationController?.popViewController(animated: true)
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
              //  tabBar?.selectedIndex += 1
                tabBarController?.selectedIndex += 1
                self.navigationController?.popViewController(animated: true)
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
}
