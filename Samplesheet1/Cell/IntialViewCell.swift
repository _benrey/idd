//
//  IntialViewCell.swift
//  Samplesheet1
//
//  Created by Ratheesh TR on 25/01/19.
//  Copyright © 2019 Sankar. All rights reserved.
//

import UIKit

class IntialViewCell: UICollectionViewCell {
    @IBOutlet weak var valueTxtField: UITextField!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var enterBtn: UIButton!
    @IBOutlet weak var signBtn : UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.enterBtn.layer.cornerRadius = 15
        self.enterBtn.layer.masksToBounds = true
        
        
    }
}
