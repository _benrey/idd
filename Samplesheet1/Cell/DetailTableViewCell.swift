//
//  DetailTableViewCell.swift
//  Samplesheet1
//
//  Created by Sankar on 21/5/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit

class DetailTableViewCell: UITableViewCell{
    
    @IBOutlet var lblSerialNo: GSBaseLabel!
    @IBOutlet var lblCourseLength: GSBaseLabel!
    @IBOutlet var lblSurvey: GSBaseLabel!
    @IBOutlet var lblSeen: GSBaseLabel!
    @IBOutlet var lblDiff: GSBaseLabel!
    @IBOutlet var lblAhead: GSBaseLabel!
    @IBOutlet var lblAvgTF: GSBaseLabel!
    @IBOutlet var lblFTSlide: GSBaseLabel!
    @IBOutlet var lblBRft: GSBaseLabel!
    @IBOutlet var lblBR30: GSBaseLabel!
    @IBOutlet var lblRequired: GSBaseLabel!
    
    
    @IBOutlet var TxtfieldStopSL: GSBaseTextField!
    @IBOutlet var TxtfieldDepth: GSBaseTextField!
    @IBOutlet var TxtfieldINC: GSBaseTextField!
    @IBOutlet var TxtFieldAZI: GSBaseTextField!
    @IBOutlet var TxtfieldStartSL: GSBaseTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        TxtfieldStopSL.text = ""
//        TxtfieldDepth.text = ""
//        TxtfieldINC.text = ""
//        TxtFieldAZI.text = ""
//        TxtfieldStartSL.text = ""
//
//        lblSerialNo.text = ""
//        lblCourseLength.text = ""
//        lblSurvey.text = ""
//        lblSeen.text = ""
//        lblDiff.text = ""
//        lblAhead.text = ""
//        lblAvgTF.text = ""
//        lblFTSlide.text = ""
//        lblBRft.text = ""
//        lblBR30.text = ""
//        lblRequired.text = ""
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
