//
//  DcsheetCell.swift
//  Samplesheet1
//
//  Created by Sankar on 27/6/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit

class DcsheetCell: UITableViewCell {

    @IBOutlet var LblFour: GSBaseLabel!
    @IBOutlet var LblThree: GSBaseLabel!
    @IBOutlet var LblTwo: GSBaseLabel!
    @IBOutlet var LblOne: GSBaseLabel!
    @IBOutlet var LblCurveshoe: GSBaseLabel!
    @IBOutlet var TxtfieldRemarks: GSBaseTextField!
    @IBOutlet var LblSurveyDepth: GSBaseLabel!
    @IBOutlet var LblKellyDown: GSBaseLabel!
    @IBOutlet var LblTotalString: GSBaseLabel!
    @IBOutlet var LblPipeOnly: GSBaseLabel!
    @IBOutlet var TxtfieldJointLength: GSBaseTextField!
    @IBOutlet var LblStdNo: GSBaseLabel!
    @IBOutlet var LblJointnumber: GSBaseLabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
