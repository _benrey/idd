//
//  CalculatorCollectionViewCell.swift
//  Samplesheet1
//
//  Created by Ratheesh TR on 12/10/18.
//  Copyright © 2018 Sankar. All rights reserved.
//

import UIKit
class CalculatorCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var enterBtn: UIButton!
    @IBOutlet weak var signBtn : UIButton!
  //  @IBOutlet weak var btnHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
     //   self.enterBtn.layer.cornerRadius = enterBtn.frame.size.height / 2
        self.enterBtn.layer.cornerRadius = 15
        self.enterBtn.layer.masksToBounds = true

        
    }
}

